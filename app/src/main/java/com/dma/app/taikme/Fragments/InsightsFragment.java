package com.dma.app.taikme.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dma.app.taikme.APIs.StatisticsAPIs;
import com.dma.app.taikme.APIsClass.StatisticsAPIsClass;
import com.dma.app.taikme.Activities.ChartActivity;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.StatisticDayObject;
import com.dma.app.taikme.Models.StatisticsObject;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class InsightsFragment extends BaseFragment {

    private BarChart cht_up_votes,cht_down_votes,cht_comments,cht_shares;
    private TextView tv_up_votes,tv_down_votes,tv_comments,tv_shares;
    private TextView tv_time_remaining;
    private TextView title,date;
    private LinearLayout root;
    private ScrollView scrollView;

    private List<Float> up_votes_list,down_votes_list,comments_list,shares_list;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_insights,container,false);
    }

    @Override
    public void init_views() {
        //Charts
        cht_up_votes = base.findViewById(R.id.insights_upvotes_chart);
        cht_down_votes = base.findViewById(R.id.insights_downvotes_chart);
        cht_comments = base.findViewById(R.id.insights_comments_chart);
        cht_shares = base.findViewById(R.id.insights_amplifies_chart);
        //TextView
        tv_up_votes = base.findViewById(R.id.insights_upvotes);
        tv_down_votes = base.findViewById(R.id.insights_downvotes);
        tv_comments = base.findViewById(R.id.insights_comments);
        tv_shares = base.findViewById(R.id.insights_amplifies);
        tv_time_remaining = base.findViewById(R.id.time_remaining);
        title = base.findViewById(R.id.insights_name);
        date = base.findViewById(R.id.insights_date);
        //LinearLayout
        root = base.findViewById(R.id.layout);
        //ScrollView
        scrollView = base.findViewById(R.id.scrollView);

    }

    @Override
    public void init_events() {

    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getArguments().getString("title"));
        date.setText(getArguments().getString("date"));
        init_charts();
        callStatsAPI();
    }

    private void init_charts(){
        up_votes_list = new ArrayList<>();
        down_votes_list = new ArrayList<>();
        comments_list = new ArrayList<>();
        shares_list = new ArrayList<>();
    }

    public class MyXAxisValueFormater extends ValueFormatter {
        public String[] mValues;

        public MyXAxisValueFormater(String[] mValues) {
            this.mValues = mValues;
        }

        @Override
        public String getFormattedValue(float value) {
            return mValues[(int)value];
        }
    }

    private void setupChart(BarChart chart, List<Float> data,int maxvalue){
        chart.setDragEnabled(true);
        chart.setScaleEnabled(false);
        ArrayList<BarEntry> yValues = new ArrayList<>();
        int index = 0;
        for (Float f : data){
            yValues.add(new BarEntry(index++,f));
        }
        BarDataSet set1 = new BarDataSet(yValues,"Data set 1");
        set1.setDrawValues(false);
        set1.setColor(getResources().getColor(R.color.colorPrimary));
        chart.getAxisRight().setEnabled(false);
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        BarData barData = new BarData(dataSets);
        barData.setBarWidth(0.2f);
        chart.setData(barData);

        String[] days = new String[]{"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new MyXAxisValueFormater(days));
        xAxis.setGranularity(1);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(getResources().getColor(R.color.dark_grey));
        xAxis.setTextSize(12f);
        xAxis.setAxisLineColor(getResources().getColor(R.color.black));
        xAxis.setAxisLineWidth(1f);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setTextColor(getResources().getColor(R.color.dark_grey));
        yAxis.setTextSize(14f);
        yAxis.setAxisLineColor(getResources().getColor(R.color.black));
        yAxis.setAxisLineWidth(1f);
        chart.setGridBackgroundColor(getResources().getColor(R.color.light_grey));

        Description description = new Description();
        description.setEnabled(false);
        chart.setDescription(description);

        Legend legend = chart.getLegend();
        legend.setEnabled(false);

        chart.animateXY(3000,3000);
    }

    private void callStatsAPI(){
        scrollView.setVisibility(View.GONE);
        StatisticsAPIsClass.get_stats(
                base,
                BaseFunctions.getDeviceId(base),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        StatisticsObject success = new Gson().fromJson(j,StatisticsObject.class);
                        tv_up_votes.setText(success.getUpvoted_count()+"");
                        tv_down_votes.setText(success.getDownvoted_count()+"");
                        tv_comments.setText(success.getCommented_count()+"");
                        tv_shares.setText(success.getShared_count()+"");
                        for (int i = 0; i < 7; i++) {
                            up_votes_list.add(0f);
                            //up_votes_list.set(i,0f);
                            down_votes_list.add(0f);
                            //down_votes_list.set(i,0f);
                            comments_list.add(0f);
                            //comments_list.set(i,0f);
                            shares_list.add(0f);
                            //shares_list.set(i,0f);
                        }
                        if (success.getUpvoted_array().size()>0){
                            for (StatisticDayObject o : success.getUpvoted_array()){
                                switch (o.getDayName().toLowerCase()){
                                   case "monday":{
                                       up_votes_list.set(0,o.getCnt());
                                   }break;
                                    case "tuesday":{
                                        up_votes_list.set(1,o.getCnt());
                                    }break;
                                    case "wednesday":{
                                        up_votes_list.set(2,o.getCnt());
                                    }break;
                                    case "thursday":{
                                        up_votes_list.set(3,o.getCnt());
                                    }break;
                                    case "friday":{
                                        up_votes_list.set(4,o.getCnt());
                                    }break;
                                    case "saturday":{
                                        up_votes_list.set(5,o.getCnt());
                                    }break;
                                    case "sunday":{
                                        up_votes_list.set(6,o.getCnt());
                                    }break;
                                }
                            }
                        }
                        if (success.getDownvoted_array().size()>0){
                            for (StatisticDayObject o : success.getDownvoted_array()){
                                switch (o.getDayName().toLowerCase()){
                                    case "monday":{
                                        down_votes_list.set(0,o.getCnt());
                                    }break;
                                    case "tuesday":{
                                        down_votes_list.set(1,o.getCnt());
                                    }break;
                                    case "wednesday":{
                                        down_votes_list.set(2,o.getCnt());
                                    }break;
                                    case "thursday":{
                                        down_votes_list.set(3,o.getCnt());
                                    }break;
                                    case "friday":{
                                        down_votes_list.set(4,o.getCnt());
                                    }break;
                                    case "saturday":{
                                        down_votes_list.set(5,o.getCnt());
                                    }break;
                                    case "sunday":{
                                        down_votes_list.set(6,o.getCnt());
                                    }break;
                                }
                            }
                        }
                        if (success.getCommented_array().size()>0){
                            for (StatisticDayObject o : success.getCommented_array()){
                                switch (o.getDayName().toLowerCase()){
                                    case "monday":{
                                        comments_list.set(0,o.getCnt());
                                    }break;
                                    case "tuesday":{
                                        comments_list.set(1,o.getCnt());
                                    }break;
                                    case "wednesday":{
                                        comments_list.set(2,o.getCnt());
                                    }break;
                                    case "thursday":{
                                        comments_list.set(3,o.getCnt());
                                    }break;
                                    case "friday":{
                                        comments_list.set(4,o.getCnt());
                                    }break;
                                    case "saturday":{
                                        comments_list.set(5,o.getCnt());
                                    }break;
                                    case "sunday":{
                                        comments_list.set(6,o.getCnt());
                                    }break;
                                }
                            }
                        }
                        if (success.getShared_array().size()>0){
                            for (StatisticDayObject o : success.getShared_array()){
                                switch (o.getDayName().toLowerCase()){
                                    case "monday":{
                                        shares_list.set(0,o.getCnt());
                                    }break;
                                    case "tuesday":{
                                        shares_list.set(1,o.getCnt());
                                    }break;
                                    case "wednesday":{
                                        shares_list.set(2,o.getCnt());
                                    }break;
                                    case "thursday":{
                                        shares_list.set(3,o.getCnt());
                                    }break;
                                    case "friday":{
                                        shares_list.set(4,o.getCnt());
                                    }break;
                                    case "saturday":{
                                        shares_list.set(5,o.getCnt());
                                    }break;
                                    case "sunday":{
                                        shares_list.set(6,o.getCnt());
                                    }break;
                                }
                            }
                        }
                        long[] a = BaseFunctions.processMilliseconds(success.getRemaining_time()*1000);
                        tv_time_remaining.setText(a[1]+":"+a[2]+":"+a[3]);
                        scrollView.setVisibility(View.VISIBLE);
                        setupChart(cht_up_votes,up_votes_list,5000);
                        setupChart(cht_down_votes,down_votes_list,5000);
                        setupChart(cht_comments,comments_list,5000);
                        setupChart(cht_shares,shares_list,5000);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callStatsAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
