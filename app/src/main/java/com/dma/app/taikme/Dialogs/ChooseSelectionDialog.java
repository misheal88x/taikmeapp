package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dma.app.taikme.Interfaces.ISelection;
import com.dma.app.taikme.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class ChooseSelectionDialog extends AlertDialog {
    private Context context;
    private String type;
    private ISelection selection;
    private RelativeLayout lyt_camera,lyt_gallery;

    public ChooseSelectionDialog(@NonNull Context context,String type,ISelection selection) {
        super(context);
        this.context = context;
        this.type = type;
        this.selection = selection;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_choose_selection);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        //LinearLayout
        lyt_camera = findViewById(R.id.choose_selection_camera);
        lyt_gallery = findViewById(R.id.choose_selection_gallery);
    }

    private void init_events() {
        lyt_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("camera")){
                    selection.onTakeCameraClicked();
                    dismiss();
                }else {
                    selection.onTakeVideoClicked();
                    dismiss();
                }
            }
        });

        lyt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("camera")){
                    selection.onCameraGalleryClicked();
                    dismiss();
                }else {
                    selection.onVideoGalleryClicked();
                    dismiss();
                }
            }
        });
    }

    private void init_dialog() {

    }
}
