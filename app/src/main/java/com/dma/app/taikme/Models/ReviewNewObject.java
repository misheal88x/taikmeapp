package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class ReviewNewObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("reviewable_id") private int reviewable_id = 0;
    @SerializedName("reviewable_type") private String reviewable_type = "";
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("review_text") private String review_text = "";
    @SerializedName("rating") private float rating = 0f;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("user_name") private String user_name = "";
    @SerializedName("user_mobile_number") private String user_mobile_number = "";
    @SerializedName("user_image") private String user_image = "";
    @SerializedName("user_thumb_image") private String user_thumb_image = "";
    @SerializedName("user_cover_image") private String user_cover_image = "";
    @SerializedName("user_email") private String user_email = "";
    @SerializedName("user_city_name") private String user_city_name = "";
    @SerializedName("since_minute") private float since_minute = 0f;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReviewable_id() {
        return reviewable_id;
    }

    public void setReviewable_id(int reviewable_id) {
        this.reviewable_id = reviewable_id;
    }

    public String getReviewable_type() {
        return reviewable_type;
    }

    public void setReviewable_type(String reviewable_type) {
        this.reviewable_type = reviewable_type;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getReview_text() {
        return review_text;
    }

    public void setReview_text(String review_text) {
        this.review_text = review_text;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_mobile_number() {
        return user_mobile_number;
    }

    public void setUser_mobile_number(String user_mobile_number) {
        this.user_mobile_number = user_mobile_number;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_thumb_image() {
        return user_thumb_image;
    }

    public void setUser_thumb_image(String user_thumb_image) {
        this.user_thumb_image = user_thumb_image;
    }

    public String getUser_cover_image() {
        return user_cover_image;
    }

    public void setUser_cover_image(String user_cover_image) {
        this.user_cover_image = user_cover_image;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public float getSince_minute() {
        return since_minute;
    }

    public void setSince_minute(float since_minute) {
        this.since_minute = since_minute;
    }

    public String getUser_city_name() {
        return user_city_name;
    }

    public void setUser_city_name(String user_city_name) {
        this.user_city_name = user_city_name;
    }
}
