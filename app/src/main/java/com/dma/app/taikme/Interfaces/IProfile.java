package com.dma.app.taikme.Interfaces;

public interface IProfile {
    void onChange();
    void onRemove();
}
