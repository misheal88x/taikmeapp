package com.dma.app.taikme.Others;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PreCachingLayoutManager extends LinearLayoutManager {
    private int defaultExtraLayoutSpace = 600;
    private int extraLayoutSpace = -1;
    private Context context = null;

    public PreCachingLayoutManager(Context context) {
        super(context);
        this.context = context;
    }

    public PreCachingLayoutManager(Context context, int extraLayoutSpace) {
        super(context);
        this.context = context;
        this.extraLayoutSpace = extraLayoutSpace;
    }

    public PreCachingLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
        this.context = context;
    }

    public void setExtraLayoutSpace(int extraLayoutSpace){
        this.extraLayoutSpace = extraLayoutSpace;
    }

    public int getExtraLayoutSpace(RecyclerView.State state){
        if (extraLayoutSpace > 0) {
            return extraLayoutSpace;
        } else return defaultExtraLayoutSpace;
    }
}
