package com.dma.app.taikme.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.CategoriesAPIsClass;
import com.dma.app.taikme.APIsClass.PlacesAPIsClass;
import com.dma.app.taikme.Adapters.SearchResultsAdapter;
import com.dma.app.taikme.Fragments.HomeFragment;
import com.dma.app.taikme.Fragments.NotificationsFragment;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.PlacesResponse;
import com.dma.app.taikme.Models.VisitedPlaceObject;
import com.dma.app.taikme.Models.VisitedPlacesResponse;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class MoreListActivity extends BaseActivity implements IMove {
    private RelativeLayout toolbar;
    private ImageView btn_home;
    private RelativeLayout btn_notifications,btn_add_post,btn_more;
    private ImageView img_icon;
    private TextView tv_title;
    private Intent myIntent;
    private List<VisitedPlaceObject> listOfPlaces;
    private RecyclerView rv_results;
    private SearchResultsAdapter adapter;
    private LinearLayoutManager layoutManager;
    private String type = "";
    private RelativeLayout root;
    private LinearLayout no_data_layout;
    private NestedScrollView scrollView;
    private AVLoadingIndicatorView pb_more;
    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;
    private float clicked_place_lat = 0f,clicked_place_lng = 0f;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_more_list);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        type = myIntent.getStringExtra("type");
        init_recycler();
        if (type.equals("trending_places")){
            callTrendingAPI(currentPage,0);
        }else if (type.equals("category")){
            callPlacesAPI(currentPage,0);
        }
    }

    @Override
    public void init_views() {
        //Intent
        myIntent = getIntent();
        //Toolbar
        toolbar = findViewById(R.id.more_list_toolbar);
        btn_home = toolbar.findViewById(R.id.toolbar_home);
        btn_notifications = toolbar.findViewById(R.id.toolbar_notifications);
        btn_add_post = toolbar.findViewById(R.id.toolbar_add_post);
        btn_more = toolbar.findViewById(R.id.toolbar_more_btn);
        img_icon = findViewById(R.id.toolbar_icon);
        tv_title = findViewById(R.id.toolbar_title);
        img_icon.setImageResource(myIntent.getIntExtra("image",0));
        tv_title.setText(myIntent.getStringExtra("title"));
        //RecyclerView
        rv_results = findViewById(R.id.more_list_recycler);
        //RelativeLayout
        root = findViewById(R.id.more_list_layout);
        //LinearLayout
        no_data_layout = findViewById(R.id.no_data_layout);
        //ScrollView
        scrollView = findViewById(R.id.more_scroll);
        //ProgressBar
        pb_more = findViewById(R.id.more_list_more);


    }

    @Override
    public void init_events() {
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MoreListActivity.this,HomeActivity.class);
                intent.putExtra("type","home");
                startActivity(intent);
                finish();
            }
        });
        btn_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MoreListActivity.this,HomeActivity.class);
                intent.putExtra("type","notifications");
                startActivity(intent);
                finish();
            }
        });
        btn_add_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MoreListActivity.this,HomeActivity.class);
                intent.putExtra("type","post");
                startActivity(intent);
                finish();
            }
        });
        btn_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MoreListActivity.this,MoreActivity.class));
            }
        });
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (listOfPlaces.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                if (type.equals("trending_places")){
                                    callTrendingAPI(currentPage,1);
                                }else if (type.equals("category")){
                                    callPlacesAPI(currentPage,1);
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        listOfPlaces = new ArrayList<>();
        adapter = new SearchResultsAdapter(MoreListActivity.this,listOfPlaces,this);
        layoutManager = new LinearLayoutManager(MoreListActivity.this,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_results.setLayoutManager(layoutManager);
        rv_results.setAdapter(adapter);
    }

    private void callTrendingAPI(final int page,final int type1){
        if (type1 == 1){
            pb_more.smoothToShow();
        }
        PlacesAPIsClass.getTrendingPlaces(
                MoreListActivity.this,
                BaseFunctions.getDeviceId(MoreListActivity.this),
                page,
                type1,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type1);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            VisitedPlacesResponse success = new Gson().fromJson(j,VisitedPlacesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type1);
                                    per_page = success.getPer_page();
                                    for (VisitedPlaceObject po : success.getData()){
                                        listOfPlaces.add(po);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (type1 == 0){
                                        BaseFunctions.runAnimation(rv_results,0,adapter);
                                    }
                                }else {
                                    no_data(type1);
                                }
                            }else {
                                error_happend(type1);
                            }
                        }else {
                            error_happend(type1);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type1);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callTrendingAPI(page,type1);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }
    private void callPlacesAPI(final int page,final int type1){
        if (type1 == 1){
            pb_more.smoothToShow();
        }
        CategoriesAPIsClass.getCategoryPlaces(
                MoreListActivity.this,
                BaseFunctions.getDeviceId(MoreListActivity.this),
                String.valueOf(getIntent().getIntExtra("id",0)),
                page,
                type1,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type1);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            VisitedPlacesResponse success = new Gson().fromJson(j,VisitedPlacesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type1);
                                    per_page = success.getPer_page();
                                    for (VisitedPlaceObject po : success.getData()){
                                        listOfPlaces.add(po);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (type1 == 0){
                                        BaseFunctions.runAnimation(rv_results,0,adapter);
                                    }
                                }else {
                                    no_data(type1);
                                }
                            }else {
                                error_happend(type1);
                            }
                        }else {
                            error_happend(type1);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type1);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callTrendingAPI(page,type1);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            pb_more.smoothToHide();
        }else {
            no_data_layout.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }

    @Override
    public void move() {

    }

    @Override
    public void move(int position) {
        VisitedPlaceObject v = listOfPlaces.get(position);
        if (BaseFunctions.isOnline(MoreListActivity.this)){
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (v.getBusiness_locations()!=null&&v.getBusiness_locations().size()>0) {
                    clicked_place_lat = v.getBusiness_locations().get(0).getLat();
                    clicked_place_lng = v.getBusiness_locations().get(0).getLng();
                }
                ActivityCompat.requestPermissions(MoreListActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION},
                        1);
            }else {
                final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
                if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                    buildAlertMessageNoGps();
                }else {
                    if (v.getBusiness_locations()!=null&&v.getBusiness_locations().size()>0){
                        Intent intent = new Intent(MoreListActivity.this,ShowLocationMapActivity.class);
                        intent.putExtra("lat",v.getBusiness_locations().get(0).getLat());
                        intent.putExtra("lng",v.getBusiness_locations().get(0).getLng());
                        startActivity(intent);
                    }
                }
            }
        }else {
            Toast.makeText(this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
                if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                    buildAlertMessageNoGps();
                }else {
                    if (clicked_place_lng!=0f&&clicked_place_lat!=0f) {
                        Intent intent = new Intent(MoreListActivity.this, ShowLocationMapActivity.class);
                        intent.putExtra("lat",clicked_place_lat);
                        intent.putExtra("lng",clicked_place_lng);
                        startActivity(intent);
                    }
                }
            }else {
                Toast.makeText(this, getResources().getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void buildAlertMessageNoGps(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(MoreListActivity.this);
        builder.setMessage(getResources().getString(R.string.gps_disabled))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.gps_turn_on), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
