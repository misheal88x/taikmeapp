package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.CustomAudienceAPIs;
import com.dma.app.taikme.APIs.FollowersAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Field;

public class CustomAudienceAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void getAll(final Context context,
                              String device_id,
                              int page,
                              final int type,
                              IResponse onResponse1,
                              final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        if (type == 0) {
            dialog = new NewProgressDialog(context);
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(context);
        CustomAudienceAPIs api = retrofit.create(CustomAudienceAPIs.class);
        Call<BaseResponse> call = api.get_all("application/json",
                device_id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0) {
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void create(final Context context,
                              String device_id,
                              String en_name,
                              String ar_name,
                              String gender,
                              int location_id,
                              String prefer,
                              String character,
                              int age_range_from,
                              int age_range_to,
                              String going_on,
                              String best_cuisine,
                              String second_best_cuisine,
                              String third_best_cuisine,
                              String best_music_gender,
                              String second_best_music_gender,
                              String third_best_music_gender,
                              String follow_you,
                              IResponse onResponse1,
                              final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        CustomAudienceAPIs api = retrofit.create(CustomAudienceAPIs.class);
        Call<BaseResponse> call = api.create("application/json",device_id,en_name,ar_name,
                gender,location_id,prefer,character,age_range_from,age_range_to,going_on
        ,best_cuisine,second_best_cuisine,third_best_cuisine,best_music_gender,
                second_best_music_gender,third_best_music_gender,follow_you);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
