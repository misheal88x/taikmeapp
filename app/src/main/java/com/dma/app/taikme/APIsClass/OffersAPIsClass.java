package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.EventsAPIs;
import com.dma.app.taikme.APIs.OffersAPIs;
import com.dma.app.taikme.APIs.ShareAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Field;

public class OffersAPIsClass extends BaseRetrofit {

    private static NewProgressDialog dialog;

    public static void addOffer(final Context context,
                             String device_id,
                             int business_location_id,
                             int discount,
                             String ar_title,
                             String en_title,
                             String expiry_date,
                             String start_date,
                             IResponse onResponse1,
                             final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.add_offer("application/json",
                device_id,
                business_location_id,
                discount,
                ar_title,
                en_title,
                expiry_date,
                start_date);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void get_offers(final Context context,
                                    String device_id,
                                    String id,
                                    int page,
                                    IResponse onResponse1,
                                    final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.get_offers("application/json",
                device_id,
                id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
