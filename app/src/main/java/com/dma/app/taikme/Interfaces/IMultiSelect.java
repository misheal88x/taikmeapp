package com.dma.app.taikme.Interfaces;

import java.util.List;

public interface IMultiSelect {
    void onSelected(List<Integer> list);
}
