package com.dma.app.taikme.Models.HomeModels;

import com.google.gson.annotations.SerializedName;

public class HomeMetaDataObject {
    @SerializedName("object_type") private int object_type = 0;
    @SerializedName("is_shared") private boolean is_shared = false;
    @SerializedName("sharer") private HomeSharerObject sharer = new HomeSharerObject();
    @SerializedName("share_time") private String share_time = "";
    @SerializedName("sharing_text") private String sharing_text = "";
    @SerializedName("is_sponsored") private boolean is_sponsored = false;

    public int getObject_type() {
        return object_type;
    }

    public void setObject_type(int object_type) {
        this.object_type = object_type;
    }

    public boolean getIs_shared() {
        return is_shared;
    }

    public void setIs_shared(boolean is_shared) {
        this.is_shared = is_shared;
    }

    public HomeSharerObject getSharer() {
        return sharer;
    }

    public void setSharer(HomeSharerObject sharer) {
        this.sharer = sharer;
    }

    public boolean isIs_shared() {
        return is_shared;
    }

    public String getShare_time() {
        return share_time;
    }

    public void setShare_time(String share_time) {
        this.share_time = share_time;
    }

    public boolean isIs_sponsored() {
        return is_sponsored;
    }

    public void setIs_sponsored(boolean is_sponsored) {
        this.is_sponsored = is_sponsored;
    }

    public String getSharing_text() {
        return sharing_text;
    }

    public void setSharing_text(String sharing_text) {
        this.sharing_text = sharing_text;
    }
}
