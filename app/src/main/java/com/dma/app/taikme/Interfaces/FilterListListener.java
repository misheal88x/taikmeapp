package com.dma.app.taikme.Interfaces;

import com.zomato.photofilters.imageprocessors.Filter;

public interface FilterListListener {
    void onFilterSelected(Filter filter);
}
