package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ShowLocationMapActivity extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap map;
    private Intent myIntent;
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_show_location_map);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        initMap();
    }

    @Override
    public void init_views() {
        //Intent
        myIntent = getIntent();
    }

    @Override
    public void init_events() {

    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        float lat = myIntent.getFloatExtra("lat",0f);
        float lng = myIntent.getFloatExtra("lng",0f);
        map.addMarker(new MarkerOptions().position(new LatLng(lat,lng)).title("location"));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng),15.0f));
    }

    private void initMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.show_location_map);
        mapFragment.getMapAsync(ShowLocationMapActivity.this);
    }
}
