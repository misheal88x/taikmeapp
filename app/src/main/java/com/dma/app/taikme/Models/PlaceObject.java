package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class PlaceObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("images") private ImageObject images = null;
    @SerializedName("en_name") private String en_name = "";
    @SerializedName("ar_name") private String ar_name = "";
    @SerializedName("price") private float price = 0;
    @SerializedName("rating") private float rating = 0;
    @SerializedName("lat") private float lat = 0f;
    @SerializedName("lng") private float lng = 0f;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ImageObject getImages() {
        return images;
    }

    public void setImages(ImageObject images) {
        this.images = images;
    }

    public String getEn_name() {
        return en_name;
    }

    public void setEn_name(String en_name) {
        this.en_name = en_name;
    }

    public String getAr_name() {
        return ar_name;
    }

    public void setAr_name(String ar_name) {
        this.ar_name = ar_name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }
}
