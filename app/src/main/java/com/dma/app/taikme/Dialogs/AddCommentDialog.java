package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.CommentsAPIsClass;
import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class AddCommentDialog extends AlertDialog {
    private Context context;
    private EditText edt_comment;
    private Button btn_save;
    private String object_type;
    private int id;

    public AddCommentDialog(@NonNull Context context,String object_type,int id ) {
        super(context);
        this.context = context;
        this.object_type = object_type;
        this.id = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_add_comment);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //EditText
        edt_comment = findViewById(R.id.add_comment_edt);
        //Button
        btn_save = findViewById(R.id.add_comment_save);

    }

    private void init_events(){
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_comment.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.add_comment_no_text), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAddCommentAPI(object_type,id,edt_comment.getText().toString());
            }
        });
    }

    private void init_dialog(){
    }

    private void callAddCommentAPI(String type, int id, String comment){
        CommentsAPIsClass.addComment(
                context,
                BaseFunctions.getDeviceId(context),
                type,
                String.valueOf(id),
                comment,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_comment_success), Toast.LENGTH_SHORT).show();
                            cancel();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }


}
