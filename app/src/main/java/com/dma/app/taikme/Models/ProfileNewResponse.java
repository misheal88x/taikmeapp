package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ProfileNewResponse {

    //ok
    @SerializedName("id") private int id = 0;
    //ok
    @SerializedName("name") private String name = "";
    //ok
    @SerializedName("email") private String email = "";
    //ok
    @SerializedName("gender") private String gender = "";
    //ok
    @SerializedName("birthday") private String birthday = "";
    //ok
    @SerializedName("city_name") private String city_name = "";
    //ok
    @SerializedName("prefer") private List<Integer> prefer = new ArrayList<>();
    //ok
    @SerializedName("bio") private String bio = "";
    //ok
    @SerializedName("user_type") private String user_type = "";
    //ok
    @SerializedName("cover_image") private String cover_image = "";
    //ok
    @SerializedName("mobile_number") private String mobile_number = "";
    //ok
    @SerializedName("image") private String image = "";
    //ok
    @SerializedName("thumb_image") private String thumb_image = "";
    //ok
    @SerializedName("age") private String age = "";
    //ok
    @SerializedName("country_name") private String country_name = "";
    //ok
    @SerializedName("first_best_cuisines") private String first_best_cuisines = "";
    //ok
    @SerializedName("second_best_cuisines") private String second_best_cuisines = "";
    //ok
    @SerializedName("third_best_cuisines") private String third_best_cuisines = "";
    //ok
    @SerializedName("website") private String website = "";
    //ok
    @SerializedName("atmosphere") private int atmosphere = 0;
    //ok
    @SerializedName("first_music_genre") private String first_music_genre = "";
    //ok
    @SerializedName("second_music_genre") private String second_music_genre = "";
    //ok
    @SerializedName("third_music_genre") private String third_music_genre = "";
    //ok
    @SerializedName("cuisine") private String cuisine = "";
    //ok
    @SerializedName("kids_area") private int kids_area = 0;
    //ok
    @SerializedName("handicapped_entrance") private int handicapped_entrance = 0;
    //ok
    @SerializedName("takeaway") private int takeaway = 0;
    //ok
    @SerializedName("rest_rooms") private int rest_rooms = 0;
    //ok
    @SerializedName("price_range") private int price_range = 0;
    //ok
    @SerializedName("better_for") private int better_for = 0;
    //ok
    @SerializedName("signature") private String signature = "";
    //ok
    @SerializedName("account_status") private String account_status = "";
    //ok
    @SerializedName("verification_code") private String verification_code = "";
    //ok
    @SerializedName("reviews_rating") private String reviews_rating = "";
    //ok
    @SerializedName("following_count") private int following_count = 0;
    //ok
    @SerializedName("followers_count") private int followers_count = 0;
    //ok
    @SerializedName("is_following") private int is_following = 0;
    //ok
    @SerializedName("votes_count") private int votes_count = 0;
    //ok
    @SerializedName("seating") private Object seating = null;
    //ok
    @SerializedName("parking") private int parking = 0;
    //ok
    @SerializedName("device_setting") private DeviceSettingsObject device_setting = new DeviceSettingsObject();
    //@SerializedName("going_on") private List<Integer> going_on = new ArrayList<>();
    //@SerializedName("categories") private List<CategoryObject> categories = new ArrayList<>();
    //@SerializedName("working_hours") private List<WorkingHourObject> working_hours = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public List<Integer> getPrefer() {
        return prefer;
    }

    public void setPrefer(List<Integer> prefer) {
        this.prefer = prefer;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getFirst_best_cuisines() {
        return first_best_cuisines;
    }

    public void setFirst_best_cuisines(String first_best_cuisines) {
        this.first_best_cuisines = first_best_cuisines;
    }

    public String getSecond_best_cuisines() {
        return second_best_cuisines;
    }

    public void setSecond_best_cuisines(String second_best_cuisines) {
        this.second_best_cuisines = second_best_cuisines;
    }

    public String getThird_best_cuisines() {
        return third_best_cuisines;
    }

    public void setThird_best_cuisines(String third_best_cuisines) {
        this.third_best_cuisines = third_best_cuisines;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getAtmosphere() {
        return atmosphere;
    }

    public void setAtmosphere(int atmosphere) {
        this.atmosphere = atmosphere;
    }

    public String getFirst_music_genre() {
        return first_music_genre;
    }

    public void setFirst_music_genre(String first_music_genre) {
        this.first_music_genre = first_music_genre;
    }

    public String getSecond_music_genre() {
        return second_music_genre;
    }

    public void setSecond_music_genre(String second_music_genre) {
        this.second_music_genre = second_music_genre;
    }

    public String getThird_music_genre() {
        return third_music_genre;
    }

    public void setThird_music_genre(String third_music_genre) {
        this.third_music_genre = third_music_genre;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public int getKids_area() {
        return kids_area;
    }

    public void setKids_area(int kids_area) {
        this.kids_area = kids_area;
    }

    public int getHandicapped_entrance() {
        return handicapped_entrance;
    }

    public void setHandicapped_entrance(int handicapped_entrance) {
        this.handicapped_entrance = handicapped_entrance;
    }

    public int getTakeaway() {
        return takeaway;
    }

    public void setTakeaway(int takeaway) {
        this.takeaway = takeaway;
    }

    public int getRest_rooms() {
        return rest_rooms;
    }

    public void setRest_rooms(int rest_rooms) {
        this.rest_rooms = rest_rooms;
    }

    public int getPrice_range() {
        return price_range;
    }

    public void setPrice_range(int price_range) {
        this.price_range = price_range;
    }

    public int getBetter_for() {
        return better_for;
    }

    public void setBetter_for(int better_for) {
        this.better_for = better_for;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }

    public String getVerification_code() {
        return verification_code;
    }

    public void setVerification_code(String verification_code) {
        this.verification_code = verification_code;
    }

    public String getReviews_rating() {
        return reviews_rating;
    }

    public void setReviews_rating(String reviews_rating) {
        this.reviews_rating = reviews_rating;
    }

    public int getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(int following_count) {
        this.following_count = following_count;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }

    public int getIs_following() {
        return is_following;
    }

    public void setIs_following(int is_following) {
        this.is_following = is_following;
    }

    public int getVotes_count() {
        return votes_count;
    }

    public void setVotes_count(int votes_count) {
        this.votes_count = votes_count;
    }

    public Object getSeating() {
        return seating;
    }

    public void setSeating(Object seating) {
        this.seating = seating;
    }

    public int getParking() {
        return parking;
    }

    public void setParking(int parking) {
        this.parking = parking;
    }

    public DeviceSettingsObject getDevice_setting() {
        return device_setting;
    }

    public void setDevice_setting(DeviceSettingsObject device_setting) {
        this.device_setting = device_setting;
    }
}
