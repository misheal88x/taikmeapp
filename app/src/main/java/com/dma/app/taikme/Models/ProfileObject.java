package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class ProfileObject {
    @SerializedName("user_basic_info") private UserObject user_basic_info = new UserObject();
    @SerializedName("device") private DeviceObject device = new DeviceObject();
    @SerializedName("device_setting") private DeviceSettingsObject device_setting = new DeviceSettingsObject();
    @SerializedName("tokens") private TokensObject tokens = new TokensObject();

    public UserObject getUser_basic_info() {
        return user_basic_info;
    }

    public void setUser_basic_info(UserObject user_basic_info) {
        this.user_basic_info = user_basic_info;
    }

    public DeviceObject getDevice() {
        return device;
    }

    public void setDevice(DeviceObject device) {
        this.device = device;
    }

    public DeviceSettingsObject getDevice_setting() {
        return device_setting;
    }

    public void setDevice_setting(DeviceSettingsObject device_setting) {
        this.device_setting = device_setting;
    }

    public TokensObject getTokens() {
        return tokens;
    }

    public void setTokens(TokensObject tokens) {
        this.tokens = tokens;
    }
}
