package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.CommentsAPIsClass;
import com.dma.app.taikme.Adapters.AllCommentsAdapter;
import com.dma.app.taikme.Dialogs.AddCommentDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.CommentObject;
import com.dma.app.taikme.Models.CommentsResponse;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class AddCommentActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private RelativeLayout back;
    private TextView title;

    private EditText edt_text;
    private Button btn_add;

    private AVLoadingIndicatorView loading,more;
    private TextView desc;
    private RecyclerView recycler;
    private AllCommentsAdapter commentsAdapter;
    private List<CommentObject> list;
    private NestedScrollView scrollView;

    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;

    private int id;
    private String type;

    private LinearLayout root;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_add_comment);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.add_comment_title));
        init_recycler();
        id = getIntent().getIntExtra("id",0);
        type = getIntent().getStringExtra("type");
        callCommentsAPI(currentPage,0);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar);
        back = findViewById(R.id.toolbar_back_btn);
        title = findViewById(R.id.toolbar_title);

        edt_text = findViewById(R.id.add_comment_edt);
        btn_add = findViewById(R.id.add_comment_save);

        loading = findViewById(R.id.loading);
        more = findViewById(R.id.comments_more);

        scrollView = findViewById(R.id.scrollView);

        desc = findViewById(R.id.desc);
        recycler = findViewById(R.id.all_comments_recycler);

        root = findViewById(R.id.layout);
    }

    @Override
    public void init_events() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(AddCommentActivity.this);
            }
        });

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callCommentsAPI(currentPage,1);
                            }
                        }
                    }
                }
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_text.getText().toString().equals("")){
                    Toast.makeText(AddCommentActivity.this, getResources().getString(R.string.add_comment_no_text), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAddCommentAPI(type,id,edt_text.getText().toString());
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        list = new ArrayList<>();
        commentsAdapter = new AllCommentsAdapter(AddCommentActivity.this,list);
        recycler.setLayoutManager(new LinearLayoutManager(AddCommentActivity.this,RecyclerView.VERTICAL,false));
        BaseFunctions.runAnimation(recycler,0,commentsAdapter);
        recycler.setAdapter(commentsAdapter);
    }

    private void callAddCommentAPI(final String type, final int id, String comment){
        btn_add.setVisibility(View.GONE);
        loading.smoothToShow();
        CommentsAPIsClass.addComment(
                AddCommentActivity.this,
                BaseFunctions.getDeviceId(AddCommentActivity.this),
                type,
                String.valueOf(id),
                comment,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        btn_add.setVisibility(View.VISIBLE);
                        loading.smoothToHide();
                    }

                    @Override
                    public void onResponse(Object json) {
                        btn_add.setVisibility(View.VISIBLE);
                        loading.smoothToHide();
                        if (json!=null){
                            Toast.makeText(AddCommentActivity.this, getResources().getString(R.string.facility_info_add_comment_success), Toast.LENGTH_SHORT).show();
                            list = new ArrayList<>();
                            commentsAdapter = new AllCommentsAdapter(AddCommentActivity.this,list);
                            recycler.setAdapter(commentsAdapter);
                            currentPage = 1;
                            continue_paginate = true;
                            edt_text.setText("");
                            callCommentsAPI(currentPage,0);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        btn_add.setVisibility(View.VISIBLE);
                        loading.smoothToHide();
                        Toast.makeText(AddCommentActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
    private void callCommentsAPI(final int page,final int type){
        if (type == 1){
            more.smoothToShow();
        }else if (type == 0){
            loading.smoothToShow();
            desc.setVisibility(View.GONE);
        }
        CommentsAPIsClass.getComments(
                AddCommentActivity.this,
                BaseFunctions.getDeviceId(AddCommentActivity.this),
                this.type,
                String.valueOf(this.id),
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            CommentsResponse success = new Gson().fromJson(j,CommentsResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (CommentObject po : success.getData()){
                                        list.add(po);
                                        commentsAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callCommentsAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            more.smoothToHide();
        }else {
            loading.smoothToHide();
        }
    }
    private void process_data(int type){
        if (type == 0){
            loading.smoothToHide();
            scrollView.setVisibility(View.VISIBLE);
            desc.setVisibility(View.GONE);
        }else {
            more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            loading.smoothToHide();
            desc.setVisibility(View.VISIBLE);
        }else {
            more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
