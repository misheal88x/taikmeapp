package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class CuisineObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("ar_name") private String ar_name = "";
    @SerializedName("en_name") private String en_name = "";

    public CuisineObject() { }

    public CuisineObject(int id, String ar_name, String en_name) {
        this.id = id;
        this.ar_name = ar_name;
        this.en_name = en_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAr_name() {
        return ar_name;
    }

    public void setAr_name(String ar_name) {
        this.ar_name = ar_name;
    }

    public String getEn_name() {
        return en_name;
    }

    public void setEn_name(String en_name) {
        this.en_name = en_name;
    }
}
