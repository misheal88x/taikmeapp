package com.dma.app.taikme.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dma.app.taikme.R;

public class CreatePostImagesAdapter extends  RecyclerView.Adapter<CreatePostImagesAdapter.ViewHolder> {
    private Context context;
    private int[] images = new int[]{
            R.drawable.view2,
            R.drawable.view3,
            R.drawable.view4,
            R.drawable.view5,
            R.drawable.view6,
            R.drawable.view7
    };

    public CreatePostImagesAdapter(Context context) {
        this.context = context;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_image;
        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_create_post_image);

        }
    }

    @Override
    public int getItemCount() {
        return images.length;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_create_post_image, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.img_image.setImageResource(images[position]);
    }
}
