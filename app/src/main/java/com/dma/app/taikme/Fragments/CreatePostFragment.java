package com.dma.app.taikme.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.zelory.compressor.Compressor;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Emitter;
import rx.Subscription;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Cancellable;

import android.os.CountDownTimer;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.baoyz.widget.PullRefreshLayout;
import com.bumptech.glide.Glide;
import com.dma.app.taikme.APIsClass.PlacesAPIsClass;
import com.dma.app.taikme.APIsClass.PostsAPIsClass;
import com.dma.app.taikme.Activities.CreateStatusActivity;
import com.dma.app.taikme.Activities.EditStatusActivity;
import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Activities.RecordVideoActivity;
import com.dma.app.taikme.Activities.VideoStatusActivity;
import com.dma.app.taikme.Activities.ViewVideoActivity;
import com.dma.app.taikme.Adapters.CreatePostImagesAdapter;
import com.dma.app.taikme.Adapters.CreatePostLocationAdapter;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Dialogs.ChooseSelectionDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Interfaces.ISelection;
import com.dma.app.taikme.Models.NearByPlaceObject;
import com.dma.app.taikme.Models.PostObject;
import com.dma.app.taikme.Models.StoryObject;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Services.UploadStoryService;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.tcking.giraffecompressor.GiraffeCompressor;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;


import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class CreatePostFragment extends BaseFragment implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate {

    private ImageView img_first_image;
    private RecyclerView rv_images;
    private List<NearByPlaceObject> listOfPlaces;
    private CreatePostLocationAdapter locations_adapter;
    private LinearLayoutManager locations_layout_manager;
    private EditText edt_caption;
    private Button btn_post;
    private LinearLayout lyt_image;
    private ImageView btn_play;

    private ImageView img_camera,img_location,img_video;
    private String imagePath = "";
    private String videoPath = "";
    private String newPath = "";
    private File imageFile,videoFile;
    private int media_type = 0;
    private boolean isVideoStarted = false;
    private boolean is_video_ended = true;
    private int stopPosition = 0;
    private PullRefreshLayout refreshLayout;
    private TextView tv_location_desc;
    private boolean is_location_clicked = false;
    private LinearLayout root;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_post,container,false);
    }

    @Override
    public void init_views() {
        super.init_views();
        //ImageView
        img_first_image = base.findViewById(R.id.create_post_first_image);
        img_camera = base.findViewById(R.id.create_post_pick_image);
        img_location = base.findViewById(R.id.create_post_pick_location);
        img_video = base.findViewById(R.id.create_post_pick_play);
        //RecyclerView
        rv_images = base.findViewById(R.id.create_post_locations_recycler);
        //EditText
        edt_caption = base.findViewById(R.id.create_post_caption_edt);
        //Button
        btn_post = base.findViewById(R.id.create_post_upload_btn);
        //LinearLayout
        lyt_image = base.findViewById(R.id.create_post_image_layout);
        root = base.findViewById(R.id.layout);
        //PullRefreshLayout
        refreshLayout = base.findViewById(R.id.swipeRefreshLayout);
        refreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        refreshLayout.setColor(getResources().getColor(R.color.colorPrimary));
        //TextView
        tv_location_desc = base.findViewById(R.id.create_post_location_desc);

    }

    @Override
    public void init_events() {
        img_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rv_images.setVisibility(View.GONE);
                tv_location_desc.setVisibility(View.GONE);
                is_location_clicked = false;
                ChooseSelectionDialog dialog = new ChooseSelectionDialog(base, "camera", new ISelection() {
                    @Override
                    public void onTakeCameraClicked() {
                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        imageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                "test.jpg");
                        Uri tempUri = Uri.fromFile(imageFile);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT,tempUri);
                        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,1);
                        startActivityForResult(intent,100);
                    }

                    @Override
                    public void onTakeVideoClicked() {

                    }

                    @Override
                    public void onCameraGalleryClicked() {
                        /*
                        BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.dma.app.taikme.provider")
                                .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                                .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                                .build();
                        singleSelectionPicker.show(getChildFragmentManager(),"picker");
                         */

                        ImagePicker.Companion.with(CreatePostFragment.this)
                                .galleryOnly()
                                //.crop(16f,9f)	    			//Crop image(Optional), Check Customization for more option
                                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                                .start();
                    }

                    @Override
                    public void onVideoGalleryClicked() {

                    }
                });
                dialog.show();
            }
        });
        img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rv_images.setVisibility(View.GONE);
                tv_location_desc.setVisibility(View.GONE);
                is_location_clicked = false;
            ChooseSelectionDialog dialog = new ChooseSelectionDialog(base, "video", new ISelection() {
                @Override
                public void onTakeCameraClicked() {

                }

                @Override
                public void onTakeVideoClicked() {
                    Intent intent = new Intent(base,RecordVideoActivity.class);
                    startActivityForResult(intent,102);
                }

                @Override
                public void onCameraGalleryClicked() {

                }

                @Override
                public void onVideoGalleryClicked() {
                    Intent intent = new Intent();
                    intent.setType("video/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Video"),101);
                }
            });
            dialog.show();
            }
        });
        img_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("dfrvdv", "onClick: "+SharedPrefManager.getInstance(base).getLat()+" "+SharedPrefManager.getInstance(base).getLng());
                if (SharedPrefManager.getInstance(base).getLat().equals("")||
                SharedPrefManager.getInstance(base).getLat().equals("0.0")||
                SharedPrefManager.getInstance(base).getLng().equals("")||
                SharedPrefManager.getInstance(base).getLng().equals("0.0")){
                    Toast.makeText(base, getResources().getString(R.string.create_post_no_location), Toast.LENGTH_SHORT).show();
                }else {
                    is_location_clicked = true;
                    media_type = 3;
                    rv_images.setVisibility(View.VISIBLE);
                    tv_location_desc.setText(getResources().getString(R.string.create_post_pull));
                    tv_location_desc.setVisibility(View.VISIBLE);
                }
            }
        });

        refreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (is_location_clicked) {
                    listOfPlaces.clear();
                    locations_adapter = new CreatePostLocationAdapter(base,listOfPlaces);
                    rv_images.setAdapter(locations_adapter);
                    rv_images.getItemAnimator().setChangeDuration(0);
                    callNearPlacesAPI();
                }}
        });

        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("fgkjfkjg", "onClick: "+imagePath);
                Log.i("media_typeee", "onClick: "+media_type);
                if (edt_caption.getText().toString().equals("")){
                    Toast.makeText(base, getResources().getString(R.string.create_post_no_caption), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (media_type == 0){
                    Toast.makeText(base, getString(R.string.create_post_no_media), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (media_type == 1){
                    if (imagePath.equals("")||imagePath==null){
                        Toast.makeText(base, getResources().getString(R.string.create_event_no_image), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Toast.makeText(base, getResources().getString(R.string.create_post_creating), Toast.LENGTH_SHORT).show();
                    BaseFunctions.uploadPost(base,edt_caption.getText().toString(),PostObject.TYPE_PHOTO,edt_caption.getText().toString(),
                            "","",imagePath);
                    /*
                    Intent intent = new Intent(base, UploadStoryService.class);
                    intent.putExtra("service_type","post");
                    intent.putExtra("topic_id","1");
                    intent.putExtra("caption",edt_caption.getText().toString());
                    intent.putExtra("post_type",PostObject.TYPE_PHOTO);
                    intent.putExtra("filePath",imagePath);
                    base.startService(intent);
                    **/
                    base.open_fragment(new HomeFragment());
                }else if (media_type == 2){
                    if (newPath.equals("")||newPath==null){
                        Toast.makeText(base, getResources().getString(R.string.create_post_no_video), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Toast.makeText(base, getResources().getString(R.string.create_post_creating), Toast.LENGTH_SHORT).show();
                    BaseFunctions.uploadPost(base,edt_caption.getText().toString(),PostObject.TYPE_VIDEO,edt_caption.getText().toString(),
                            "","",newPath);
                    /*
                    Intent intent = new Intent(base, UploadStoryService.class);
                    intent.putExtra("service_type","post");
                    intent.putExtra("topic_id","1");
                    intent.putExtra("caption",edt_caption.getText().toString());
                    intent.putExtra("post_type",PostObject.TYPE_VIDEO);
                    intent.putExtra("filePath",newPath);
                    base.startService(intent);
                    **/
                    base.open_fragment(new HomeFragment());
                }else if (media_type == 3){
                    if (locations_adapter.get_selected_place()!=0&&locations_adapter.get_selected_location_id()!=0) {
                        callAddPostAPI();
                        //BaseFunctions.uploadPost(base, edt_caption.getText().toString(), PostObject.TYPE_CHECK_IN, edt_caption.getText().toString(),
                          //      String.valueOf(locations_adapter.get_selected_place()),String.valueOf(locations_adapter.get_selected_location_id()), "");
                    }else {
                        Toast.makeText(base, getResources().getString(R.string.create_post_no_place_selected), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        img_first_image.setImageResource(R.drawable.view1);
        init_recycler();
    }

    private void init_recycler(){
        listOfPlaces = new ArrayList<>();
        locations_adapter = new CreatePostLocationAdapter(base,listOfPlaces);
        locations_layout_manager = new LinearLayoutManager(base,RecyclerView.VERTICAL,false);
        rv_images.setLayoutManager(locations_layout_manager);
        rv_images.setAdapter(locations_adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        switch (requestCode){
            case 100:{
                if (resultCode == RESULT_OK) {
                    Log.i("fgkjfkjg", "onActivityResult: beforeCheck "+imageFile.getPath());
                    if (imageFile.exists()){
                        Log.i("fgkjfkjg", "onActivityResult: afterCheck "+imageFile.getPath());
                        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getPath());
                        img_first_image.setImageBitmap(bitmap);
                        img_first_image.setVisibility(View.VISIBLE);
                        media_type = 1;
                        compressImage(imageFile);
                    }else {
                        Toast.makeText(base, "Image file does not exists", Toast.LENGTH_SHORT).show();
                    }
                }
            }break;
            case 101:{
                if (resultCode == RESULT_OK) {
                    Uri selectedImageUri = data.getData();
                    videoPath = getPath(base,selectedImageUri);
                    File old_file = new File(videoPath);
                    int file_size = Integer.parseInt(String.valueOf(old_file.length()/1024));
                    if (file_size>3000){
                        Toast.makeText(base, getResources().getString(R.string.create_post_large), Toast.LENGTH_LONG).show();
                    }else {
                        Intent intent = new Intent(base,ViewVideoActivity.class);
                        intent.putExtra("video_path",videoPath);
                        startActivityForResult(intent,103);
                    }
                }
            }break;
            case 102:{
                if (resultCode == RESULT_OK){
                    videoPath = data.getStringExtra("video_path");
                    if (videoPath!=null){
                        File file = new File(videoPath);
                        if (file.exists()){
                            img_first_image.setVisibility(View.GONE);
                            Intent intent = new Intent(base,ViewVideoActivity.class);
                            intent.putExtra("video_path",videoPath);
                            startActivityForResult(intent,103);
                        }else {
                            Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                }
            }break;
            case 103:{
                if (resultCode == RESULT_OK){
                    videoPath = data.getStringExtra("video_path");
                    File file = new File(videoPath);
                    final ProgressDialog progressDialog = new ProgressDialog(base);
                    progressDialog.setMessage(getResources().getString(R.string.create_status_prepare_video));
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    newPath = String.format("/sdcard/%d.mp4", System.currentTimeMillis());
                    File newFile = new File(newPath);
                    GiraffeCompressor.init(base);
                    GiraffeCompressor.create("ffmpeg")
                            .input(file)
                            .output(newFile)
                            .bitRate(690000)
                            .resizeFactor(1.0f)
                            .ready()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Emitter<GiraffeCompressor.Result>() {
                                @Override
                                public void setSubscription(Subscription s) {

                                }

                                @Override
                                public void setCancellation(Cancellable c) {

                                }

                                @Override
                                public long requested() {
                                    return 0;
                                }

                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onNext(GiraffeCompressor.Result s) {
                                    progressDialog.cancel();
                                    media_type = 2;
                                }
                            });
                }
            }break;
            default:{
                if (resultCode == Activity.RESULT_OK) {
                    base.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String path = ImagePicker.Companion.getFilePath(data);
                            Log.i("dfbdhb", "onActivityResult: "+path);
                            //Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
                            img_first_image.setVisibility(View.VISIBLE);
                            img_first_image.setImageURI(Uri.fromFile(ImagePicker.Companion.getFile(data)));
                            media_type = 1;
                            imagePath = path;
                        }
                    });
                    //compressImage(new File(path));
                }
            }
        }
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {
                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static MultipartBody.Part getMultiPartBody(String key, String mMediaUrl) {
        if (mMediaUrl != null) {
            File file = new File(mMediaUrl);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            return MultipartBody.Part.createFormData(key, file.getName(), requestFile);
        } else {
            return MultipartBody.Part.createFormData(key, "");
        }
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        img_first_image.setImageURI(uri);
        img_first_image.setVisibility(View.VISIBLE);
        media_type = 1;
        imagePath = uri.getPath();
        compressImage(new File(uri.getPath()));
    }

    @Override
    public void loadImage(File imageFile, ImageView ivImage) {
        Glide.with(base).load(imageFile).into(ivImage);
    }

    private void compressImage(final File imageFile){
       // base.runOnUiThread(new Runnable() {
         //   @Override
         //   public void run() {
                new Compressor(base)
                        .setQuality(75)
                        .setDestinationDirectoryPath("/sdcard/")
                        .compressToFileAsFlowable(imageFile)
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<File>() {
                            @Override
                            public void accept(File file) {
                                if (file.exists()){
                                    Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
                                    //img_first_image.setImageBitmap(bitmap);
                                    imagePath = file.getPath();
                                    media_type = 1;
                                    //img_first_image.setVisibility(View.VISIBLE);
                                    Log.i("fgkjfkjg", "onActivityResult: "+file.getPath());
                                }else {
                                    Toast.makeText(base, "Please select another image", Toast.LENGTH_SHORT).show();
                                }
                                Log.i("media_typeee", "onClick: "+media_type);
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) {
                                throwable.printStackTrace();
                                Log.i("media_typeee", "onClick: "+throwable.getMessage());
                            }
                        });
           // }
       // });
    }

    private void callNearPlacesAPI(){
        refreshLayout.setRefreshing(true);
        PlacesAPIsClass.getNearPlaces(
                base,
                BaseFunctions.getDeviceId(base),
                SharedPrefManager.getInstance(base).getLat(),
                SharedPrefManager.getInstance(base).getLng(),
                10000,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        refreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onResponse(Object json) {
                        refreshLayout.setRefreshing(false);
                        String j = new Gson().toJson(json);
                        NearByPlaceObject[] success = new Gson().fromJson(j,NearByPlaceObject[].class);
                        if (success.length>0){
                            for (NearByPlaceObject o : success){
                                listOfPlaces.add(o);
                                locations_adapter.notifyDataSetChanged();
                            }
                        }else {
                            tv_location_desc.setText(getResources().getString(R.string.create_post_no_locations));
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        refreshLayout.setRefreshing(false);
                        Toast.makeText(base, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    private void callAddPostAPI(){
        PostsAPIsClass.addPost(base,
                BaseFunctions.getDeviceId(base),
                PostObject.TYPE_CHECK_IN,
                edt_caption.getText().toString(),
                edt_caption.getText().toString(),
                String.valueOf(locations_adapter.get_selected_place()),
                String.valueOf(locations_adapter.get_selected_location_id()),
                "",
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        base.open_fragment(new HomeFragment());
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAddPostAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
