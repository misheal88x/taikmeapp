package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import org.androidannotations.annotations.rest.Head;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CustomAudienceAPIs {

    @GET("custom_audience")
    Call<BaseResponse> get_all(@Header("Accept")String accept,
                               @Header("device_id") String device_id,
                               @Query("page") int page);
    @FormUrlEncoded
    @POST("custom_audience")
    Call<BaseResponse> create(@Header("Accept") String accept,
                              @Header("device_id") String device_id,
                              @Field("en_name") String en_name,
                              @Field("ar_name") String ar_name,
                              @Field("gender") String gender,
                              @Field("location_id") int location_id,
                              @Field("prefer") String prefer,
                              @Field("character") String character,
                              @Field("age_range_from") int age_range_from,
                              @Field("age_range_to") int age_range_to,
                              @Field("going_on") String going_on,
                              @Field("best_cuisine") String best_cuisine,
                              @Field("second_best_cuisine") String second_best_cuisine,
                              @Field("third_best_cuisine") String third_best_cuisine,
                              @Field("best_music_gender") String best_music_gender,
                              @Field("second_best_music_gender") String second_best_music_gender,
                              @Field("third_best_music_gender") String third_best_music_gender,
                              @Field("follow_you") String follow_you);
}
