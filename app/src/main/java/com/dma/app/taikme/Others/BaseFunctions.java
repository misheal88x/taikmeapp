package com.dma.app.taikme.Others;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import android.os.Build;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.ILoadImage;
import com.dma.app.taikme.Interfaces.IOnDateSelected;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.glide.slider.library.svg.GlideApp;
import com.google.gson.Gson;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.dma.app.taikme.Others.BaseRetrofit.onResponse;

/**
 * Created by Misheal on 11/12/2019.
 */

public class BaseFunctions {
    public static boolean isOnline(Context context){
        try{
            ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conMgr != null){
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null){
                    return false;
                }else {
                    return true;
                }
            }else {
                return false;
            }
        }catch (Exception e){
            Toast.makeText(context, "Crashed because of isOnline function", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
    public static String dateExtractor(String datetime){
        String myDate = "";
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!='T'){
                myDate+=datetime.charAt(i);
            }else {
                break;
            }
        }
        return myDate;
    }

    public static String dateExtractorOld(String datetime){
        String myDate = "";
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!=' '){
                myDate+=datetime.charAt(i);
            }else {
                break;
            }
        }
        return myDate;
    }

    public static String timeExtractor(String datetime){
        String myTime = "";
        boolean isTime = false;
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!='T'&&datetime.charAt(i)!='.'&&isTime){
                myTime+=datetime.charAt(i);
            }else if (datetime.charAt(i)=='T'){
                isTime = true;
            }else {
                isTime = false;
            }
        }
        return myTime;
    }

    public static String formatSeconds(long totalSecs) {
        long hours = (int) ((totalSecs) / 3600);
        long minutes = (int) ((totalSecs % 3600) / 60);
        long seconds = (int) (totalSecs % 60);
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static String languageToNumberConverter(String in){
        String out = "";
        if (in.equals("en")){
            out = "1";
        }else {
            out = "2";
        }
        return out;
    }

    public static String numberToLanguageConverter(String in){
        String out = "";
        if (in.equals("1")){
            out = "en";
        }else {
            out = "ar";
        }
        return out;
    }

    public static String getDeviceId(Context context){
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public static void setGlideImageWithoutCrop(Context context, ImageView image, String url){
        try{
            GlideApp.with(context).load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(image);
        }catch (Exception e){}
    }

    public static void setGlideImage(Context context, ImageView image, String url){
        try{
            GlideApp.with(context).load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(600, 230)
                    .apply(RequestOptions.bitmapTransform(new RoundedCorners(16)))
                    //.placeholder(R.drawable.placeholder)
                    .centerCrop().into(image);
        }catch (Exception e){}
    }
    public static void setGlideImageWithEvents(Context context, ImageView image, String url, final ILoadImage iLoadImage){
        try{
            GlideApp.with(context).load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //.placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            iLoadImage.onFailed();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                       DataSource dataSource, boolean isFirstResource) {
                            iLoadImage.onLoaded();
                            return false;
                        }
                    })
                    .into(image);
        }catch (Exception e){}
    }
    public static void setGlideDrawableImage(Context context, ImageView image, int drawable){
        try{
            GlideApp.with(context).load(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(600, 230)
                    .apply(RequestOptions.bitmapTransform(new RoundedCorners(16)))
                    //.placeholder(R.drawable.placeholder)
                    .centerCrop().into(image);
        }catch (Exception e){}
    }
    public static void setGlideFileImage(Context context, ImageView image, File file){
        try{
            GlideApp.with(context).load(file)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(600, 230)
                    .apply(RequestOptions.bitmapTransform(new RoundedCorners(16)))
                    //.placeholder(R.drawable.placeholder)
                    .centerCrop().into(image);
        }catch (Exception e){}
    }

    public static void processResponse(Response<BaseResponse> response, IResponse onResponse, Context context){
        if (response.code() == 200){
            if (response.body()!= null){
                if (response.body().getError_code() == -1){
                    onResponse.onResponse(response.body().getData());
                }else {
                    onResponse.onResponse();
                    try {
                        Toast.makeText(context, response.body().getError_message(), Toast.LENGTH_SHORT).show();
                    }catch (Exception e){}
                }
            }else {
                onResponse.onResponse();
                Toast.makeText(context, context.getResources().getString(R.string.error_service), Toast.LENGTH_SHORT).show();
            }
        }else if (response.code() == 500){
            onResponse.onResponse();
            //Toast.makeText(context, context.getResources().getString(R.string.internal_error), Toast.LENGTH_SHORT).show();
        }else if (response.code() == 422){
            onResponse.onResponse();
            //Toast.makeText(context, context.getResources().getString(R.string.missing_data_error), Toast.LENGTH_SHORT).show();
            Toast.makeText(context, context.getResources().getString(R.string.dublicated_user), Toast.LENGTH_SHORT).show();
        }else if (response.code() == 502){
            onResponse.onResponse();
            Toast.makeText(context, context.getResources().getString(R.string.dublicated_user), Toast.LENGTH_SHORT).show();
        }   else {
            onResponse.onResponse();
            try {
                BaseResponse res = new Gson().fromJson(response.errorBody().string(),BaseResponse.class);
                Toast.makeText(context, res.getError_message(), Toast.LENGTH_SHORT).show();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public static void setFrescoImage(SimpleDraweeView image,String url){

        try{
            Uri uri = Uri.parse(url);
            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                    .setResizeOptions(new ResizeOptions(480, 320))
                    .build();
            image.setController(
                    Fresco.newDraweeControllerBuilder()
                            .setOldController(image.getController())
                            .setImageRequest(request)
                            .build());
        }catch (Exception e){}
       /*
        try {
            Uri uri = Uri.parse(url);
            image.setImageURI(uri);
        }catch (Exception e){}

        */
    }
    public static Date stringToDateConverter(String stringDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        try {
            Date d = sdf.parse(stringDate.substring(0,19).replace("T"," "));
            return d;
        } catch (ParseException ex) {
            Log.i("jhjhujh", "stringToDateConverter: "+ex.getLocalizedMessage());
            return null;
        }
    }

    public static Date stringToDateTimeConverter(String stringDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
        try {
            Date d = sdf.parse(stringDate.substring(0,19).replace("T"," "));
            return d;
        } catch (ParseException ex) {
            Log.i("jhjhujh", "stringToDateConverter: "+ex.getLocalizedMessage());
            return null;
        }
    }

    public static String dateToDayConverter(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        String dayOfTheWeek = sdf.format(date);
        return dayOfTheWeek;
    }

    public static String todayDateString(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    public static String todayDateTimeString(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    public static long deferenceBetweenTwoDates(Date d1,Date d2){
        return d1.getTime()-d2.getTime();
    }

    public static String processDate(Context context,String date){
        Date inputDate = stringToDateConverter(date);
        Date todayDate = stringToDateConverter(todayDateString());
        long i = deferenceBetweenTwoDates(todayDate,inputDate);
        long[] res = processMilliseconds(i);
        if (res[0] != 0){
            return String.valueOf(res[0])+" "+context.getResources().getString(R.string.days);
        }else if (res[1] != 0){
            return String.valueOf(res[1])+" "+context.getResources().getString(R.string.hours);
        }else if (res[2] != 0){
            return String.valueOf(res[2])+" "+context.getResources().getString(R.string.minutes);
        }else {
            return context.getResources().getString(R.string.seconds);
        }
    }

    public static String processDateTime(Context context,String date){
        Date inputDate = stringToDateTimeConverter(date);
        Date todayDate = stringToDateTimeConverter(todayDateTimeString());
        long i = deferenceBetweenTwoDates(todayDate,inputDate);
        long[] res = processMilliseconds(i);
        if (res[0] != 0){
            return String.valueOf(res[0])+" "+context.getResources().getString(R.string.days);
        }else if (res[1] != 0){
            return String.valueOf(res[1])+" "+context.getResources().getString(R.string.hours);
        }else if (res[2] != 0){
            return String.valueOf(res[2])+" "+context.getResources().getString(R.string.minutes);
        }else {
            return context.getResources().getString(R.string.seconds);
        }
    }

    public static long[] processMilliseconds(long milli){
        long[] out = new long[]{0,0,0,0};

        long seconds = milli / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        long day = days;
         //long day = TimeUnit.MILLISECONDS.toDays(milli);

         //long hours = TimeUnit.MILLISECONDS.toHours(milli)
                //- TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(milli));

         //long minutes = TimeUnit.MILLISECONDS.toMinutes(milli)
                //- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milli));
         out[0] = day;
         out[1] = hours%24;
         out[2] = minutes%60;
         out[3] = seconds%60;
         return out;
    }

    public static String subString(String input,int start,int end){
        return input.substring(start,end);
    }

    public static String booleanToStringNumber(boolean in){
        String out = "";
        if (in){
            out = "1";
        }else {
            out = "0";
        }
        return out;
    }
    public static boolean stringNumberToBoolean(String in){
        boolean out = false;
        if (in.equals("1")){
            out = true;
        }else {
            out = false;
        }
        return out;
    }

    public static int getSpinnerPosition(AdapterView<?> parent, int i){
        long pos = parent.getItemIdAtPosition(i);
        int position = Integer.valueOf(String.valueOf(pos));
        return position;
    }

    public static void openBrowser(Context context,String url){
        Intent i = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        context.startActivity(i);
    }
    public static void openDialer(Context context,String phone_number){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+phone_number));
        context.startActivity(intent);
    }
    public static void openEmail(Context context,String email){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        String[] addresses = new String[]{email};
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Sawwah");
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public static void openFacebook(Context context,String url){
        context.startActivity(newFacebookIntent(context.getPackageManager(),url));
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public static void openTwitter(Context context,String url){
        Intent intent = null;
        try {
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        }
    }
    public static void sendMessageBySMS(Context context,String message,String errorMessage){
        try {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(Uri.parse("sms:"));
            sendIntent.putExtra("sms_body", message);
            context.startActivity(sendIntent);
        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void sendMessageByMessenger(Context context,String message,String errorMessage){
        try{
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, message);
            sendIntent.setType("text/plain");
            sendIntent.setPackage("com.facebook.orca");
            context.startActivity(sendIntent);
        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void sendMessageByWhatsapp(Context context,String message,String errorMessage){
        try{
            PackageManager pm = context.getPackageManager();
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = message;
            PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            context.startActivity(Intent.createChooser(waIntent, "Share with"));

        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void runAnimation(RecyclerView recyclerView, int type, RecyclerView.Adapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }

    public static void runSlideAnimation(Activity activity){
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void runBackSlideAnimation(Activity activity){
        activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public static void init_spinner(Context context ,Spinner spinner , CustomSpinnerAdapter adapter, List<String> list){
        adapter = new CustomSpinnerAdapter(context,R.layout.spinner_item,list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public static void playMusic(Context context,int music){
        MediaPlayer mPlayer = MediaPlayer.create(context, music);
        mPlayer.start();
    }
    public void stopPlayMusic(MediaPlayer mediaPlayer){
        mediaPlayer.stop();
    }

    public static MultipartBody.Part uploadFileImageConverter(String imageFieldName,String imagePath){
        File file = new File(imagePath);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData(imageFieldName, file.getName(), requestBody);
        return body;
    }

    public static RequestBody uploadFileStringConverter(String input){
        return RequestBody.create(MultipartBody.FORM,input);
    }

    public  static void uploadStory(final Context context, String type, String content, String filePath) {
        try {
            String uploadId =
                    new MultipartUploadRequest(context, "http://taikme.com/taikme2_api/public/api/v1/users/stories")
                            .addHeader("Accept","application/json")
                            .addHeader("device_id",getDeviceId(context))
                            .addHeader("Authorization","Bearer "+SharedPrefManager.getInstance(context).getAccessToken())
                            .addParameter("content",content)
                            .addParameter("type",type)
                            .addFileToUpload(filePath, "file")
                            .setNotificationConfig(new UploadNotificationConfig())
                            .setMaxRetries(2)
                            .startUpload();
            Log.i("test_upload", "uploadMultipart: "+uploadId);
        } catch (Exception exc) {
            Log.i("test_upload","uploadMultipart: "+ exc.getMessage());
        }
    }

    public static void uploadPost(final Context context, String content, String post_type,String caption,String checkin_business_id,
                                  String checkin_business_location_id,String filePath) {
        try {
            String uploadId =
                    new MultipartUploadRequest(context, BaseRetrofit.BASE_URL+"posts")
                            .addHeader("Accept","application/json")
                            .addHeader("device_id",BaseFunctions.getDeviceId(context))
                            .addHeader("Authorization","Bearer "+SharedPrefManager.getInstance(context).getAccessToken())
                            .addParameter("post_type",post_type)
                            .addParameter("content",content)
                            .addParameter("caption",caption)
                            .addParameter("checkin_business_id",checkin_business_id)
                            .addParameter("checkin_business_location_id",checkin_business_location_id)
                            .addFileToUpload(filePath, "file")
                            .setNotificationConfig(new UploadNotificationConfig())
                            .setMaxRetries(2)
                            .startUpload();
            Log.i("test_upload", "uploadMultipart: "+uploadId);
        } catch (Exception exc) {
            Log.i("test_upload","uploadMultipart: "+ exc.getMessage());
        }
    }

    public static void showDatePicker(Context context,final  IOnDateSelected iOnDateSelected){
        final Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog cal = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int month, int day) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, day);

                String year_str = new String();
                String month_str = new String();
                String day_str = new String();
                year_str = String.valueOf(year);
                if (month + 1 < 10) {
                    month_str = "0" + String.valueOf(month + 1);
                } else {
                    month_str = String.valueOf(month + 1);
                }
                if (day < 10) {
                    day_str = "0" + String.valueOf(day);
                } else {
                    day_str = String.valueOf(day);
                }
                String desDate = year_str + "-" + month_str + "-" + day_str;
                iOnDateSelected.onDateSelected(desDate);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        cal.show();
    }

    public static void showTimePicker(Context context,final IOnDateSelected iOnDateSelected){
        final Calendar newCalendar = Calendar.getInstance();
        TimePickerDialog time_picker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hour, int minute) {
                String hour_str = new String();
                String minute_str = new String();
                if (hour<10){
                    hour_str = "0"+String.valueOf(hour);
                }else {
                    hour_str = String.valueOf(hour);
                }
                if (minute<10){
                    minute_str = "0"+String.valueOf(minute);
                }else {
                    minute_str = String.valueOf(minute);
                }
                String fullTime = hour_str+":"+minute_str+":00";
                iOnDateSelected.onDateSelected(fullTime);

            }
        },newCalendar.get(Calendar.HOUR_OF_DAY),newCalendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(context));
        time_picker.show();
    }

    public static void showNotification(Context context,
                                        PendingIntent pendingIntent,
                                        String message,
                                        NotificationCompat.Builder builder,
                                        NotificationManager notificationManager,
                                        String NOTIFICATION_CHANNEL_ID,
                                        int MY_NOTIFICATION_ID){
        if (pendingIntent!=null){
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setTicker("new notification")
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.logo)
                    .setVibrate(new long[]{500, 500, 500});
        }else {
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setTicker("new notification")
                    .setWhen(System.currentTimeMillis())
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.logo)
                    .setVibrate(new long[]{500, 500, 500});
        }
        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        //notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNAEL", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{500, 500, 500});
            assert notificationManager != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        assert notificationManager != null;
        notificationManager.notify(MY_NOTIFICATION_ID, builder.build());
    }
}
