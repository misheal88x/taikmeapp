package com.dma.app.taikme.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.FollowersAPIsClass;
import com.dma.app.taikme.APIsClass.PlacesAPIsClass;
import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Adapters.PlacesReviewedAdapter;
import com.dma.app.taikme.Adapters.PlacesVisitedAdapter;
import com.dma.app.taikme.Adapters.ProfileMediaAdapter;
import com.dma.app.taikme.Adapters.ProfileReviewedPlacesAdapter;
import com.dma.app.taikme.Adapters.ProfileVisitedPlacesAdapter;
import com.dma.app.taikme.Dialogs.ChangeProfileImageDialog;
import com.dma.app.taikme.Dialogs.EditNameDialog;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IProfile;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.PlacesResponse;
import com.dma.app.taikme.Models.ProfileMediaObject;
import com.dma.app.taikme.Models.ProfileMediaResponse;
import com.dma.app.taikme.Models.ProfileNewResponse;
import com.dma.app.taikme.Models.ProfilePlaceObject;
import com.dma.app.taikme.Models.ProfileResponse;
import com.dma.app.taikme.Models.ProfileReviewObject;
import com.dma.app.taikme.Models.ReviewedPlaceObject;
import com.dma.app.taikme.Models.ReviewedPlacesResponse;
import com.dma.app.taikme.Models.UpdateNameResponse;
import com.dma.app.taikme.Models.UserObject;
import com.dma.app.taikme.Models.VisitedPlaceObject;
import com.dma.app.taikme.Models.VisitedPlacesResponse;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.soundcloud.android.crop.Crop;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Misheal on 19/11/2019.
 */

public class ProfileFragment extends BaseFragment {

    private LinearLayout root;
    private ImageView img_cover;
    private RelativeLayout btn_add_cover,btn_edit_name;
    private ImageView img_edit_name;
    private TextView tv_cover_desc,tv_following,tv_followers;
    private CircleImageView img_profile_image;
    private ImageView img_add_profile_image;
    private TextView tv_name;
    private ImageView btn_location,btn_star,btn_play;
    private AVLoadingIndicatorView pb_loading;
    private TextView tv_section_title;
    private TextView tv_section_desc;
    private RecyclerView rv_recycler;
    private ProfileVisitedPlacesAdapter placesVisitedAdapter;
    private ProfileReviewedPlacesAdapter placesReviewedAdapter;
    private ProfileMediaAdapter mediaAdapter;
    private List<ReviewedPlaceObject> listOfPlacesReviewed;
    private List<VisitedPlaceObject> listOfPlacesVisited;
    private List<ProfileMediaObject> listOfMedia;
    private LinearLayoutManager layoutManager;
    private NestedScrollView scrollView;
    private AVLoadingIndicatorView pb_more;
    private boolean is_followed = false;
    //private int currentPage = 1;
    private int media_current_page = 1;
    private int review_current_page = 1;
    private int places_current_page = 1;
    private boolean continue_paginate = true;
    //private int per_page = 20;
    private int media_per_page = 20;
    private int place_per_page = 20;
    private int review_per_page = 20;
    private int per_page = 20;
    private int selected_list = 0;
    private boolean profile_set = false;

    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    private static final int PICK_FROM_GALLERY = 1;
    private String imagePath;
    private int selected_image_for_upload = 0;
    private int profile_id = 0;
    private boolean my_data = true;
    private String received_profile_image = "";
    private String received_name = "";
    private String received_city_name = "";
    private String received_rate = "";
    private RelativeLayout private_layout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile,container,false);
    }

    @Override
    public void init_views() {
        super.init_views();
        //LinearLayout
        root = base.findViewById(R.id.profile_layout);
        //ImageView
        img_cover = base.findViewById(R.id.profile_cover_image);
        img_add_profile_image = base.findViewById(R.id.profile_image_cross);
        btn_location = base.findViewById(R.id.profile_location_btn);
        btn_star = base.findViewById(R.id.profile_star_btn);
        btn_play = base.findViewById(R.id.profile_play_btn);
        img_edit_name = base.findViewById(R.id.profile_name_icon);
        //CircleImageView
        img_profile_image = base.findViewById(R.id.profile_image);
        //TextView
        tv_cover_desc = base.findViewById(R.id.profile_cover_txt);
        tv_section_title = base.findViewById(R.id.profile_section_title);
        tv_section_desc = base.findViewById(R.id.profile_section_desc);
        tv_name = base.findViewById(R.id.profile_name);
        tv_followers = base.findViewById(R.id.profile_followers);
        tv_following = base.findViewById(R.id.profile_following);
        //Recycler View
        rv_recycler = base.findViewById(R.id.profile_recycler);
        //RelativeLayout
        btn_add_cover = base.findViewById(R.id.profile_cover_add_btn);
        btn_edit_name = base.findViewById(R.id.profile_name_layout);
        private_layout = base.findViewById(R.id.private_layout);
        //NestedScrollView
        scrollView = base.findViewById(R.id.profile_scrollview);
        //AVLoadingIndicator
        pb_more = base.findViewById(R.id.profile_more);
        pb_loading = base.findViewById(R.id.profile_section_loading);
        try{
            profile_id = this.getArguments().getInt("profile_id");
            if (profile_id == 0){
                my_data = true;
            }else {
                if (profile_id == SharedPrefManager.getInstance(base).getUser().getId()){
                    my_data = true;
                }else {
                    my_data = false;
                }
            }
        }catch (Exception e){
            my_data = true;

        }
    }

    @Override
    public void init_events() {
        btn_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_list!=1){
                    selected_list = 1;
                    continue_paginate = true;
                    tv_section_title.setText(getResources().getString(R.string.profile_places));
                    tv_section_desc.setText(getString(R.string.profile_no_places));
                    btn_location.setImageResource(R.drawable.ic_circle_location_primary);
                    btn_star.setImageResource(R.drawable.ic_circle_star_grey);
                    btn_play.setImageResource(R.drawable.ic_circle_play_grey);
                    tv_section_desc.setVisibility(View.GONE);
                    listOfPlacesVisited = new ArrayList<>();
                    places_current_page = 1;
                    placesVisitedAdapter = new ProfileVisitedPlacesAdapter(base,listOfPlacesVisited);
                    rv_recycler.setAdapter(placesVisitedAdapter);
                    if (my_data){
                        callVisitedPlacesAPI(places_current_page,0,String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                    }else {
                        callVisitedPlacesAPI(places_current_page,0,String.valueOf(profile_id));
                    }
                }
            }
        });
        btn_star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_list!=2){
                    selected_list = 2;
                    continue_paginate = true;
                    tv_section_title.setText(getResources().getString(R.string.profile_reviews));
                    tv_section_desc.setText(getString(R.string.profile_no_places_reviewed));
                    btn_location.setImageResource(R.drawable.ic_circle_location_grey);
                    btn_star.setImageResource(R.drawable.ic_circle_star_primary);
                    btn_play.setImageResource(R.drawable.ic_circle_play_grey);
                    tv_section_desc.setVisibility(View.GONE);
                    listOfPlacesReviewed = new ArrayList<>();
                    review_current_page = 1;
                    placesReviewedAdapter = new ProfileReviewedPlacesAdapter(base,listOfPlacesReviewed);
                    rv_recycler.setAdapter(placesReviewedAdapter);
                    if (my_data){
                        callReviewedPlacesAPI(review_current_page,0,String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                    }else {
                        callReviewedPlacesAPI(review_current_page,0,String.valueOf(profile_id));
                    }
                }
            }
        });
        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_list!=3){
                    selected_list = 3;
                    continue_paginate = true;
                    tv_section_title.setText(getResources().getString(R.string.profile_media));
                    tv_section_desc.setText(getString(R.string.profile_no_media));
                    btn_location.setImageResource(R.drawable.ic_circle_location_grey);
                    btn_star.setImageResource(R.drawable.ic_circle_star_grey);
                    btn_play.setImageResource(R.drawable.ic_circle_play_primary);
                    tv_section_desc.setVisibility(View.GONE);
                    listOfMedia = new ArrayList<>();
                    media_current_page = 1;
                    mediaAdapter = new ProfileMediaAdapter(
                            base,
                            listOfMedia,
                            received_profile_image,
                            received_name,
                            received_city_name,
                            Float.valueOf(received_rate),
                            "without");
                    rv_recycler.setAdapter(mediaAdapter);
                    if (my_data){
                        callMediaAPI(media_current_page,0,String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                    }else {
                        callMediaAPI(media_current_page,0,String.valueOf(profile_id));
                    }

                }
            }
        });
        btn_add_cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (my_data) {
                    selected_image_for_upload = 1;
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PICK_FROM_GALLERY);
                }

            }
        });
        img_add_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (my_data) {
                    selected_image_for_upload = 2;
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PICK_FROM_GALLERY);
                }

            }
        });

        img_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (my_data){
                    if (profile_set){
                        ChangeProfileImageDialog dialog = new ChangeProfileImageDialog(base, new IProfile() {
                            @Override
                            public void onChange() {
                                selected_image_for_upload = 2;
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        PICK_FROM_GALLERY);
                            }

                            @Override
                            public void onRemove() {
                                callRemoveProfileImageAPI();
                            }
                        });
                        dialog.show();
                    }else {
                        selected_image_for_upload = 2;
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PICK_FROM_GALLERY);
                    }
                }
            }
        });

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        switch (selected_list){
                            case 1:{
                                if (listOfPlacesVisited.size()>=place_per_page){
                                    if (continue_paginate){
                                        places_current_page++;
                                        if (my_data){
                                            callVisitedPlacesAPI(places_current_page,1,String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                                        }else {
                                            callVisitedPlacesAPI(places_current_page,1,String.valueOf(profile_id));
                                        }
                                    }
                                }
                            }break;
                            case 2:{
                                if (listOfPlacesReviewed.size()>=review_per_page){
                                    if (continue_paginate){
                                        review_current_page++;
                                        if (my_data){
                                            callReviewedPlacesAPI(review_current_page,1,String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                                        }else {
                                            callReviewedPlacesAPI(review_current_page,1,String.valueOf(profile_id));
                                        }
                                    }
                                }
                            }break;
                            case 3:{
                                if (listOfMedia.size()>=media_per_page){
                                    if (continue_paginate){
                                        media_current_page++;
                                        if (my_data){
                                            callMediaAPI(media_current_page,1,String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                                        }else {
                                            callMediaAPI(media_current_page,1,String.valueOf(profile_id));
                                        }
                                    }
                                }
                            }break;
                        }
                    }
                }
            }
        });
        btn_edit_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (my_data){
                    String username = SharedPrefManager.getInstance(base).getUser().getName();
                    EditNameDialog dialog = new EditNameDialog(base, username, new IEditName() {
                        @Override
                        public void onNameWritten(String name) {
                            callUpdateNameAPI(name);
                        }
                    });
                    dialog.show();
                }else {
                    if (is_followed){
                        callFollowAPI(profile_id+"","no");
                    }else {
                        callFollowAPI(profile_id+"","yes");
                    }

                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        if (!my_data){
            btn_add_cover.setVisibility(View.GONE);
            img_add_profile_image.setVisibility(View.GONE);
        }
        if (my_data){
            btn_edit_name.setBackground(getResources().getDrawable(R.drawable.primary_circle));
            img_edit_name.setImageResource(R.drawable.ic_minus);
            callProfileAPI(String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
        }else {
            if (is_followed){
                btn_edit_name.setBackground(getResources().getDrawable(R.drawable.dark_grey_circle));
                img_edit_name.setImageResource(R.drawable.ic_minus);
            }else {
                btn_edit_name.setBackground(getResources().getDrawable(R.drawable.primary_circle));
                img_edit_name.setImageResource(R.drawable.ic_plus);
            }
            callProfileAPI(String.valueOf(profile_id));
        }
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    private void init_recycler(){
        //selected_list = 1;
        //btn_location.setImageResource(R.drawable.ic_circle_location_primary);
        //continue_paginate = true;
        //tv_section_title.setText(getResources().getString(R.string.profile_places));
        //tv_section_desc.setText(getString(R.string.profile_no_places));
        //listOfPlacesVisited = new ArrayList<>();
        //currentPage = 1;
        //placesVisitedAdapter = new PlacesVisitedAdapter(base,listOfPlacesVisited);
        layoutManager = new LinearLayoutManager(base,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_recycler.setLayoutManager(layoutManager);
        //rv_recycler.setAdapter(placesVisitedAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.i("call_sequests", "onRequestPermissionsResult: "+requestCode);
        switch (requestCode) {
            case PICK_FROM_GALLERY: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    //Crop.pickImage(base, this);
                    ImagePicker.Companion.with(ProfileFragment.this)
                            .galleryOnly()
                            //.crop(16f,9f)	    			//Crop image(Optional), Check Customization for more option
                            .compress(1024)			//Final image size will be less than 1 MB(Optional)
                            .start();
                } else {
                    Toast.makeText(base, getResources().getString(R.string.no_permissions), Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (resultCode==RESULT_OK){
            if (requestCode == Crop.REQUEST_PICK) {
                base.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Uri source_uri = data.getData();
                        Uri destination_uri = Uri.fromFile(getTempFile());
                        if (selected_image_for_upload == 1){
                            Crop.of(source_uri,destination_uri).withAspect(16,9).start(base,ProfileFragment.this);
                        }else if (selected_image_for_upload == 2){
                            Crop.of(source_uri,destination_uri).asSquare().start(base,ProfileFragment.this);
                        }
                        String filePath=  Environment.getExternalStorageDirectory()
                                + "/"+"temporary_holder.jpg";
                        imagePath = filePath;
                    }
                });
            }else if (requestCode == Crop.REQUEST_CROP){
                handle_crop(resultCode,data);
            }else {
                Uri source_uri = Uri.fromFile(ImagePicker.Companion.getFile(data));
                Uri destination_uri = Uri.fromFile(getTempFile());
                if (selected_image_for_upload == 1){
                    Crop.of(source_uri,destination_uri).withAspect(16,9).start(base,ProfileFragment.this);
                }else if (selected_image_for_upload == 2){
                    Crop.of(source_uri,destination_uri).asSquare().start(base,ProfileFragment.this);
                }
                String filePath=  Environment.getExternalStorageDirectory()
                        + "/"+"temporary_holder.jpg";
                imagePath = filePath;
            }

        }
    }

    private void handle_crop(int code, final Intent data){
        if (code == RESULT_OK){
            base.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (selected_image_for_upload == 1){
                        img_cover.setImageDrawable(null);
                        img_cover.setImageURI(Crop.getOutput(data));
                        tv_cover_desc.setVisibility(View.GONE);
                        String filePath=  Environment.getExternalStorageDirectory()
                                + "/"+"temporary_holder.jpg";
                        imagePath = filePath;
                        callUpdateCoverAPI();
                    }else if (selected_image_for_upload == 2){
                        img_profile_image.setImageDrawable(null);
                        img_profile_image.setImageURI(Crop.getOutput(data));
                        img_add_profile_image.setVisibility(View.GONE);
                        String filePath=  Environment.getExternalStorageDirectory()
                                + "/"+"temporary_holder.jpg";
                        imagePath = filePath;
                        callUpdateProfileImageAPI();
                    }

                }
            });

        }else if (code == Crop.RESULT_ERROR){
            Toast.makeText(base, getResources().getString(R.string.error_cropping), Toast.LENGTH_SHORT).show();
        }
    }


    private void callUpdateCoverAPI(){
        UserAPIsClass.updateCover(
                base,
                BaseFunctions.getDeviceId(base),
                imagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!= null){
                            String json1 = new Gson().toJson(json);
                            String success = new Gson().fromJson(json1,String.class);
                            if (success!=null){
                                Toast.makeText(base, getResources().getString(R.string.profile_cover_updated), Toast.LENGTH_SHORT).show();
                                UserObject user = SharedPrefManager.getInstance(base).getUser();
                                user.setCover_image(success);
                                SharedPrefManager.getInstance(base).setUser(user);
                            }else {
                                Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                                img_cover.setImageDrawable(null);
                                img_cover.setBackgroundColor(getResources().getColor(R.color.light_grey));
                                tv_cover_desc.setVisibility(View.VISIBLE);
                                imagePath = "";
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        img_cover.setImageDrawable(null);
                        img_cover.setBackgroundColor(getResources().getColor(R.color.light_grey));
                        tv_cover_desc.setVisibility(View.VISIBLE);
                        imagePath = "";
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callUpdateCoverAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callUpdateProfileImageAPI(){
        UserAPIsClass.updateProfileImage(
                base,
                BaseFunctions.getDeviceId(base),
                imagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!= null){
                            String json1 = new Gson().toJson(json);
                            String success = new Gson().fromJson(json1,String.class);
                            if (success!=null){
                                Toast.makeText(base, getResources().getString(R.string.profile_image_updated), Toast.LENGTH_SHORT).show();
                                UserObject user = SharedPrefManager.getInstance(base).getUser();
                                user.setImage(success);
                                SharedPrefManager.getInstance(base).setUser(user);
                                profile_set = true;
                            }else {
                                Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                                img_profile_image.setImageDrawable(null);
                                img_profile_image.setBackgroundColor(getResources().getColor(R.color.light_grey));
                                img_add_profile_image.setVisibility(View.VISIBLE);
                                imagePath = "";
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        img_profile_image.setImageDrawable(null);
                        img_profile_image.setBackgroundColor(getResources().getColor(R.color.light_grey));
                        img_add_profile_image.setVisibility(View.VISIBLE);
                        imagePath = "";
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callUpdateProfileImageAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callFollowAPI(final String id,final String value){
        FollowersAPIsClass.followNew(
                base,
                BaseFunctions.getDeviceId(base),
                "user",
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                if (value.equals("yes")){
                                    Toast.makeText(base, getResources().getString(R.string.follow_person_true), Toast.LENGTH_SHORT).show();
                                    img_edit_name.setImageResource(R.drawable.ic_minus);
                                    btn_edit_name.setBackground(getResources().getDrawable(R.drawable.dark_grey_circle));
                                    is_followed = true;
                                }else {
                                    Toast.makeText(base, getResources().getString(R.string.follow_person_false), Toast.LENGTH_SHORT).show();
                                    img_edit_name.setImageResource(R.drawable.ic_plus);
                                    btn_edit_name.setBackground(getResources().getDrawable(R.drawable.primary_circle));
                                    is_followed = false;
                                }
                            }else {
                                Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callFollowAPI(id,value);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callProfileAPI(final String user_id){
        scrollView.setVisibility(View.GONE);
        UserAPIsClass.getProfile(
                base,
                BaseFunctions.getDeviceId(base),
                user_id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            ProfileNewResponse success = new Gson().fromJson(j,ProfileNewResponse.class);
                            //Cover
                            if (success.getCover_image()!=null&&!success.getCover_image().equals("")){
                                BaseFunctions.setGlideImage(base,img_cover,success.getCover_image());
                                tv_cover_desc.setVisibility(View.GONE);
                            }
                            //Profile
                            if (success.getImage()!=null&&!success.getImage().equals("")){
                                BaseFunctions.setGlideImage(base,img_profile_image,success.getImage());
                                img_add_profile_image.setVisibility(View.GONE);
                                profile_set = true;
                            }
                            received_profile_image = success.getImage();
                            //Username
                            tv_name.setText(success.getName());
                            received_name = success.getName();
                            //Followers
                            tv_followers.setText(success.getFollowers_count()+" "+
                                    getResources().getString(R.string.profile_followers));
                            //Following
                            tv_following.setText(getResources().getString(R.string.profile_following)+" "+
                                    success.getFollowing_count());
                                //City
                                received_city_name = success.getCity_name();
                                //Rate
                                received_rate = success.getReviews_rating();
                                //Is Followed
                                try {
                                    is_followed = success.getIs_following()==0?false:true;
                                }catch (Exception e){}
                                if (!my_data){
                                    if (!is_followed){
                                        img_edit_name.setImageResource(R.drawable.ic_plus);
                                        btn_edit_name.setBackground(getResources().getDrawable(R.drawable.primary_circle));
                                    }else {
                                        img_edit_name.setImageResource(R.drawable.ic_minus);
                                        btn_edit_name.setBackground(getResources().getDrawable(R.drawable.dark_grey_circle));
                                    }
                                }
                                if (my_data){
                                    scrollView.setVisibility(View.VISIBLE);
                                    if (selected_list!=1){
                                        selected_list = 1;
                                        continue_paginate = true;
                                        tv_section_title.setText(getResources().getString(R.string.profile_places));
                                        tv_section_desc.setText(getString(R.string.profile_no_places));
                                        btn_location.setImageResource(R.drawable.ic_circle_location_primary);
                                        btn_star.setImageResource(R.drawable.ic_circle_star_grey);
                                        btn_play.setImageResource(R.drawable.ic_circle_play_grey);
                                        tv_section_desc.setVisibility(View.GONE);
                                        listOfPlacesVisited = new ArrayList<>();
                                        places_current_page = 1;
                                        placesVisitedAdapter = new ProfileVisitedPlacesAdapter(base,listOfPlacesVisited);
                                        rv_recycler.setAdapter(placesVisitedAdapter);
                                        if (my_data){
                                            callVisitedPlacesAPI(places_current_page,0,String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                                        }else {
                                            callVisitedPlacesAPI(places_current_page,0,String.valueOf(profile_id));
                                        }
                                    }
                                }else {
                                        scrollView.setVisibility(View.VISIBLE);
                                        if (selected_list!=1){
                                            selected_list = 1;
                                            continue_paginate = true;
                                            tv_section_title.setText(getResources().getString(R.string.profile_places));
                                            tv_section_desc.setText(getString(R.string.profile_no_places));
                                            btn_location.setImageResource(R.drawable.ic_circle_location_primary);
                                            btn_star.setImageResource(R.drawable.ic_circle_star_grey);
                                            btn_play.setImageResource(R.drawable.ic_circle_play_grey);
                                            tv_section_desc.setVisibility(View.GONE);
                                            listOfPlacesVisited = new ArrayList<>();
                                            places_current_page = 1;
                                            placesVisitedAdapter = new ProfileVisitedPlacesAdapter(base,listOfPlacesVisited);
                                            rv_recycler.setAdapter(placesVisitedAdapter);
                                            if (my_data){
                                                callVisitedPlacesAPI(places_current_page,0,String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                                            }else {
                                                callVisitedPlacesAPI(places_current_page,0,String.valueOf(profile_id));
                                            }
                                        }

                                    if (success.getDevice_setting().getAllow_people_to_follow()==0){
                                        btn_edit_name.setVisibility(View.GONE);
                                        scrollView.setVisibility(View.VISIBLE);
                                    }
                                }
                        }
                    }

                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callProfileAPI(user_id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callMediaAPI(final int page,final int type,final String id){
        if (type == 0){
            pb_loading.smoothToShow();
        }else {
            pb_more.smoothToShow();
        }
        UserAPIsClass.profileMedia(
                base,
                BaseFunctions.getDeviceId(base),
                id,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            ProfileMediaResponse success = new Gson().fromJson(j,ProfileMediaResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    media_per_page = success.getPer_page();
                                    for (ProfileMediaObject o : success.getData()){
                                        listOfMedia.add(o);
                                        mediaAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callMediaAPI( page, type, id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callVisitedPlacesAPI(final int page,final int type, final String id){
        if (type == 0){
            pb_loading.smoothToShow();
        }else {
            pb_more.smoothToShow();
        }
        PlacesAPIsClass.getVisitedPlaces(
                base,
                BaseFunctions.getDeviceId(base),
                id,
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            VisitedPlacesResponse success = new Gson().fromJson(j,VisitedPlacesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (VisitedPlaceObject po : success.getData()){
                                        listOfPlacesVisited.add(po);
                                        placesVisitedAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callVisitedPlacesAPI(page,type,id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callReviewedPlacesAPI(final int page,final int type,final String id){
        if (type == 0){
            pb_loading.smoothToShow();
        }else {
            pb_more.smoothToShow();
        }
        PlacesAPIsClass.getReviewedPlaces(
                base,
                BaseFunctions.getDeviceId(base),
                id,
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            ReviewedPlacesResponse success = new Gson().fromJson(j,ReviewedPlacesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (ReviewedPlaceObject po : success.getData()){
                                        listOfPlacesReviewed.add(po);
                                        placesReviewedAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callReviewedPlacesAPI(page,type,id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            pb_more.smoothToHide();
        }else {
            pb_loading.smoothToHide();
            tv_section_desc.setVisibility(View.VISIBLE);
            rv_recycler.setVisibility(View.GONE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            tv_section_desc.setVisibility(View.GONE);
            rv_recycler.setVisibility(View.VISIBLE);
            pb_loading.setVisibility(View.GONE);
        }else {
            pb_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            tv_section_desc.setVisibility(View.VISIBLE);
            rv_recycler.setVisibility(View.GONE);
            pb_loading.smoothToHide();
        }else {
            pb_more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }

    private void callRemoveProfileImageAPI(){
        UserAPIsClass.removeProfileImage(base,
                BaseFunctions.getDeviceId(base),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        boolean success = new Gson().fromJson(j,Boolean.class);
                        if (success){
                            Toast.makeText(base, getResources().getString(R.string.profile_profile_image_remove_success), Toast.LENGTH_SHORT).show();
                            UserObject user = SharedPrefManager.getInstance(base).getUser();
                            user.setImage("");
                            SharedPrefManager.getInstance(base).setUser(user);
                            img_profile_image.setImageResource(R.drawable.grey_circle);
                            img_add_profile_image.setVisibility(View.VISIBLE);
                        }else {
                            Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(base, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callUpdateNameAPI(final String name){
        UserAPIsClass.updateName(base,
                BaseFunctions.getDeviceId(base),
                name,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        boolean success = new Gson().fromJson(j,boolean.class);
                        if (success){
                            UserObject user = SharedPrefManager.getInstance(base).getUser();
                            user.setName(name);
                            SharedPrefManager.getInstance(base).setUser(user);
                            tv_name.setText(name);
                        }else{
                            Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(base, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
