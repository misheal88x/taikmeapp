package com.dma.app.taikme.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.ReviewedPlaceObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Misheal on 19/11/2019.
 */

public class PlacesReviewedAdapter extends  RecyclerView.Adapter<PlacesReviewedAdapter.ViewHolder> {
    private Context context;
    private List<ReviewedPlaceObject> list;

    public PlacesReviewedAdapter(Context context,List<ReviewedPlaceObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView img_image;
        private TextView tv_name;
        private SimpleRatingBar rb_rate;
        private RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_places_reviewed_image);
            tv_name = view.findViewById(R.id.item_places_reviewed_name);
            rb_rate = view.findViewById(R.id.item_places_reviewed_rate);
            layout = view.findViewById(R.id.item_place_reviewed_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_places_reviewed, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ReviewedPlaceObject place = list.get(position);
        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();
        holder.tv_name.setText(place.getName());
        /*
        if (lan.equals("en")){
            if (place.getEn_name()!=null){
                holder.tv_name.setText(place.getEn_name());
            }else {
                if (place.getAr_name()!=null){
                    holder.tv_name.setText(place.getAr_name());
                }
            }
        }else if (lan.equals("ar")){
            if (place.getAr_name()!=null){
                holder.tv_name.setText(place.getAr_name());
            }else {
                if (place.getEn_name()!=null){
                    holder.tv_name.setText(place.getEn_name());
                }
            }
        }
         */
        BaseFunctions.setGlideImage(context,holder.img_image,place.getThumb_image());
        try {
            holder.rb_rate.setRating(Float.valueOf(place.getBusiness_avg_review()));
        }catch (Exception e){}

        holder.img_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)) {
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra("type", "facility_info");
                    intent.putExtra("place_id", place.getId());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)) {
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra("type", "facility_info");
                    intent.putExtra("place_id", place.getId());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
