package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class PostFatherObject {
    @SerializedName("object_type") private int object_type = 0;
    @SerializedName("shared_object_type") private int shared_object_type = 0;
    @SerializedName("sharer_id") private String sharer_id = "";
    @SerializedName("sharer_name") private String sharer_name = "";
    @SerializedName("sharer_profile_image") private String sharer_profile_image = "";
    @SerializedName("real_object") private PostObject real_object = new PostObject();

    public int getObject_type() {
        return object_type;
    }

    public void setObject_type(int object_type) {
        this.object_type = object_type;
    }

    public int getShared_object_type() {
        return shared_object_type;
    }

    public void setShared_object_type(int shared_object_type) {
        this.shared_object_type = shared_object_type;
    }

    public PostObject getReal_object() {
        return real_object;
    }

    public void setReal_object(PostObject real_object) {
        this.real_object = real_object;
    }

    public String getSharer_id() {
        return sharer_id;
    }

    public void setSharer_id(String sharer_id) {
        this.sharer_id = sharer_id;
    }

    public String getSharer_name() {
        return sharer_name;
    }

    public void setSharer_name(String sharer_name) {
        this.sharer_name = sharer_name;
    }

    public String getSharer_profile_image() {
        return sharer_profile_image;
    }

    public void setSharer_profile_image(String sharer_profile_image) {
        this.sharer_profile_image = sharer_profile_image;
    }
}
