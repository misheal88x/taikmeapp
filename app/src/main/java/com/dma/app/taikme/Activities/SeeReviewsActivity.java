package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dma.app.taikme.APIsClass.CommentsAPIsClass;
import com.dma.app.taikme.APIsClass.ReviewsAPIClass;
import com.dma.app.taikme.Adapters.AllCommentsAdapter;
import com.dma.app.taikme.Adapters.AllReviewsAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.CommentObject;
import com.dma.app.taikme.Models.CommentsResponse;
import com.dma.app.taikme.Models.ReviewNewObject;
import com.dma.app.taikme.Models.ReviewsNewResponse;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class SeeReviewsActivity extends BaseActivity {

    private RelativeLayout toolbar;
    private RelativeLayout back;
    private TextView title;
    private AVLoadingIndicatorView loading,more;
    private TextView desc;
    private RecyclerView recycler;
    private AllReviewsAdapter reviewsAdapter;
    private List<ReviewNewObject> list;
    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;
    private int id;
    private String type;
    private NestedScrollView scrollView;
    private LinearLayout root;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_see_reviews);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        init_recycler();
        id = getIntent().getIntExtra("id",0);
        type = getIntent().getStringExtra("type");
        callReviewsAPI(currentPage,0);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar);
        back = findViewById(R.id.toolbar_back_btn);
        title = findViewById(R.id.toolbar_title);

        loading = findViewById(R.id.loading);
        more = findViewById(R.id.comments_more);

        scrollView = findViewById(R.id.scrollView);

        desc = findViewById(R.id.desc);
        recycler = findViewById(R.id.all_comments_recycler);

        root = findViewById(R.id.layout);
    }

    @Override
    public void init_events() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(SeeReviewsActivity.this);
            }
        });

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callReviewsAPI(currentPage,1);
                            }
                        }
                    }
                }
            }
        });
    }

    private void init_recycler(){
        list = new ArrayList<>();
        reviewsAdapter = new AllReviewsAdapter(SeeReviewsActivity.this,list);
        recycler.setLayoutManager(new LinearLayoutManager(SeeReviewsActivity.this,RecyclerView.VERTICAL,false));
        BaseFunctions.runAnimation(recycler,0,reviewsAdapter);
        recycler.setAdapter(reviewsAdapter);
    }

    @Override
    public void set_fragment_place() {

    }

    private void callReviewsAPI(final int page,final int type){
        if (type == 1){
            more.smoothToShow();
        }else if (type == 0){
            loading.smoothToShow();
            desc.setVisibility(View.GONE);
        }
        ReviewsAPIClass.getReviews(
                SeeReviewsActivity.this,
                BaseFunctions.getDeviceId(SeeReviewsActivity.this),
                this.type,
                String.valueOf(this.id),
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            ReviewsNewResponse success = new Gson().fromJson(j,ReviewsNewResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (ReviewNewObject po : success.getData()){
                                        list.add(po);
                                        reviewsAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callReviewsAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            more.smoothToHide();
        }else {
            loading.smoothToHide();
        }
    }
    private void process_data(int type){
        if (type == 0){
            loading.smoothToHide();
            scrollView.setVisibility(View.VISIBLE);
            desc.setVisibility(View.GONE);
        }else {
            more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            loading.smoothToHide();
            desc.setVisibility(View.VISIBLE);
        }else {
            more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
