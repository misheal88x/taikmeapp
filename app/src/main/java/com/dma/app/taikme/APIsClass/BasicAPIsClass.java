package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.AuthAPIs;
import com.dma.app.taikme.APIs.BasicAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BasicAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void startup(final Context context,
                                      String device_id,
                                      IResponse onResponse1,
                                      final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.startup("application/json",
                device_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void getQuestions(final Context context,
                               String device_id,
                               IResponse onResponse1,
                               final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.getQuestions("application/json",
                device_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
