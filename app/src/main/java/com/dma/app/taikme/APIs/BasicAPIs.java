package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface BasicAPIs {
    @POST("startup")
    Call<BaseResponse> startup(
            @Header("Accept") String accept,
            @Header("device_id") String device_id
    );

    @GET("faq")
    Call<BaseResponse> getQuestions(@Header("Accept") String accept,
                                    @Header("device_id") String device_id);
}
