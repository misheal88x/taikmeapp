package com.dma.app.taikme.Models.HomeModels;

import com.google.gson.annotations.SerializedName;

public class HomePostsObject {
    @SerializedName("metadata") private HomeMetaDataObject metadata = new HomeMetaDataObject();
    @SerializedName("object") private HomePostObject post_object = new HomePostObject();

    public HomeMetaDataObject getMetadata() {
        return metadata;
    }

    public void setMetadata(HomeMetaDataObject metadata) {
        this.metadata = metadata;
    }

    public HomePostObject getPost_object() {
        return post_object;
    }

    public void setObject(HomePostObject object) {
        this.post_object = object;
    }
}
