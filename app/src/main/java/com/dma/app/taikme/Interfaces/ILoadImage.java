package com.dma.app.taikme.Interfaces;

public interface ILoadImage {
    void onLoaded();
    void onFailed();
}
