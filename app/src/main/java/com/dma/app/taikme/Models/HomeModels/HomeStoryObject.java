package com.dma.app.taikme.Models.HomeModels;

import com.google.gson.annotations.SerializedName;

public class HomeStoryObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("content") private String content = "";
    @SerializedName("content_type") private String content_type = "";
    @SerializedName("scope") private Object scope = null;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("user_name") private String  user_name = "";
    @SerializedName("user_profile_image") private String user_profile_image = "";
    @SerializedName("user_profile_thumb_image") private String user_profile_thumb_image = "";
    @SerializedName("user_cover_image") private String user_cover_image = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public Object getScope() {
        return scope;
    }

    public void setScope(Object scope) {
        this.scope = scope;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }

    public String getUser_profile_thumb_image() {
        return user_profile_thumb_image;
    }

    public void setUser_profile_thumb_image(String user_profile_thumb_image) {
        this.user_profile_thumb_image = user_profile_thumb_image;
    }

    public String getUser_cover_image() {
        return user_cover_image;
    }

    public void setUser_cover_image(String user_cover_image) {
        this.user_cover_image = user_cover_image;
    }
}
