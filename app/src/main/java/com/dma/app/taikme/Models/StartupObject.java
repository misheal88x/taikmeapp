package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StartupObject {
    //Done
    @SerializedName("posts_types") private List<StartupItemObject> posts_types = new ArrayList<>();
    //Not used
    @SerializedName("CONTENT_TYPE_IDs") private ContentTypeObject CONTENT_TYPE_IDs = new ContentTypeObject();
    //Done
    @SerializedName("gender_types") private List<StartupItemObject> genders = new ArrayList<>();
    //Done
    @SerializedName("going_out_types") private List<StartupItemObject> going_out_as_type = new ArrayList<>();
    //Done
    @SerializedName("traveling_types") private List<StartupItemObject> traveling_type = new ArrayList<>();
    //Done
    @SerializedName("prefer_types") private List<StartupItemObject> prefer = new ArrayList<>();
    //Replaces by My place better for
    //@SerializedName("my_place_better_for") private List<StartupItemObject> character = new ArrayList<>();
    //Done
    @SerializedName("character_going_to_types") private List<StartupItemObject> going_on = new ArrayList<>();
    //Not used
    @SerializedName("topics") private List<TopicObject> topics = new ArrayList<>();
    //Not used
    @SerializedName("locations") private List<LocationObject> locations = new ArrayList<>();
    //Done
    @SerializedName("feel_to_do") private List<StartupItemObject> feel_to_do = new ArrayList<>();
    //Done
    @SerializedName("music_genre") private List<StartupItemObject> music_gender_type = new ArrayList<>();
    //Done
    @SerializedName("cuisines") private List<StartupItemObject> cuisine_definitions = new ArrayList<>();
    //Done
    @SerializedName("following_types") private List<StartupItemObject> follow_you = new ArrayList<>();
    //Done
    @SerializedName("seating_types") private List<StartupItemObject> business_seating = new ArrayList<>();
    //Done
    @SerializedName("atmosphere_types") private List<StartupItemObject> business_atmosphere = new ArrayList<>();
    //Done
    @SerializedName("kids_area") private KidsAreaObject kids_area = new KidsAreaObject();
    //Done
    @SerializedName("parking") private ParkingObject parking = new ParkingObject();
    //Done
    @SerializedName("handicapped_entrance") private HandicappedEntranceObject handicapped_entrance = new HandicappedEntranceObject();
    //Done
    @SerializedName("delivery_and_takeaway") private DeliveryTakeAwayObject delivery_and_takeaway = new DeliveryTakeAwayObject();
    //Done
    @SerializedName("restrooms") private RestRoomsObject restrooms = new RestRoomsObject();
    //Done
    @SerializedName("price_range") private List<Integer> price_range = new ArrayList<>();
    //Done
    @SerializedName("my_place_better_for") private List<StartupItemObject> my_place_better_for = new ArrayList<>();
    //todo Not exists
    @SerializedName("i_prefer") private List<String> i_prefer = new ArrayList<>();
    //Replaces by My place better for
    //@SerializedName("my_place_better_for") private List<StartupItemObject> i_am = new ArrayList<>();
    //todo Not exists
    @SerializedName("place_categories") private List<CategoryObject> places_categories = new ArrayList<>();
    //Done
    @SerializedName("countries") private List<String> countries = new ArrayList<>();
    //Done
    @SerializedName("lebanon_cities") private List<LebanonCityObject> lebanon_cities = new ArrayList<>();
    //New
    @SerializedName("home_flags_types") private List<StartupItemObject> home_flags_types = new ArrayList<>();
    //New
    @SerializedName("report_state_types") private List<StartupItemObject> report_state_types = new ArrayList<>();
    //New
    @SerializedName("stories_types") private List<StartupItemObject> stories_types = new ArrayList<>();


    public StartupObject() { }

    public List<StartupItemObject> getPosts_types() {
        return posts_types;
    }

    public void setPosts_types(List<StartupItemObject> posts_types) {
        this.posts_types = posts_types;
    }

    public List<TopicObject> getTopics() {
        return topics;
    }

    public void setTopics(List<TopicObject> topics) {
        this.topics = topics;
    }

    public List<LocationObject> getLocations() {
        return locations;
    }

    public void setLocations(List<LocationObject> locations) {
        this.locations = locations;
    }

    public List<StartupItemObject> getFeel_to_do() {
        return feel_to_do;
    }

    public void setFeel_to_do(List<StartupItemObject> feel_to_do) {
        this.feel_to_do = feel_to_do;
    }

    public List<StartupItemObject> getMusic_gender_type() {
        return music_gender_type;
    }

    public void setMusic_gender_type(List<StartupItemObject> music_gender_type) {
        this.music_gender_type = music_gender_type;
    }

    public List<StartupItemObject> getCuisine_definitions() {
        return cuisine_definitions;
    }

    public void setCuisine_definitions(List<StartupItemObject> cuisine_definitions) {
        this.cuisine_definitions = cuisine_definitions;
    }

    public ContentTypeObject getCONTENT_TYPE_IDs() {
        return CONTENT_TYPE_IDs;
    }

    public void setCONTENT_TYPE_IDs(ContentTypeObject CONTENT_TYPE_IDs) {
        this.CONTENT_TYPE_IDs = CONTENT_TYPE_IDs;
    }

    public List<StartupItemObject> getGenders() {
        return genders;
    }

    public void setGenders(List<StartupItemObject> genders) {
        this.genders = genders;
    }

    public List<StartupItemObject> getGoing_out_as_type() {
        return going_out_as_type;
    }

    public void setGoing_out_as_type(List<StartupItemObject> going_out_as_type) {
        this.going_out_as_type = going_out_as_type;
    }

    public List<StartupItemObject> getTraveling_type() {
        return traveling_type;
    }

    public void setTraveling_type(List<StartupItemObject> traveling_type) {
        this.traveling_type = traveling_type;
    }

    public List<StartupItemObject> getPrefer() {
        return prefer;
    }

    public void setPrefer(List<StartupItemObject> prefer) {
        this.prefer = prefer;
    }

    /*
    public List<StartupItemObject> getCharacter() {
        return character;
    }

    public void setCharacter(List<StartupItemObject> character) {
        this.character = character;
    }


     */
    public List<StartupItemObject> getGoing_on() {
        return going_on;
    }

    public void setGoing_on(List<StartupItemObject> going_on) {
        this.going_on = going_on;
    }

    public List<StartupItemObject> getFollow_you() {
        return follow_you;
    }

    public void setFollow_you(List<StartupItemObject> follow_you) {
        this.follow_you = follow_you;
    }

    public List<StartupItemObject> getBusiness_seating() {
        return business_seating;
    }

    public void setBusiness_seating(List<StartupItemObject> business_seating) {
        this.business_seating = business_seating;
    }

    public List<StartupItemObject> getBusiness_atmosphere() {
        return business_atmosphere;
    }

    public void setBusiness_atmosphere(List<StartupItemObject> business_atmosphere) {
        this.business_atmosphere = business_atmosphere;
    }

    public KidsAreaObject getKids_area() {
        return kids_area;
    }

    public void setKids_area(KidsAreaObject kids_area) {
        this.kids_area = kids_area;
    }

    public ParkingObject getParking() {
        return parking;
    }

    public void setParking(ParkingObject parking) {
        this.parking = parking;
    }

    public HandicappedEntranceObject getHandicapped_entrance() {
        return handicapped_entrance;
    }

    public void setHandicapped_entrance(HandicappedEntranceObject handicapped_entrance) {
        this.handicapped_entrance = handicapped_entrance;
    }

    public DeliveryTakeAwayObject getDelivery_and_takeaway() {
        return delivery_and_takeaway;
    }

    public void setDelivery_and_takeaway(DeliveryTakeAwayObject delivery_and_takeaway) {
        this.delivery_and_takeaway = delivery_and_takeaway;
    }

    public RestRoomsObject getRestrooms() {
        return restrooms;
    }

    public void setRestrooms(RestRoomsObject restrooms) {
        this.restrooms = restrooms;
    }

    public List<Integer> getPrice_range() {
        return price_range;
    }

    public void setPrice_range(List<Integer> price_range) {
        this.price_range = price_range;
    }

    public List<StartupItemObject> getMy_place_better_for() {
        return my_place_better_for;
    }

    public void setMy_place_better_for(List<StartupItemObject> my_place_better_for) {
        this.my_place_better_for = my_place_better_for;
    }

    public List<String> getI_prefer() {
        return i_prefer;
    }

    public void setI_prefer(List<String> i_prefer) {
        this.i_prefer = i_prefer;
    }
/*
    public List<StartupItemObject> getI_am() {
        return i_am;
    }

    public void setI_am(List<StartupItemObject> i_am) {
        this.i_am = i_am;
    }


 */
    public List<CategoryObject> getPlaces_categories() {
        return places_categories;
    }

    public void setPlaces_categories(List<CategoryObject> places_categories) {
        this.places_categories = places_categories;
    }

    public List<String> getCountries() {
        return countries;
    }

    public void setCountries(List<String> countries) {
        this.countries = countries;
    }

    public List<LebanonCityObject> getLebanon_cities() {
        return lebanon_cities;
    }

    public void setLebanon_cities(List<LebanonCityObject> lebanon_cities) {
        this.lebanon_cities = lebanon_cities;
    }

    public List<StartupItemObject> getHome_flags_types() {
        return home_flags_types;
    }

    public void setHome_flags_types(List<StartupItemObject> home_flags_types) {
        this.home_flags_types = home_flags_types;
    }

    public List<StartupItemObject> getReport_state_types() {
        return report_state_types;
    }

    public void setReport_state_types(List<StartupItemObject> report_state_types) {
        this.report_state_types = report_state_types;
    }

    public List<StartupItemObject> getStories_types() {
        return stories_types;
    }

    public void setStories_types(List<StartupItemObject> stories_types) {
        this.stories_types = stories_types;
    }
}
