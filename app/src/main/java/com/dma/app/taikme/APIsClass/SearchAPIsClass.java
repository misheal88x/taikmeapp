package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.HomeAPIs;
import com.dma.app.taikme.APIs.ReviewsAPIs;
import com.dma.app.taikme.APIs.SearchAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Field;

public class SearchAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void search(final Context context,
                                        String device_id,
                                        String keyWord,
                                        int page,
                                        final int type,
                                        String advanced_search,
                                        String category,
                                        String sub_category,
                                        String seating,
                                        String atmosphere,
                                        String music_genre,
                                        String cuisine,
                                        String kids_area,
                                        String parking,
                                        String handicapped_entrance,
                                        String takeaway,
                                        String rest_rooms,
                                        String price_range ,
                                        IResponse onResponse1,
                                       final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        if (type == 0) {
            dialog = new NewProgressDialog(context);
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(context);
        SearchAPIs api = retrofit.create(SearchAPIs.class);
        Call<BaseResponse> call = api.search("application/json",
                device_id,
                page,
                keyWord,
                advanced_search,
                category,
                sub_category,
                seating,
                atmosphere,
                music_genre,
                cuisine,
                kids_area,
                parking,
                handicapped_entrance,
                takeaway,
                rest_rooms,
                price_range);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0) {
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0) {
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void get_home(final Context context,
                                String text,
                                int page,
                                IResponse onResponse1,
                                IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        SearchAPIs api = retrofit.create(SearchAPIs.class);
        Call<BaseResponse> call = api.get_by_hashtag("application/json",text);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
