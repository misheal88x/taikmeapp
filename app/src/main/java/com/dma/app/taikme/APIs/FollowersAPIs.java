package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FollowersAPIs {
    @FormUrlEncoded
    @POST("followOrUnFollow")
    Call<BaseResponse> follow(
            @Header("Accept") String accept,
            @Field("object_id") String object_id,
            @Field("object_type") String object_type
    );

    @GET("following/people")
    Call<BaseResponse> get_people(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Query("page") int page
    );

    @POST("following/{type}/{id}/{value}")
    Call<BaseResponse> new_follow(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("type") String type,
            @Path("id") String id,
            @Path("value") String value
    );
}
