package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface TaikMeAPIs {

    @FormUrlEncoded
    @POST("taikme")
    Call<BaseResponse> taik(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("feel") String feel,
            @Field("how_many") String how_many,
            @Field("going_out_type") String going_out_type,
            @Field("include_kids") String include_kids,
            @Field("trip_long") String trip_long,
            @Field("spending_per_person") String spending_per_person,
            @Field("traveling_type") String traveling_type,
            @Field("within") String within
    );
}
