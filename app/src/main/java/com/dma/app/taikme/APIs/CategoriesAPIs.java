package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CategoriesAPIs {

    @GET("categories")
    Call<BaseResponse> get_categories(
            @Header("Accept") String accept,
            @Header("device_id") String device_id
    );

    @GET("categories/{id}/businesses")
    Call<BaseResponse> get_category_places(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("id") String id,
            @Query("page") int page
    );
}
