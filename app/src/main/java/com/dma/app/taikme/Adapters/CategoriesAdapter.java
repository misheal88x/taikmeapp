package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.ShareAPIsClass;
import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Activities.MoreListActivity;
import com.dma.app.taikme.Dialogs.AllCommentsDialog;
import com.dma.app.taikme.Dialogs.AllReviewsDialog;
import com.dma.app.taikme.Dialogs.AmplifyDialog;
import com.dma.app.taikme.Dialogs.SharePostDialog;
import com.dma.app.taikme.Dialogs.ViewImageDialog;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.CategoryObject;
import com.dma.app.taikme.Models.EventObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.w3c.dom.Text;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder>{
    private Context context;
    private List<CategoryObject> list;

    public CategoriesAdapter(Context context,List<CategoryObject> list) {
        this.context = context;
        this.list = list;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView name;
        private LinearLayout layout;
        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            image = view.findViewById(R.id.image);
            layout = view.findViewById(R.id.layout);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final CategoryObject dpo = list.get(position);
        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();
        holder.name.setText(lan.equals("ar")?dpo.getAr_name():dpo.getEn_name());
        BaseFunctions.setGlideImageWithoutCrop(context,holder.image,dpo.getThumb_image());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MoreListActivity.class);
                intent.putExtra("type","category");
                intent.putExtra("id",dpo.getId());
                context.startActivity(intent);
            }
        });
    }
}
