package com.dma.app.taikme.Services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.PostsAPIsClass;
import com.dma.app.taikme.APIsClass.StoriesAPIsClass;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;

public class UploadStoryService extends Service {
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    public final int MY_NOTIFICATION_ID = 1;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setContentTitle("Uploading Story")
                .setContentText("Be cool")
                .setTicker("TICKER");
        Notification notification = builder.build();
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNAEL", NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("NOTIFICATION_CHANNAEL");
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
        startForeground(50, notification);
        //Code
        if (intent.getStringExtra("service_type").equals("story")){
            callUploadStoryAPI(intent.getStringExtra("file_type"),
                    intent.getStringExtra("content"),
                    intent.getStringExtra("file_path"));
        }else if (intent.getStringExtra("service_type").equals("post")){
            callCreatePostAPI(intent.getStringExtra("post_type"),
                    intent.getStringExtra("content"),
                    intent.getStringExtra("caption"),
                    intent.getStringExtra("business_id"),
                    intent.getStringExtra("business_location_id"),
                    intent.getStringExtra("filePath"));
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
    }

    private void callUploadStoryAPI(String type,String content,String filePath){
        StoriesAPIsClass.addStory(getApplicationContext(),
                BaseFunctions.getDeviceId(getApplicationContext()),
                type,
                content,
                filePath, new IResponse() {
                    @Override
                    public void onResponse() {
                        stopForeground(true);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.create_status_success), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                        stopForeground(true);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(UploadStoryService.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        stopForeground(true);
                    }
                });
    }

    private void callCreatePostAPI(String post_type,String content,String caption,String business_id,String business_location_id,String filePath){
        PostsAPIsClass.addPost(
                getApplicationContext(),
                BaseFunctions.getDeviceId(getApplicationContext()),
                post_type,
                content,
                caption,
                business_id,
                business_location_id,
                filePath,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        stopForeground(true);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.create_post_success), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                        stopForeground(true);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(UploadStoryService.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        stopForeground(true);
                    }
                });
    }


}
