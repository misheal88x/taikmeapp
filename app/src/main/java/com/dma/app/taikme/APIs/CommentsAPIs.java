package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CommentsAPIs {

    @FormUrlEncoded
    @POST("comments/{type}/{id}")
    Call<BaseResponse> addComment(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("type") String type,
            @Path("id") String id,
            @Field("comment_text") String comment_text
    );

    @GET("comments/{type}/{id}")
    Call<BaseResponse> getComments(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("type") String type,
            @Path("id") String id,
            @Query("page") int page
    );
}
