package com.dma.app.taikme.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dma.app.taikme.Models.NotificationObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Misheal on 18/11/2019.
 */

public class NotificationsAdapter extends  RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
    private Context context;
    private List<NotificationObject> list;
    public NotificationsAdapter(Context context,List<NotificationObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView img_image;
        private TextView tv_name;
        private LinearLayout layout;


        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_notification_image);
            layout = view.findViewById(R.id.item_notification_layout);
            tv_name = view.findViewById(R.id.item_notification_text);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notifications, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        NotificationObject no = list.get(position);
        BaseFunctions.setGlideImage(context,holder.img_image,no.getUser_thumb_image());
        holder.tv_name.setText(no.getDisplayed_message());
        holder.tv_name.setText(Html.fromHtml(no.getDisplayed_message()+" "+
                "<font color=#BBBDC0>"+BaseFunctions.processDate(context,no.getCreated_at())+"</font>"));

    }
}
