package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.StoriesAPIsClass;
import com.dma.app.taikme.Activities.CreateStatusActivity;
import com.dma.app.taikme.Activities.ViewStoryActivity;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.HomeModels.HomeStoriesObject;
import com.dma.app.taikme.Models.HomeModels.HomeStoryObject;
import com.dma.app.taikme.Models.StoryObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeStatusAdapter extends RecyclerView.Adapter<HomeStatusAdapter.ViewHolder> {

private Context context;
private List<HomeStoryObject> list;
private IMove iMove;

public HomeStatusAdapter(Context context,List<HomeStoryObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
        }

public class ViewHolder extends RecyclerView.ViewHolder {


    private SimpleDraweeView img_image,img_cover;
    private RelativeLayout img_add;
    public ViewHolder(View view) {
        super(view);
        img_image = view.findViewById(R.id.item_status_image);
        img_cover = view.findViewById(R.id.item_status_cover);
        img_add = view.findViewById(R.id.item_status_add);

    }
}

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_circle_status, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        HomeStoryObject so = list.get(position);
        if (position == 0){
            holder.img_add.setVisibility(View.VISIBLE);
            holder.img_cover.setVisibility(View.VISIBLE);
            //BaseFunctions.setGlideImage(context,holder.img_image,
                    //SharedPrefManager.getInstance(context).getUser().getProfile_image());
            BaseFunctions.setFrescoImage(holder.img_image,SharedPrefManager.getInstance(context).getUser().getImage());
            if (so.getUser_profile_thumb_image()!=null&&!so.getUser_profile_thumb_image().equals("")&&
            so.getContent()!=null&&!so.getContent().equals("")){
                BaseFunctions.setFrescoImage(holder.img_image,so.getContent());
                holder.img_cover.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iMove.move(position);
                    }
                });
            }
        }else {
            //BaseFunctions.setGlideImage(context,holder.img_image,
                    //so.getUser_profile_image());
            BaseFunctions.setFrescoImage(holder.img_image,so.getUser_profile_thumb_image());
            holder.img_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iMove.move(position);
                }
            });
        }

        holder.img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(0).getContent()!=null&&!list.get(0).getContent().equals("")) {
                    PopupMenu popup = new PopupMenu(context, holder.img_add);
                    popup.inflate(R.menu.status_menu);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                //case R.id.add: {
                                //    iMove.move();
                                //}
                                //break;
                                case R.id.remove: {
                                    callRemoveStoryAPI();
                                }
                            }
                            return true;
                        }
                    });
                    popup.show();
                }else {
                    iMove.move();
                }
            }
        });
    }

    private void callRemoveStoryAPI(){
        StoriesAPIsClass.deleteStory(
                context,
                BaseFunctions.getDeviceId(context),
                String.valueOf(list.get(0).getId()),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        list.set(0,new HomeStoryObject());
                        notifyItemChanged(0);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
