package com.dma.app.taikme.Fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.dma.app.taikme.APIsClass.PlacesAPIsClass;
import com.dma.app.taikme.Adapters.PlacesReviewedAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.PlacesResponse;
import com.dma.app.taikme.Models.ReviewedPlaceObject;
import com.dma.app.taikme.Models.ReviewedPlacesResponse;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 19/11/2019.
 */

public class PlacesReviewedFragment extends BaseFragment {

    private RecyclerView rv_places;
    private PlacesReviewedAdapter adapter;
    private List<ReviewedPlaceObject> listOfPlaces;
    private LinearLayoutManager layoutManager;
    private RelativeLayout root;
    private LinearLayout no_data_layout;
    private NestedScrollView scrollView;
    private AVLoadingIndicatorView pb_more;
    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_places_reviewed,container,false);
    }

    @Override
    public void init_views() {
        super.init_views();
        //RelativeLayout
        root = base.findViewById(R.id.places_reviewed_layout);
        //RecyclerView
        rv_places = base.findViewById(R.id.places_reviewed_recycler);
        //LinearLayout
        no_data_layout = base.findViewById(R.id.no_data_layout);
        //NestedScrollView
        scrollView = base.findViewById(R.id.places_reviewed_scrollview);
        //AVLoadingIndicator
        pb_more = base.findViewById(R.id.places_reviewed_more);
    }

    @Override
    public void init_events() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (listOfPlaces.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callReviewedPlacesAPI(currentPage,1,"-1");
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        callReviewedPlacesAPI(currentPage,0,"-1");
    }

    private void init_recycler(){
        listOfPlaces = new ArrayList<>();
        adapter = new PlacesReviewedAdapter(base,listOfPlaces);
        layoutManager = new LinearLayoutManager(base,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_places.setLayoutManager(layoutManager);
        rv_places.setAdapter(adapter);
    }

    private void callReviewedPlacesAPI(final int page,final int type,final String id){
        if (type == 1){
            pb_more.smoothToShow();
        }
        PlacesAPIsClass.getReviewedPlaces(
                base,
                BaseFunctions.getDeviceId(base),
                id,
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            ReviewedPlacesResponse success = new Gson().fromJson(j,ReviewedPlacesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (ReviewedPlaceObject po : success.getData()){
                                        listOfPlaces.add(po);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (type == 0){
                                        BaseFunctions.runAnimation(rv_places,0,adapter);
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callReviewedPlacesAPI(page,type,id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            pb_more.smoothToHide();
        }else {
            no_data_layout.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
