package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.FollowersAPIs;
import com.dma.app.taikme.APIs.ReviewsAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FollowersAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void follow(final Context context,
                                 String object_id,
                                 String object_type,
                                 IResponse onResponse1,
                                 final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        FollowersAPIs api = retrofit.create(FollowersAPIs.class);
        Call<BaseResponse> call = api.follow("application/json",
                object_id,
                object_type);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getPeople(final Context context,
                                  String device_id,
                                  int page,
                                 final int type,
                                  IResponse onResponse1,
                                  final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        if (type == 0) {
            dialog = new NewProgressDialog(context);
            dialog.show();
        }

        Retrofit retrofit = configureRetrofitWithBearer(context);
        FollowersAPIs api = retrofit.create(FollowersAPIs.class);
        Call<BaseResponse> call = api.get_people("application/json",
                device_id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0) {
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0) {
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void followNew(final Context context,
                              String device_id,
                              String type,
                              String id,
                              String value,
                              IResponse onResponse1,
                              final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        //dialog = new NewProgressDialog(context);
        //dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        FollowersAPIs api = retrofit.create(FollowersAPIs.class);
        Call<BaseResponse> call = api.new_follow("application/json",
                device_id,type,id,value);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                //dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                //dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
