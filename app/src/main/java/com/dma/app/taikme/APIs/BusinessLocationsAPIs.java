package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface BusinessLocationsAPIs {

    @FormUrlEncoded
    @POST("business/locations")
    Call<BaseResponse> add_location(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("lat") String lat,
            @Field("lng") String lng,
            @Field("location_address") String location_address
    );

    @GET("business/{id}/locations")
    Call<BaseResponse> get_all(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("id") String id
    );
}
