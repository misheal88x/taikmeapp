package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class ReviewObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("review") private String review = "";
    @SerializedName("rating") private float rating = 0;
    @SerializedName("created_at") private String created_at= "";
    @SerializedName("user_name") private String user_name = "";
    @SerializedName("user_mobile_number") private String user_mobile_number= "";
    @SerializedName("user_profile_image") private String user_profile_image= "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_mobile_number() {
        return user_mobile_number;
    }

    public void setUser_mobile_number(String user_mobile_number) {
        this.user_mobile_number = user_mobile_number;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }
}
