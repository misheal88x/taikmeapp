package com.dma.app.taikme.Dialogs;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.dma.app.taikme.APIsClass.BusinessLocationsAPIsClass;
import com.dma.app.taikme.APIsClass.OffersAPIsClass;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IOnDateSelected;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BusinessLocatonNewObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class AddOfferDialog extends AlertDialog {
    private Context context;
    private EditText edt_title;
    private Spinner type_spinner,discount_spinner,locations_spinner;
    private TextView start_date_spinner,end_date_spinner;
    private CustomSpinnerAdapter type_adapter,discount_adapter,locations_adapter;
    private List<String> type_list_string,discount_list_string,locations_list_string;
    private List<BusinessLocatonNewObject> locations_list;
    private List<Integer> discount_list;
    private TextView btn_start;
    private RelativeLayout layout;

    private int selected_business_location_id = -1;
    private int selected_discount = -1;
    private String selected_start_date = "",selected_end_date = "";
    public AddOfferDialog(@NonNull Context context) {
        super(context);
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_add_offer);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //Spinner
        type_spinner = findViewById(R.id.add_offer_type_spinner);
        discount_spinner = findViewById(R.id.add_offer_discount_spinner);
        locations_spinner = findViewById(R.id.add_offer_locations_spinner);
        start_date_spinner = findViewById(R.id.add_offer_start_date_spinner);
        end_date_spinner = findViewById(R.id.add_offer_end_date_spinner);
        //EditText
        edt_title = findViewById(R.id.dialog_add_offer_name);
        //TextView
        btn_start = findViewById(R.id.add_offer_start_btn);
        //RelativeLayout
        layout = findViewById(R.id.layout);
    }

    private void init_events(){
        locations_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (p == 0){
                    selected_business_location_id = -1;
                }else {
                    selected_business_location_id = locations_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        discount_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_discount = -1;
                }else {
                    selected_discount = discount_list.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        start_date_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseFunctions.showDatePicker(context, new IOnDateSelected() {
                    @Override
                    public void onDateSelected(String selected_date) {
                        selected_start_date = selected_date;
                        start_date_spinner.setText(selected_date);
                    }
                });
            }
        });

        end_date_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseFunctions.showDatePicker(context, new IOnDateSelected() {
                    @Override
                    public void onDateSelected(String selected_date) {
                        selected_end_date = selected_date;
                        end_date_spinner.setText(selected_date);
                    }
                });
            }
        });
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_title.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.create_offer_no_title), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_discount == -1){
                    Toast.makeText(context, context.getResources().getString(R.string.create_offer_no_discount), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_start_date.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.create_offer_no_start_date), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_end_date.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.create_offer_no_end_date), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_business_location_id == -1){
                    Toast.makeText(context, context.getResources().getString(R.string.create_offer_no_location), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAddOfferAPI();
            }
        });
    }

    private void init_dialog(){
        init_spinners();
    }

    private void init_spinners(){
        type_list_string = new ArrayList<>();
        discount_list_string = new ArrayList<>();
        discount_list = new ArrayList<>();
        locations_list_string = new ArrayList<>();
        locations_list = new ArrayList<>();

        type_list_string.add(context.getResources().getString(R.string.create_offer_type));
        discount_list_string.add(context.getResources().getString(R.string.create_offer_discount));
        discount_list.add(0);
        for (int i = 1; i <=100 ; i++) {
            discount_list.add(i);
            discount_list_string.add(i+"%");
        }

        locations_list_string.add(context.getResources().getString(R.string.create_offer_locations));
        locations_list.add(new BusinessLocatonNewObject());

        BaseFunctions.init_spinner(context,type_spinner,type_adapter,type_list_string);
        BaseFunctions.init_spinner(context,discount_spinner,discount_adapter,discount_list_string);
        locations_adapter = new CustomSpinnerAdapter(context,R.layout.spinner_item,locations_list_string);
        locations_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locations_spinner.setAdapter(locations_adapter);
        callLocationAPI();

    }

    private void callLocationAPI(){
        btn_start.setEnabled(false);
        BusinessLocationsAPIsClass.getAll(
                context,
                BaseFunctions.getDeviceId(context),
                "-1",
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        btn_start.setEnabled(true);
                        String j = new Gson().toJson(json);
                        BusinessLocatonNewObject[] success = new Gson().fromJson(j,BusinessLocatonNewObject[].class);
                        if (success.length>0){
                            for (BusinessLocatonNewObject o : success){
                                locations_list.add(o);
                                locations_list_string.add(o.getLocation_address());
                                locations_adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        btn_start.setEnabled(true);
                        Snackbar.make(layout, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callLocationAPI();
                                    }
                                }).setActionTextColor(context.getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callAddOfferAPI(){
        OffersAPIsClass.addOffer(context,
                BaseFunctions.getDeviceId(context),
                selected_business_location_id,
                selected_discount,
                edt_title.getText().toString(),
                edt_title.getText().toString(),
                selected_end_date,
                selected_start_date,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        Toast.makeText(context, context.getResources().getString(R.string.create_offer_success), Toast.LENGTH_SHORT).show();
                        cancel();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(layout, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAddOfferAPI();
                                    }
                                }).setActionTextColor(context.getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }
}
