package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class AdObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("en_name") private String en_name = "";
    @SerializedName("ar_name") private String ar_name = "";
    @SerializedName("business_name") private String business_name = "";
    @SerializedName("business_image") private String business_image = "";
    @SerializedName("business_thumb_image") private String business_thumb_image = "";
    @SerializedName("business_cover_image") private String business_cover_image = "";
    @SerializedName("business_id") private int business_id = 0;
    @SerializedName("business_location_id") private int business_location_id = 0;
    @SerializedName("city") private String city = "";
    @SerializedName("budget") private String budget = "";
    @SerializedName("followers") private String followers = "";
    @SerializedName("gender") private String gender = "";
    @SerializedName("age_from") private int age_from = 0;
    @SerializedName("age_to") private int age_to = 0;
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("created_at") private String created_at = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEn_name() {
        return en_name;
    }

    public void setEn_name(String en_name) {
        this.en_name = en_name;
    }

    public String getAr_name() {
        return ar_name;
    }

    public void setAr_name(String ar_name) {
        this.ar_name = ar_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getBusiness_image() {
        return business_image;
    }

    public void setBusiness_image(String business_image) {
        this.business_image = business_image;
    }

    public String getBusiness_thumb_image() {
        return business_thumb_image;
    }

    public void setBusiness_thumb_image(String business_thumb_image) {
        this.business_thumb_image = business_thumb_image;
    }

    public String getBusiness_cover_image() {
        return business_cover_image;
    }

    public void setBusiness_cover_image(String business_cover_image) {
        this.business_cover_image = business_cover_image;
    }

    public int getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(int business_id) {
        this.business_id = business_id;
    }

    public int getBusiness_location_id() {
        return business_location_id;
    }

    public void setBusiness_location_id(int business_location_id) {
        this.business_location_id = business_location_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge_from() {
        return age_from;
    }

    public void setAge_from(int age_from) {
        this.age_from = age_from;
    }

    public int getAge_to() {
        return age_to;
    }

    public void setAge_to(int age_to) {
        this.age_to = age_to;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
