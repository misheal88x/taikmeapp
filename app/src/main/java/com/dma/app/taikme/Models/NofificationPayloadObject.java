package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NofificationPayloadObject {
    @SerializedName("highlighted_words") private List<String> highlighted_words = new ArrayList<>();

    public List<String> getHighlighted_words() {
        return highlighted_words;
    }

    public void setHighlighted_words(List<String> highlighted_words) {
        this.highlighted_words = highlighted_words;
    }
}
