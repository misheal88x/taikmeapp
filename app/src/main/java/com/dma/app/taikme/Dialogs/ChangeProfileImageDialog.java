package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dma.app.taikme.Interfaces.IProfile;
import com.dma.app.taikme.Interfaces.ISelection;
import com.dma.app.taikme.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class ChangeProfileImageDialog extends AlertDialog {
    private Context context;

    private IProfile profile;
    private RelativeLayout lyt_change,lyt_remove;


    public ChangeProfileImageDialog(@NonNull Context context, IProfile profile) {
        super(context);
        this.context = context;
        this.profile = profile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_change_profile_image);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        //RelativeLayout
        lyt_change = findViewById(R.id.change_image_change);
        lyt_remove = findViewById(R.id.change_image_delete);
    }

    private void init_events() {
        lyt_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               profile.onChange();
               dismiss();
            }
        });

        lyt_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.onRemove();
                dismiss();
            }
        });
    }

    private void init_dialog() {

    }
}
