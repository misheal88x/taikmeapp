package com.dma.app.taikme.Models;

public class DumpPlansObject {
    private String title = "";
    private String time = "";
    private String facility_name = "";
    private float facility_rate = 0;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFacility_name() {
        return facility_name;
    }

    public void setFacility_name(String facility_name) {
        this.facility_name = facility_name;
    }

    public float getFacility_rate() {
        return facility_rate;
    }

    public void setFacility_rate(float facility_rate) {
        this.facility_rate = facility_rate;
    }
}
