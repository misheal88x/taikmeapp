package com.dma.app.taikme.Activities;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIs.AuthAPIs;
import com.dma.app.taikme.APIsClass.AuthAPIsClass;
import com.dma.app.taikme.APIsClass.BasicAPIsClass;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Dialogs.ViewHomeVideoDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Models.FacebookObject;
import com.dma.app.taikme.Models.FaqObject;
import com.dma.app.taikme.Models.LoginResponse;
import com.dma.app.taikme.Models.ProfileObject;
import com.dma.app.taikme.Models.SettingsObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.LocaleHelper;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends BaseActivity {

    private RelativeLayout root;
    private EditText edt_phone_number,edt_password;
    private Button btn_register,btn_login;
    private CardView btn_facebook,btn_google;

    private CallbackManager callbackManager;

    private TextView my_token;

    private GoogleSignInClient mGoogleSignInClient;

    private static final int RC_SIGN_IN = 101;

    private Boolean is_register_trasfer = false;

    private AVLoadingIndicatorView loading;

    private TextView tv_english,tv_arabic;
    @Override
    public void set_layout() {
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_login);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        googleInit();
        facebook_init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        is_register_trasfer = false;
    }

    @Override
    public void init_views() {
        //RelativeLayout
        root = findViewById(R.id.login_layout);
        //EditText
        edt_phone_number = findViewById(R.id.login_phone_number);
        edt_password = findViewById(R.id.login_password);
        //Button
        btn_register = findViewById(R.id.login_sign_up);
        btn_login = findViewById(R.id.login_login);
        //CardView
        btn_facebook = findViewById(R.id.login_facebook);
        btn_google = findViewById(R.id.login_google);
        my_token = findViewById(R.id.my_token);
        //ProgressBar
        loading = findViewById(R.id.loading);
        //TextViews
        tv_english = findViewById(R.id.login_english_language);
        tv_arabic = findViewById(R.id.login_arabic_language);
    }

    @Override
    public void init_events() {
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                intent.putExtra("type","register");
                startActivity(intent);
                finish();
            }
        });
        btn_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this,
                        Arrays.asList("public_profile", "email"));
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edt_phone_number.getText().toString())){
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_no_phone_number), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(edt_password.getText().toString())){
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_no_password), Toast.LENGTH_SHORT).show();
                    return;
                }
                callLoginAPI();
            }
        });
        btn_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
        tv_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefManager.getInstance(LoginActivity.this).getSettings()!=null){
                    if (!SharedPrefManager.getInstance(LoginActivity.this).getSettings().getView_language().equals("en")){
                        SettingsObject o = SharedPrefManager.getInstance(LoginActivity.this).getSettings();
                        o.setView_language("en");
                        SharedPrefManager.getInstance(LoginActivity.this).setSettings(o);
                        LocaleHelper.setLocale(LoginActivity.this, "en");
                        //attachBaseContext(CalligraphyContextWrapper.wrap(LocaleHelper.onAttach(RegisterActivity.this)));
                        Intent intent = new Intent(LoginActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
        tv_arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefManager.getInstance(LoginActivity.this).getSettings()!=null){
                    if (!SharedPrefManager.getInstance(LoginActivity.this).getSettings().getView_language().equals("ar")){
                        SettingsObject o = SharedPrefManager.getInstance(LoginActivity.this).getSettings();
                        o.setView_language("ar");
                        SharedPrefManager.getInstance(LoginActivity.this).setSettings(o);
                        LocaleHelper.setLocale(LoginActivity.this, "ar");
                        //attachBaseContext(CalligraphyContextWrapper.wrap(LocaleHelper.onAttach(RegisterActivity.this)));
                        Intent intent = new Intent(LoginActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void facebook_init(){
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.i("facebool_status", "onSuccess: "+loginResult.toString());
                        handleFacebookToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Log.i("facebool_status", "onCancel: "+"Canceled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.i("facebool_status", "onError: "+exception);
                    }});
        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken==null){
                    Toast.makeText(LoginActivity.this, "User logged out", Toast.LENGTH_SHORT).show();
                }else {
                    handleFacebookToken(currentAccessToken);
                }
            }
        };
    }

    private void googleInit(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
    }

    private void handleFacebookToken(final AccessToken accessToken){
        AuthAPIsClass.get_facebook_data(LoginActivity.this, accessToken.getToken(), new IResponse() {
            @Override
            public void onResponse() {

            }

            @Override
            public void onResponse(Object json1) {
                String jj = new Gson().toJson(json1);
                FacebookObject success = new Gson().fromJson(jj,FacebookObject.class);
                String id = success.getId();
                String name = success.getName();
                String email = success.getEmail();
                String image = "https://graph.facebook.com/"+id+"/picture?type=large";
                Log.i("vcffcvcv", "onResponse: "+image);
                callNewCehckSocialAPI(accessToken.getToken(),image,name,email,"2");
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                Toast.makeText(LoginActivity.this, "Failed getting facebook data", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            getGoogleData(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("google_result", "signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void getGoogleData(GoogleSignInAccount account) {
        String name = account.getDisplayName();
        String email = account.getEmail();
        String image = account.getPhotoUrl().toString();
        //callNewCehckSocialAPI(account.getId(),image,name,email,"3");
    }

    private void callLoginAPI(){
        AuthAPIsClass.userLogin(
                LoginActivity.this,
                BaseFunctions.getDeviceId(LoginActivity.this),
                edt_phone_number.getText().toString(),
                edt_password.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        //Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_wrong), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            LoginResponse success = new Gson().fromJson(json1,LoginResponse.class);
                            if (success.getProfile()!=null){
                                if (success.getProfile().getUser_basic_info()!=null){
                                    SharedPrefManager.getInstance(LoginActivity.this).setUser(success.getProfile().getUser_basic_info());
                                    SettingsObject so = new SettingsObject();
                                    so.setVisible_to_search(success.getProfile().getDevice_setting().getVisible_to_search());
                                    so.setAllow_people_to_follow(success.getProfile().getDevice_setting().getAllow_people_to_follow());
                                    so.setIs_profile_public(success.getProfile().getDevice_setting().getIs_profile_public());
                                    so.setPeople_can_comment_on_posts(success.getProfile().getDevice_setting().getPeople_can_comment_on_posts());
                                    so.setPeople_can_vote_posts(success.getProfile().getDevice_setting().getPeople_can_vote_posts());
                                    so.setComments_notifications(success.getProfile().getDevice_setting().getComments_notifications());
                                    so.setVotes_notifications(success.getProfile().getDevice_setting().getVotes_notifications());
                                    so.setView_language(success.getProfile().getDevice_setting().getView_language());
                                    so.setIs_stories_visible(success.getProfile().getDevice_setting().getIs_stories_visible());
                                    SharedPrefManager.getInstance(LoginActivity.this).setSettings(so);
                                }
                                if (success.getProfile().getDevice()!=null){
                                    SharedPrefManager.getInstance(LoginActivity.this).setDevice(success.getProfile().getDevice());
                                }
                                if (success.getProfile().getDevice_setting()!=null){
                                    SharedPrefManager.getInstance(LoginActivity.this).setDeviceSetting(success.getProfile().getDevice_setting());
                                }
                                if (success.getProfile().getTokens()!=null){
                                    SharedPrefManager.getInstance(LoginActivity.this).setAccessToken(success.getProfile().getTokens().getAccess_token());
                                }
                                if (success.getProfile().getUser_basic_info().getAccount_verified_at()!=null) {
                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                    intent.putExtra("type", "home");
                                    startActivity(intent);
                                }else {
                                    startActivity(new Intent(LoginActivity.this,ConfirmAccountActivity.class));
                                    finish();
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callLoginAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callNewCehckSocialAPI(final String access_token, final String image, final String name, final String email, final String loginType){
        String device_id = BaseFunctions.getDeviceId(LoginActivity.this);
        loading.smoothToShow();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        AuthAPIs api = retrofit.create(AuthAPIs.class);
        Call<BaseResponse> call = api.social_verify("application/json",
                device_id,access_token,loginType);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                loading.smoothToHide();
                if (response.code() == 200){
                    if (response.body()!= null){
                        if (response.body().getError_code() == -1){
                            if (response.body().getData()!=null){
                                String jj = new Gson().toJson(response.body().getData());
                                LoginResponse success = new Gson().fromJson(jj,LoginResponse.class);
                                if (success.getProfile().getUser_basic_info().getUser_type()==null||
                                        success.getProfile().getUser_basic_info().getUser_type().equals("")){
                                    if (success.getProfile().getTokens()!=null){
                                        SharedPrefManager.getInstance(LoginActivity.this).setAccessToken(success.getProfile().getTokens().getAccess_token());
                                    }
                                    if (!is_register_trasfer) {
                                        is_register_trasfer = true;
                                        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                                        intent.putExtra("image", image);
                                        intent.putExtra("name", name);
                                        intent.putExtra("email", email);
                                        intent.putExtra("type", "edit");
                                        startActivity(intent);
                                    }
                                    //callSocialRegister(access_token,image,name,email,loginType);
                                }else {
                                    if (success.getProfile()!=null){
                                        if (success.getProfile().getUser_basic_info()!=null){
                                            SharedPrefManager.getInstance(LoginActivity.this).setUser(success.getProfile().getUser_basic_info());
                                            SettingsObject so = new SettingsObject();
                                            so.setVisible_to_search(success.getProfile().getDevice_setting().getVisible_to_search());
                                            so.setAllow_people_to_follow(success.getProfile().getDevice_setting().getAllow_people_to_follow());
                                            so.setIs_profile_public(success.getProfile().getDevice_setting().getIs_profile_public());
                                            so.setPeople_can_comment_on_posts(success.getProfile().getDevice_setting().getPeople_can_comment_on_posts());
                                            so.setPeople_can_vote_posts(success.getProfile().getDevice_setting().getPeople_can_vote_posts());
                                            so.setComments_notifications(success.getProfile().getDevice_setting().getComments_notifications());
                                            so.setVotes_notifications(success.getProfile().getDevice_setting().getVotes_notifications());
                                            so.setView_language(success.getProfile().getDevice_setting().getView_language());
                                            so.setIs_stories_visible(success.getProfile().getDevice_setting().getIs_stories_visible());
                                            SharedPrefManager.getInstance(LoginActivity.this).setSettings(so);
                                        }
                                        if (success.getProfile().getDevice()!=null){
                                            SharedPrefManager.getInstance(LoginActivity.this).setDevice(success.getProfile().getDevice());
                                        }
                                        if (success.getProfile().getDevice_setting()!=null){
                                            SharedPrefManager.getInstance(LoginActivity.this).setDeviceSetting(success.getProfile().getDevice_setting());
                                        }
                                        if (success.getProfile().getTokens()!=null){
                                            SharedPrefManager.getInstance(LoginActivity.this).setAccessToken(success.getProfile().getTokens().getAccess_token());
                                        }
                                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                        intent.putExtra("type", "home");
                                        startActivity(intent);
                                    }
                                }
                            }
                            else {
                                callNewSocialRegisterAPI(access_token,image,name,email,loginType);
                            }
                        }
                        else {
                            //onResponse.onResponse();
                            try {
                                Toast.makeText(LoginActivity.this, response.body().getError_message(), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){}
                        }
                    }
                    else {
                        //onResponse.onResponse();
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.error_service), Toast.LENGTH_SHORT).show();
                    }
                }
                else if (response.code() == 500){
                    //onResponse.onResponse();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.internal_error), Toast.LENGTH_SHORT).show();
                }
                else if (response.code() == 422){
                   // onResponse.onResponse();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.missing_data_error), Toast.LENGTH_SHORT).show();
                }
                else if (response.code() == 502){
                    //onResponse.onResponse();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.dublicated_user), Toast.LENGTH_SHORT).show();
                }
                else {
                    //onResponse.onResponse();
                    try {
                        BaseResponse res = new Gson().fromJson(response.errorBody().string(),BaseResponse.class);
                        Toast.makeText(LoginActivity.this, res.getError_message(), Toast.LENGTH_SHORT).show();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                loading.smoothToHide();
                Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callNewCehckSocialAPI(access_token,image,name,email,loginType);
                            }
                        }).setActionTextColor(getResources().getColor(R.color.white)).show();
            }
        });
    }

    private void callNewSocialRegisterAPI(final String access_token, final String image, final String name, final String email, final String loginType){
        String device_id = BaseFunctions.getDeviceId(LoginActivity.this);

        loading.smoothToShow();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        AuthAPIs api = retrofit.create(AuthAPIs.class);
        Call<BaseResponse> call = api.social_register("application/json",
                device_id,access_token,loginType,email,name,image);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                loading.smoothToHide();
                if (response.code() == 200){
                    if (response.body()!= null){
                        if (response.body().getError_code() == -1){
                            if (response.body().getData()!=null){
                                String j = new Gson().toJson(response.body().getData());
                                LoginResponse success = new Gson().fromJson(j,LoginResponse.class);
                                if (success.getProfile()!=null){
                                    if (success.getProfile().getTokens()!=null){
                                        SharedPrefManager.getInstance(LoginActivity.this).setAccessToken(success.getProfile().getTokens().getAccess_token());
                                    }
                                    if (is_register_trasfer == false) {
                                        is_register_trasfer = true;
                                        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                                        intent.putExtra("image", image);
                                        intent.putExtra("name", name);
                                        intent.putExtra("email", email);
                                        intent.putExtra("type", "edit");
                                        startActivity(intent);
                                    }
                                }
                            }
                        }else {
                            //onResponse.onResponse();
                            try {
                                Toast.makeText(LoginActivity.this, response.body().getError_message(), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){}
                        }
                    }else {
                        //onResponse.onResponse();
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.error_service), Toast.LENGTH_SHORT).show();
                    }
                }else if (response.code() == 500){
                    //onResponse.onResponse();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.internal_error), Toast.LENGTH_SHORT).show();
                }else if (response.code() == 422){
                    // onResponse.onResponse();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.missing_data_error), Toast.LENGTH_SHORT).show();
                }else if (response.code() == 502){
                    //onResponse.onResponse();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.dublicated_user), Toast.LENGTH_SHORT).show();
                }   else {
                    //onResponse.onResponse();
                    try {
                        BaseResponse res = new Gson().fromJson(response.errorBody().string(),BaseResponse.class);
                        Toast.makeText(LoginActivity.this, res.getError_message(), Toast.LENGTH_SHORT).show();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                loading.smoothToHide();
                Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callNewSocialRegisterAPI(access_token,image,name,email,loginType);
                            }
                        }).setActionTextColor(getResources().getColor(R.color.white)).show();
            }
        });
    }



    public static Retrofit configureRetrofitWithoutBearer(){
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(25, TimeUnit.SECONDS)
                .writeTimeout(25, TimeUnit.SECONDS).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl("http://taikme.com/taikme2_api/public/api/v1/").addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit;
    }
}
