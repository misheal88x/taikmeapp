package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Models.CustomAudienceObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CustomAudiencesAdapter  extends RecyclerView.Adapter<CustomAudiencesAdapter.ViewHolder> {
    private Context context;
    private List<CustomAudienceObject> list;
    private IMove iMove;

    public CustomAudiencesAdapter(Context context,List<CustomAudienceObject> list,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_date;
        private ImageView btn_explore;
        private RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.item_ads_manager_name);
            tv_date = view.findViewById(R.id.item_ads_manager_date);
            btn_explore = view.findViewById(R.id.item_ads_manager_explore);
            layout =view.findViewById(R.id.item_ads_manager_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ads_manager, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        CustomAudienceObject cao = list.get(position);
        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();
        if (lan!=null){
            holder.tv_name.setText(lan.equalsIgnoreCase("ar")?cao.getAr_name():cao.getEn_name());
        }
        holder.tv_date.setText(BaseFunctions.dateExtractor(cao.getCreated_at()));
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
        /*
        holder.btn_explore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
**/
    }
}
