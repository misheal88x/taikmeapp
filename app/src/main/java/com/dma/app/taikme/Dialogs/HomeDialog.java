package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.dma.app.taikme.APIs.TaikMeAPIs;
import com.dma.app.taikme.APIsClass.TaikMeAPIsClass;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Interfaces.ITripLong;
import com.dma.app.taikme.Models.FeelToDoObject;
import com.dma.app.taikme.Models.LebanonCityObject;
import com.dma.app.taikme.Models.LocationObject;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Models.StartupObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class HomeDialog extends AlertDialog {

    private Spinner feel_spinner,going_out_spinner,kids_spinner,spending_spinner,
    traveling_spinner,within_spinner;
    private CrystalSeekbar there_seek;
    private TextView there_value;
    private TextView trip_spinner;

    private List<String> feel_list_string;
    private List<StartupItemObject> feel_list;
    private List<String> going_out_list_string;
    private List<StartupItemObject> going_out_list;
    private List<String> kids_list_string;
    private List<String> spending_list_string;
    private List<String> traveling_list_string;
    private List<StartupItemObject> traveling_list;
    private List<String> within_list_string;
    private List<LebanonCityObject> within_list;

    private CustomSpinnerAdapter feel_adapter,going_out_adapter,kids_adapter,spending_adapter,
            traveling_adapter,within_adapter;

    private String selected_day = "",selected_hours = "",selected_minutes = "";
    private ImageView btn_confirm;
    private IMove iMove;

    private String selected_feel = "";
    private String selected_there = "";
    private String selected_going_out = "";
    private String selected_kids = "";
    private String selected_spending = "";
    private String selected_traveling = "";
    private String selected_within = "";

    private RelativeLayout root;

    private Context context;
    public HomeDialog(@NonNull Context context,IMove iMove) {
        super(context);
        this.context = context;
        this.iMove = iMove;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_home_taikme);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //Spinner
        feel_spinner = findViewById(R.id.home_dialog_feel_spinner);
        going_out_spinner = findViewById(R.id.home_dialog_out_as_spinner);
        kids_spinner = findViewById(R.id.home_dialog_kids_spinner);

        trip_spinner = findViewById(R.id.home_dialog_trip_spinner);

        spending_spinner = findViewById(R.id.home_dialog_spending_spinner);
        traveling_spinner = findViewById(R.id.home_dialog_travelling_spinner);
        within_spinner = findViewById(R.id.home_dialog_whithen_spinner);
        //ImageView
        btn_confirm = findViewById(R.id.home_dialog_confirm);
        //Seekbar
        there_seek = findViewById(R.id.there_seek);
        //TextView
        there_value = findViewById(R.id.there_value);

        root = findViewById(R.id.layout);

    }


    private void init_events(){

        feel_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_feel = feel_list.get(p).getId();
                }else {
                    selected_feel = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        there_seek.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                selected_there = String.valueOf(value);
                there_value.setText(selected_there);
            }
        });

        going_out_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_going_out = going_out_list.get(p).getId();
                }else {
                    selected_going_out = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        kids_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p == 0){
                    selected_kids = "";
                }else if (p == 1){
                    selected_kids = "1";
                }else if (p == 2){
                    selected_kids = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spending_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p == 0){
                    selected_spending = "";
                }else {
                    selected_spending = String.valueOf(spending_list_string.get(p).length());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        traveling_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_traveling = traveling_list.get(p).getId();
                }else {
                    selected_traveling = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        within_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_within = String.valueOf(within_list.get(p).getId());
                }else {
                    selected_within = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        trip_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TripLongDialog dialog = new TripLongDialog(context, new ITripLong() {
                    @Override
                    public void onTimeSelected(String days, String hours, String minutes) {
                        selected_day = days;
                        selected_hours = hours;
                        selected_minutes = minutes;
                    }
                });
                dialog.show();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_feel.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.home_dialog_no_feel), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_there.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.home_dialog_no_many), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_going_out.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.home_dialog_no_going_out), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_kids.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.home_dialog_no_kids), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_spending.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.home_dialog_no_spending), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_traveling.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.home_dialog_no_traveling), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_within.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.home_dialog_no_within), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_day.equals("")&&selected_minutes.equals("")&&selected_hours.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.home_dialog_no_trip), Toast.LENGTH_SHORT).show();
                    return;
                }
                callTaikAPI();
            }
        });

    }

    @Override
    public void setOnDismissListener(OnDismissListener listener) {
        super.setOnDismissListener(listener);
    }

    private void init_dialog(){
        init_spinners();
    }

    private void init_spinners(){
        StartupObject so = SharedPrefManager.getInstance(context).getStartUp();
        String language = SharedPrefManager.getInstance(context).getSettings().getView_language();
        feel_list_string = new ArrayList<>();
        feel_list = new ArrayList<>();
        going_out_list_string = new ArrayList<>();
        going_out_list = new ArrayList<>();
        kids_list_string = new ArrayList<>();
        spending_list_string = new ArrayList<>();
        traveling_list_string = new ArrayList<>();
        traveling_list = new ArrayList<>();
        within_list_string = new ArrayList<>();
        within_list = new ArrayList<>();

        feel_list_string.add(context.getResources().getString(R.string.home_dialog_feel));
        feel_list.add(new StartupItemObject());
        if (so.getFeel_to_do()!=null){
            if (so.getFeel_to_do().size()>0){
                for (StartupItemObject fo : so.getFeel_to_do()){
                    feel_list.add(fo);
                    feel_list_string.add(fo.getName());
                }
            }
        }

        going_out_list_string.add(context.getResources().getString(R.string.home_dialog_going_out));
        going_out_list.add(new StartupItemObject());
        if (so.getGoing_out_as_type()!=null){
            if (so.getGoing_out_as_type().size()>0){
                for (StartupItemObject s : so.getGoing_out_as_type()){
                    going_out_list_string.add(s.getName());
                    going_out_list.add(s);
                }
            }
        }

        kids_list_string.add(context.getResources().getString(R.string.home_dialog_kids));
        kids_list_string.add(context.getResources().getString(R.string.yes));
        kids_list_string.add(context.getResources().getString(R.string.no));

        spending_list_string.add(context.getResources().getString(R.string.home_dialog_spending));
        spending_list_string.add("$");
        spending_list_string.add("$$");
        spending_list_string.add("$$$");
        spending_list_string.add("$$$$");
        spending_list_string.add("$$$$$");

        traveling_list_string.add(context.getResources().getString(R.string.home_dialog_traveling));
        traveling_list.add(new StartupItemObject());
        if (so.getTraveling_type()!=null){
            if (so.getTraveling_type().size()>0){
                for (StartupItemObject s : so.getTraveling_type()){
                    traveling_list_string.add(s.getName());
                    traveling_list.add(s);
                }
            }
        }

        within_list_string.add(context.getResources().getString(R.string.home_dialog_within));
        within_list.add(new LebanonCityObject());
        if (so.getLebanon_cities()!=null){
            if (so.getLebanon_cities().size()>0){
                for (LebanonCityObject lo : so.getLebanon_cities()){
                    within_list.add(lo);
                    if (language.equals("en")){
                        within_list_string.add(lo.getEn_name());
                    }else {
                        within_list_string.add(lo.getAr_name());
                    }
                }
            }
        }

        BaseFunctions.init_spinner(context,feel_spinner,feel_adapter,feel_list_string);
        BaseFunctions.init_spinner(context,going_out_spinner,going_out_adapter,going_out_list_string);
        BaseFunctions.init_spinner(context,kids_spinner,kids_adapter,kids_list_string);
        BaseFunctions.init_spinner(context,spending_spinner,spending_adapter,spending_list_string);
        BaseFunctions.init_spinner(context,traveling_spinner,traveling_adapter,traveling_list_string);
        BaseFunctions.init_spinner(context,within_spinner,within_adapter,within_list_string);
    }


    private void callTaikAPI(){
        TaikMeAPIsClass.taik(
                context,
                BaseFunctions.getDeviceId(context),
                selected_feel,
                selected_there,
                selected_going_out,
                selected_kids,
                (!selected_day.equals("") ? selected_day + " " + context.getResources().getString(R.string.days) + " - " : "") +
                        (!selected_hours.equals("") ? selected_hours + " " + context.getResources().getString(R.string.hours) + " - " : "") +
                        (!selected_minutes.equals("") ? selected_minutes + " " + context.getResources().getString(R.string.minutes) + " - " : ""),
                selected_spending,
                selected_traveling,
                selected_within,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        Toast.makeText(context, context.getResources().getString(R.string.done_successfully), Toast.LENGTH_SHORT).show();
                        cancel();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, context.getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callTaikAPI();
                                    }
                                }).setActionTextColor(context.getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
