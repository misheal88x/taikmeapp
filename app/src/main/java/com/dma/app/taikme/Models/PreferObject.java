package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class PreferObject {
    @SerializedName("indoor") private int indoor = 0;
    @SerializedName("outdoor") private int outdoor = 0;
    @SerializedName("all") private int all = 0;

    public int getIndoor() {
        return indoor;
    }

    public void setIndoor(int indoor) {
        this.indoor = indoor;
    }

    public int getOutdoor() {
        return outdoor;
    }

    public void setOutdoor(int outdoor) {
        this.outdoor = outdoor;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
