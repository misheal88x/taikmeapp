package com.dma.app.taikme.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.PictureResult;
import com.otaliastudios.cameraview.VideoResult;
import com.otaliastudios.cameraview.controls.Mode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class RecordVideoActivity extends BaseActivity {

    private CameraView camera;
    private ImageView btn_record;
    private CircularProgressBar pb_video_progress;
    private float rest_time = 30;
    private Handler handler;
    private Boolean handler_declared = false,is_recording = false;
    private TextView tv_timer;
    private RelativeLayout lyt_timer;
    private ImageView btn_change_camera;
    private boolean camera_face_back = true;
    private String videoPath = "";


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_record_video);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        camera.setLifecycleOwner(this);
        camera.addCameraListener(new CameraListener() {
            @Override
            public void onCameraError(@NonNull CameraException exception) {
                super.onCameraError(exception);
                Log.i("camera_error", "onCameraError: "+exception.getMessage());
            }

            @Override
            public void onPictureTaken(@NonNull PictureResult result) {
                super.onPictureTaken(result);
            }

            @Override
            public void onVideoTaken(@NonNull VideoResult result) {
                super.onVideoTaken(result);
            }
        });
    }

    @Override
    public void init_views() {
        camera = findViewById(R.id.camera);
        btn_record = findViewById(R.id.record_video_record_btn);
        pb_video_progress = findViewById(R.id.record_video_progress);
        pb_video_progress.setProgressMax(30f);
        pb_video_progress.setProgress(30f);
        tv_timer = findViewById(R.id.record_video_timer_txt);
        lyt_timer = findViewById(R.id.record_video_timer_layout);
        btn_change_camera = findViewById(R.id.record_video_change_camera);
    }

    @Override
    public void init_events() {
        btn_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!is_recording){
                    is_recording = true;
                    btn_record.setImageResource(R.drawable.ic_stop_video_record);
                    if (!handler_declared){
                        handler_declared = true;
                        lyt_timer.setVisibility(View.VISIBLE);
                        BaseFunctions.playMusic(RecordVideoActivity.this,R.raw.start_record);
                        camera.setMode(Mode.VIDEO);
                        videoPath = String.format("/sdcard/%d.mp4", System.currentTimeMillis());
                        File file = new File(videoPath);
                        camera.takeVideoSnapshot(file);
                        handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (rest_time>0){
                                    rest_time--;
                                    tv_timer.setText(String.valueOf(Math.round(rest_time)));
                                    pb_video_progress.setProgressWithAnimation(rest_time);
                                    handler.postDelayed(this, 1000);
                                }else {
                                    BaseFunctions.playMusic(RecordVideoActivity.this,R.raw.end_record);
                                    camera.stopVideo();
                                    Intent intent = new Intent();
                                    intent.putExtra("video_path",videoPath);
                                    setResult(RESULT_OK,intent);
                                    finish();
                                }
                            }
                        }, 1000);
                    }
                }else {
                    is_recording = false;
                    btn_record.setImageResource(R.drawable.ic_start_video_record);
                    BaseFunctions.playMusic(RecordVideoActivity.this,R.raw.end_record);
                    camera.stopVideo();
                    handler.removeCallbacksAndMessages(null);
                    Intent intent = new Intent();
                    intent.putExtra("video_path",videoPath);
                    setResult(RESULT_OK,intent);
                    finish();
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        pb_video_progress.setProgress(30f);
        rest_time = 30;
        handler_declared = false;
        tv_timer.setText("30");
        lyt_timer.setVisibility(View.GONE);
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onStop() {

        super.onStop();
    }
}
