package com.dma.app.taikme.APIsClass;

import android.content.Context;
import android.util.Log;

import com.dma.app.taikme.APIs.PostsAPIs;
import com.dma.app.taikme.APIs.StoriesAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Header;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class PostsAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void addPost(
            final Context context,
            String device_id,
            String  post_type,
            String content,
            String caption,
            String checkin_business_id,
            String checkin_business_location_id,
            String file,
            IResponse onRespose1,
            IFailure onFailure1){
        onResponse = onRespose1;
        onFailure = onFailure1;

        dialog = new NewProgressDialog(context);
        dialog.show();

        //File
        //MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("file",file);
        //PostType
        RequestBody type_request = BaseFunctions.uploadFileStringConverter(post_type);
        //Content
        RequestBody content_request = BaseFunctions.uploadFileStringConverter(content);
        //caption
        RequestBody caption_request = BaseFunctions.uploadFileStringConverter(caption);
        //Business id
        RequestBody checkin_business_id_request = BaseFunctions.uploadFileStringConverter(checkin_business_id);
        //Business location id
        RequestBody checkin_business_location_id_request = BaseFunctions.uploadFileStringConverter(checkin_business_location_id);

        Retrofit retrofit = configureRetrofitWithBearerLongTime(context);
        PostsAPIs api = retrofit.create(PostsAPIs.class);
        Call<BaseResponse> call = api.store(
                "application/json",
                device_id,
                type_request,
                content_request,
                caption_request,
                checkin_business_id_request,
                checkin_business_location_id_request);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.i("dnbfhdvhd", "onFailure: "+t.getMessage());
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void updatePost(
            final Context context,
            String device_id,
            String device_token,
            String topic_id,
            String post_type,
            String caption,
            String filePath,
            String post_id,
            IResponse onRespose1,
            IFailure onFailure1){
        onResponse = onRespose1;
        onFailure = onFailure1;

        //File
        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("file",filePath);
        //Topic id
        RequestBody topic_request = BaseFunctions.uploadFileStringConverter(topic_id);
        //Type
        RequestBody type_request = BaseFunctions.uploadFileStringConverter(post_type);
        //Caption
        RequestBody caption_request = BaseFunctions.uploadFileStringConverter(caption);
        Retrofit retrofit = configureRetrofitWithBearerLongTime(context);
        PostsAPIs api = retrofit.create(PostsAPIs.class);
        Call<BaseResponse> call = api.update(
                "application/json",
                topic_request,
                type_request,
                caption_request,
                post_id,
                body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
    public static void deletePost(final Context context,
                                   String device_id,
                                   String device_token,
                                   String post_id,
                                   IResponse onResponse1,
                                   final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        PostsAPIs api = retrofit.create(PostsAPIs.class);
        Call<BaseResponse> call = api.delete("application/json",
                device_id,
                device_token,
                post_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getAllPosts(final Context context,
                                  String device_id,
                                  String device_token,
                                  int page,
                                  IResponse onResponse1,
                                  final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        PostsAPIs api = retrofit.create(PostsAPIs.class);
        Call<BaseResponse> call = api.getPosts("application/json",
                device_id,
                device_token,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
