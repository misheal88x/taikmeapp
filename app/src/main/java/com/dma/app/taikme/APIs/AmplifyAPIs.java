package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AmplifyAPIs {

    @FormUrlEncoded
    @POST("amplifies")
    Call<BaseResponse> create(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("en_name") String en_name,
            @Field("ar_name") String ar_name,
            @Field("budget") int budget,
            @Field("location_id") int location_id,
            @Field("custom_audience_id") int custom_audience_id,
            @Field("gender") String gender,
            @Field("age_from") int age_range_from,
            @Field("age_to") int age_range_to,
            @Field("followers") String followers,
            @Field("amplifiable_id") String amplifiable_id,
            @Field("amplifiable_type") String amplifiable_type
    );

    @GET("amplifies")
    Call<BaseResponse> get_all(@Header("Accept") String accept,
                               @Header("device_id") String device_id,
                               @Query("page") int page);
}
