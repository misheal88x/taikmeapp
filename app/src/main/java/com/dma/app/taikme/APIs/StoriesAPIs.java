package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface StoriesAPIs {
    @Multipart
    @POST("users/stories")
    Call<BaseResponse> store(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Part("type") RequestBody type,
            @Part("content") RequestBody content,
            @Part MultipartBody.Part file);
    @POST("users/stories/{story_id}/delete")
    Call<BaseResponse> delete(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("story_id") String story_id
    );
}
