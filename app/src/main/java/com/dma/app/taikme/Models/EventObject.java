package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class EventObject {
    //ok
    @SerializedName("id") private int id = 0;
    //ok
    @SerializedName("event_date") private String eventDate = "";
    //ok
    @SerializedName("image") private String image = "";
    //ok
    @SerializedName("created_at") private String createdAt = "";
    //ok
    @SerializedName("since_minute") private int sinceMinute = 0;

    //New
    @SerializedName("business_location_id") private int business_location_id = 0;
    //New
    @SerializedName("business_id") private int business_id = 0;
    //New
    @SerializedName("ar_title") private String ar_title = "";
    //New
    @SerializedName("en_title") private String en_title = "";
    //New
    @SerializedName("en_description") private String en_description = "";
    //New
    @SerializedName("ar_description") private String ar_description = "";
    //New
    @SerializedName("business_location_address") private String business_location_address = "";
    //New
    @SerializedName("business_location_lat") private float business_location_lat = 0.0f;
    //New
    @SerializedName("business_location_lng") private float business_location_lng = 0.0f;
    //New
    @SerializedName("business_name") private String business_name = "";
    //New
    @SerializedName("business_mobile_number") private String business_mobile_number = "";
    //New
    @SerializedName("business_email") private String business_email = "";
    //New
    @SerializedName("business_image") private String business_image = "";
    //New
    @SerializedName("business_thumb_image") private String business_thumb_image = "";
    //New
    @SerializedName("business_cover_image") private String business_cover_image = "";
    //New
    @SerializedName("business_country_name") private String business_country_name = "";
    //New
    @SerializedName("business_city_name") private String business_city_name = "";
    //New
    @SerializedName("going_count") private int going_count = 0;
    //New
    @SerializedName("reviews_count") private int reviews_count = 0;
    //New
    @SerializedName("reviews_rating") private String reviews_rating = "";
    //New
    @SerializedName("following_count") private int following_count = 0;
    //New
    @SerializedName("upvote_count") private int upvoting_count = 0;
    //New
    @SerializedName("downvote_count") private int downvote_count = 0;
    //New
    @SerializedName("business_avg_review") private String business_avg_review = "";
    //New
    @SerializedName("is_going") private int is_going = 0;
    //New
    @SerializedName("user_vote") private int user_vote = 0;
    //New
    @SerializedName("device_setting") private DeviceSettingsObject device_setting = new DeviceSettingsObject();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getSinceMinute() {
        return sinceMinute;
    }

    public void setSinceMinute(Integer sinceMinute) {
        this.sinceMinute = sinceMinute;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSinceMinute(int sinceMinute) {
        this.sinceMinute = sinceMinute;
    }

    public int getBusiness_location_id() {
        return business_location_id;
    }

    public void setBusiness_location_id(int business_location_id) {
        this.business_location_id = business_location_id;
    }

    public int getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(int business_id) {
        this.business_id = business_id;
    }

    public String getAr_title() {
        return ar_title;
    }

    public void setAr_title(String ar_title) {
        this.ar_title = ar_title;
    }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
    }

    public String getEn_description() {
        return en_description;
    }

    public void setEn_description(String en_description) {
        this.en_description = en_description;
    }

    public String getAr_description() {
        return ar_description;
    }

    public void setAr_description(String ar_description) {
        this.ar_description = ar_description;
    }

    public String getBusiness_location_address() {
        return business_location_address;
    }

    public void setBusiness_location_address(String business_location_address) {
        this.business_location_address = business_location_address;
    }

    public float getBusiness_location_lat() {
        return business_location_lat;
    }

    public void setBusiness_location_lat(float business_location_lat) {
        this.business_location_lat = business_location_lat;
    }

    public float getBusiness_location_lng() {
        return business_location_lng;
    }

    public void setBusiness_location_lng(float business_location_lng) {
        this.business_location_lng = business_location_lng;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getBusiness_mobile_number() {
        return business_mobile_number;
    }

    public void setBusiness_mobile_number(String business_mobile_number) {
        this.business_mobile_number = business_mobile_number;
    }

    public String getBusiness_email() {
        return business_email;
    }

    public void setBusiness_email(String business_email) {
        this.business_email = business_email;
    }

    public String getBusiness_image() {
        return business_image;
    }

    public void setBusiness_image(String business_image) {
        this.business_image = business_image;
    }

    public String getBusiness_thumb_image() {
        return business_thumb_image;
    }

    public void setBusiness_thumb_image(String business_thumb_image) {
        this.business_thumb_image = business_thumb_image;
    }

    public String getBusiness_cover_image() {
        return business_cover_image;
    }

    public void setBusiness_cover_image(String business_cover_image) {
        this.business_cover_image = business_cover_image;
    }

    public String getBusiness_country_name() {
        return business_country_name;
    }

    public void setBusiness_country_name(String business_country_name) {
        this.business_country_name = business_country_name;
    }

    public String getBusiness_city_name() {
        return business_city_name;
    }

    public void setBusiness_city_name(String business_city_name) {
        this.business_city_name = business_city_name;
    }

    public int getGoing_count() {
        return going_count;
    }

    public void setGoing_count(int going_count) {
        this.going_count = going_count;
    }

    public int getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(int reviews_count) {
        this.reviews_count = reviews_count;
    }

    public String getReviews_rating() {
        return reviews_rating;
    }

    public void setReviews_rating(String reviews_rating) {
        this.reviews_rating = reviews_rating;
    }

    public int getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(int following_count) {
        this.following_count = following_count;
    }

    public int getUpvoting_count() {
        return upvoting_count;
    }

    public void setUpvoting_count(int upvoting_count) {
        this.upvoting_count = upvoting_count;
    }

    public int getDownvote_count() {
        return downvote_count;
    }

    public void setDownvote_count(int downvote_count) {
        this.downvote_count = downvote_count;
    }

    public String getBusiness_avg_review() {
        return business_avg_review;
    }

    public void setBusiness_avg_review(String business_avg_review) {
        this.business_avg_review = business_avg_review;
    }

    public int getIs_going() {
        return is_going;
    }

    public void setIs_going(int is_going) {
        this.is_going = is_going;
    }

    public int getUser_vote() {
        return user_vote;
    }

    public void setUser_vote(int user_vote) {
        this.user_vote = user_vote;
    }

    public DeviceSettingsObject getDevice_setting() {
        return device_setting;
    }

    public void setDevice_setting(DeviceSettingsObject device_setting) {
        this.device_setting = device_setting;
    }
}
