package com.dma.app.taikme.Activities;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.PictureResult;
import com.otaliastudios.cameraview.VideoResult;
import com.otaliastudios.cameraview.controls.Facing;
import com.otaliastudios.cameraview.controls.Mode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CreateStatusActivity extends BaseActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate{

    private CameraView camera;
    private TextView btn_capture;
    private String imagePath = "";
    private String videoPath = "";
    private CircularProgressBar pb_video_progress;
    private boolean isSpeakButtonLongPressed = false;
    private float rest_time = 30;
    private Handler handler;
    private Boolean handler_declared = false;
    private TextView tv_timer;
    private RelativeLayout lyt_timer;
    private ImageView btn_change_camera;
    private boolean camera_face_back = true;
    private ImageView btn_gallery;
    private TextView tv_message;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_create_status);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        camera.setLifecycleOwner(this);
        camera.addCameraListener(new CameraListener() {
            @Override
            public void onCameraError(@NonNull CameraException exception) {
                super.onCameraError(exception);
                Log.i("camera_error", "onCameraError: "+exception.getMessage());
            }

            @Override
            public void onPictureTaken(@NonNull PictureResult result) {
                super.onPictureTaken(result);
                imagePath = String.format("/sdcard/%d.jpg", System.currentTimeMillis());
                FileOutputStream outStream = null;
                try {
                    outStream = new FileOutputStream(imagePath);
                    outStream.write(result.getData());
                    outStream.close();
                    Intent intent = new Intent(CreateStatusActivity.this,EditStatusActivity.class);
                    intent.putExtra("imagePath",imagePath);
                    startActivity(intent);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                }
            }

            @Override
            public void onVideoTaken(@NonNull VideoResult result) {
                super.onVideoTaken(result);
                //result.getFile().getAbsolutePath();
            }
        });
    }

    @Override
    public void init_views() {
        camera = findViewById(R.id.camera);
        btn_capture = findViewById(R.id.create_status_capture);
        pb_video_progress = findViewById(R.id.create_status_progress);
        pb_video_progress.setProgressMax(30f);
        pb_video_progress.setProgress(30f);
        tv_timer = findViewById(R.id.create_status_timer_txt);
        lyt_timer = findViewById(R.id.create_status_timer_layout);
        btn_change_camera = findViewById(R.id.create_status_change_camera);
        btn_gallery = findViewById(R.id.create_status_gallery);
        tv_message = findViewById(R.id.create_status_bottom_message);
    }

    @Override
    public void init_events() {

        btn_capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //BaseFunctions.playMusic(CreateStatusActivity.this,R.raw.camera_capure_sound);
                camera.setMode(Mode.PICTURE);
                camera.takePicture();
            }
        });
        btn_capture.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // Do something when your hold starts here.
                isSpeakButtonLongPressed = true;
                if (!handler_declared){
                    handler_declared = true;
                    lyt_timer.setVisibility(View.VISIBLE);
                    BaseFunctions.playMusic(CreateStatusActivity.this,R.raw.start_record);
                    camera.setMode(Mode.VIDEO);
                    videoPath = String.format("/sdcard/%d.mp4", System.currentTimeMillis());
                    File file = new File(videoPath);
                    camera.takeVideoSnapshot(file);
                    handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (rest_time>0){
                                rest_time--;
                                tv_timer.setText(String.valueOf(Math.round(rest_time)));
                                pb_video_progress.setProgressWithAnimation(rest_time);
                                handler.postDelayed(this, 1000);
                            }else {
                                isSpeakButtonLongPressed = false;
                                BaseFunctions.playMusic(CreateStatusActivity.this,R.raw.end_record);
                                camera.stopVideo();
                                Intent intent = new Intent(CreateStatusActivity.this,VideoStatusActivity.class);
                                intent.putExtra("video_path",videoPath);
                                startActivity(intent);
                                //Toast.makeText(CreateStatusActivity.this, "Stop record", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, 1000);
                }
                return true;
            }
        });
        btn_capture.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                // We're only interested in when the button is released.
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // We're only interested in anything if our speak button is currently pressed.
                    if (isSpeakButtonLongPressed) {
                        // Do something when the button is released.
                        isSpeakButtonLongPressed = false;
                        BaseFunctions.playMusic(CreateStatusActivity.this,R.raw.end_record);
                        camera.stopVideo();
                        //Toast.makeText(CreateStatusActivity.this, "Stop record", Toast.LENGTH_SHORT).show();
                        handler.removeCallbacksAndMessages(null);
                        Intent intent = new Intent(CreateStatusActivity.this,VideoStatusActivity.class);
                        intent.putExtra("video_path",videoPath);
                        startActivity(intent);
                    }
                }
                return false;
            }
        });

        btn_change_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (camera_face_back){
                    camera_face_back = false;
                    camera.setFacing(Facing.FRONT);
                }else {
                    camera_face_back = true;
                    camera.setFacing(Facing.BACK);
                }
            }
        });
        btn_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.dma.app.taikme.provider")
                        .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                        .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                        .build();
                singleSelectionPicker.show(getSupportFragmentManager(), "picker");
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        pb_video_progress.setProgress(30f);
        rest_time = 30;
        isSpeakButtonLongPressed = false;
        handler_declared = false;
        tv_timer.setText("30");
        lyt_timer.setVisibility(View.GONE);
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onStop() {

        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        String imagePath1 = uri.getPath();
        Intent intent = new Intent(CreateStatusActivity.this,EditStatusActivity.class);
        intent.putExtra("imagePath",imagePath1);
        startActivity(intent);
        Log.i("image_path_res", "onSingleImageSelected: "+imagePath1);
    }

    @Override
    public void loadImage(File imageFile, ImageView ivImage) {
        Glide.with(CreateStatusActivity.this).load(imageFile).into(ivImage);
    }


}
