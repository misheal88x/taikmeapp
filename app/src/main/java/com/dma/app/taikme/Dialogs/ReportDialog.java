package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.ReportAPIsClass;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class ReportDialog  extends AlertDialog {
    private Context context;
    private String id,type;
    private EditText reason;
    private Button send;

    public ReportDialog(@NonNull Context context, String id, String type) {
        super(context);
        this.context = context;
        this.id = id;
        this.type = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_report);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //EditText
        reason = findViewById(R.id.reason);
        //Button
        send = findViewById(R.id.send);

    }

    private void init_events(){
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reason.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.report_no_reason), Toast.LENGTH_SHORT).show();
                    return;
                }
                callReportAPI(id,type);
            }
        });
    }

    private void init_dialog(){

    }

    private void callReportAPI(String id,
                               String type){
        ReportAPIsClass.report(context,
                BaseFunctions.getDeviceId(context),
                type,
                id,
                reason.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        boolean sucess = new Gson().fromJson(j,Boolean.class);
                        if (sucess){
                            Toast.makeText(context, context.getResources().getString(R.string.report_success), Toast.LENGTH_LONG).show();
                            cancel();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            cancel();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        cancel();
                    }
                }
        );
    }
}
