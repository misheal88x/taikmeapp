package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;
//import com.onesignal.OSNotificationPayload;

public class NotificationObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("user_name") private String user_name = "";
    @SerializedName("user_image") private String user_image = "";
    @SerializedName("user_thumb_image") private String user_thumb_image = "";
    @SerializedName("user_action_id") private int user_action_id = 0;
    @SerializedName("action_type") private String action_type = "";
    @SerializedName("target_type") private String target_type = "";
    @SerializedName("target_type_owner") private int target_type_owner = 0;
    @SerializedName("target_id") private int target_id = 0;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("since_minute") private Object since_minute = null;
    @SerializedName("displayed_message") private String displayed_message = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_thumb_image() {
        return user_thumb_image;
    }

    public void setUser_thumb_image(String user_thumb_image) {
        this.user_thumb_image = user_thumb_image;
    }

    public int getUser_action_id() {
        return user_action_id;
    }

    public void setUser_action_id(int user_action_id) {
        this.user_action_id = user_action_id;
    }

    public String getAction_type() {
        return action_type;
    }

    public void setAction_type(String action_type) {
        this.action_type = action_type;
    }

    public String getTarget_type() {
        return target_type;
    }

    public void setTarget_type(String target_type) {
        this.target_type = target_type;
    }

    public int getTarget_type_owner() {
        return target_type_owner;
    }

    public void setTarget_type_owner(int target_type_owner) {
        this.target_type_owner = target_type_owner;
    }

    public int getTarget_id() {
        return target_id;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Object getSince_minute() {
        return since_minute;
    }

    public void setSince_minute(Object since_minute) {
        this.since_minute = since_minute;
    }

    public String getDisplayed_message() {
        return displayed_message;
    }

    public void setDisplayed_message(String displayed_message) {
        this.displayed_message = displayed_message;
    }
}
