package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("profile") private ProfileObject profile = new ProfileObject();

    public ProfileObject getProfile() {
        return profile;
    }

    public void setProfile(ProfileObject profile) {
        this.profile = profile;
    }
}
