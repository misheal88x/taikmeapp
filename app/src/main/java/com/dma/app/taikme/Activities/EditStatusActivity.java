package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.zelory.compressor.Compressor;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ja.burhanrashid52.photoeditor.OnSaveBitmap;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dma.app.taikme.Adapters.ImageFiltersAdapter;
import com.dma.app.taikme.Fragments.EditImageTextFragment;
import com.dma.app.taikme.Interfaces.AddtextFragmentListener;
import com.dma.app.taikme.Interfaces.FilterListListener;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Models.StoryObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Services.UploadStoryService;
import com.dma.app.taikme.Utils.BitmapUtils;
import com.zomato.photofilters.FilterPack;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.NativeImageProcessor;
import com.zomato.photofilters.utils.ThumbnailItem;
import com.zomato.photofilters.utils.ThumbnailsManager;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class EditStatusActivity extends BaseActivity implements FilterListListener, AddtextFragmentListener {

    private PhotoEditorView img_image;
    private ImageView btn_filters;
    private TextView tv_upload,btn_add_text;
    private Intent myIntent;
    private RecyclerView rv_filters;
    private ImageFiltersAdapter filtersAdapter;
    private LinearLayoutManager filtersLayoutManager;
    private List<ThumbnailItem> listOfFilters;
    private boolean showFilters = false;
    private PhotoEditor photoEditor;
    private String imagePath = "";

    private Bitmap originalBitmap,filteredBitmap,finalBitmap;

    //Load native image filters library
    static{
        System.loadLibrary("NativeImageProcessor");
    }
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_edit_status);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        imagePath = myIntent.getStringExtra("imagePath");
        File file = new File(myIntent.getStringExtra("imagePath"));
        //BaseFunctions.setGlideFileImage(EditStatusActivity.this,img_image.getSource(),file);
        init_recycler();
        display_thumbnail(BitmapUtils.getBitmapFromPath(EditStatusActivity.this,myIntent.getStringExtra("imagePath"),
                Resources.getSystem().getDisplayMetrics().widthPixels,
                Resources.getSystem().getDisplayMetrics().heightPixels));
        originalBitmap = BitmapUtils.getBitmapFromPath(EditStatusActivity.this,myIntent.getStringExtra("imagePath"),
                Resources.getSystem().getDisplayMetrics().widthPixels,
                Resources.getSystem().getDisplayMetrics().heightPixels);
        img_image.getSource().setImageBitmap(originalBitmap);
        filteredBitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
        finalBitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888,true);

    }

    @Override
    public void init_views() {
        //ImageView
        img_image = findViewById(R.id.edit_status_image);
        btn_filters = findViewById(R.id.edit_status_filter_btn);
        //Intent
        myIntent = getIntent();
        //RecyclerView
        rv_filters = findViewById(R.id.edit_status_filters_recycler);
        //TextView
        tv_upload = findViewById(R.id.edit_status_upload);
        btn_add_text = findViewById(R.id.edit_status_add_text);
        //Photo Editor
        photoEditor = new PhotoEditor.Builder(EditStatusActivity.this,img_image).setPinchTextScalable(true).build();
    }

    @Override
    public void init_events() {
        btn_filters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showFilters){
                    showFilters = false;
                    rv_filters.setVisibility(View.GONE);
                }else {
                    showFilters = true;
                    rv_filters.setVisibility(View.VISIBLE);
                }
            }
        });
        tv_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoEditor.saveAsBitmap(new OnSaveBitmap() {
                    @Override
                    public void onBitmapReady(Bitmap saveBitmap) {
                        try {
                            img_image.getSource().setImageBitmap(saveBitmap);
                            String path = BitmapUtils.insertImage(EditStatusActivity.this,getContentResolver(),
                                    saveBitmap,
                                    System.currentTimeMillis()+"_status.jpg",null);
                            imagePath = path;
                            compressImage(new File(imagePath));
                            /*
                            Intent intent = new Intent(EditStatusActivity.this, UploadStoryService.class);
                            intent.putExtra("service_type","story");
                            intent.putExtra("file_type",StoryObject.TYPE_IMAGE);
                            intent.putExtra("content","story");
                            intent.putExtra("file_path",imagePath);
                            startService(intent);
                            **/
                            Intent intent1 = new Intent(EditStatusActivity.this,HomeActivity.class);
                            intent1.putExtra("type","home");
                            startActivity(intent1);
                            finish();
                            //Log.i("edited_image", "onClick: "+path);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {

                    }
                });
                /*
                try {
                    String path = BitmapUtils.insertImage(EditStatusActivity.this,getContentResolver(),
                            finalBitmap,
                            System.currentTimeMillis()+"_status.jpg",null);
                    Log.i("edited_image", "onClick: "+path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                **/
            }
        });
        btn_add_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditImageTextFragment editImageTextFragment = EditImageTextFragment.getInstance();
                editImageTextFragment.setListener(EditStatusActivity.this);
                editImageTextFragment.show(getSupportFragmentManager(),editImageTextFragment.getTag());

            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        listOfFilters = new ArrayList<>();
        filtersAdapter = new ImageFiltersAdapter(EditStatusActivity.this,listOfFilters,this);
        filtersLayoutManager = new LinearLayoutManager(EditStatusActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_filters.setLayoutManager(filtersLayoutManager);
        rv_filters.setAdapter(filtersAdapter);
    }

    @Override
    public void onFilterSelected(Filter filter) {
        filteredBitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
        img_image.getSource().setImageBitmap(filter.processFilter(filteredBitmap));
        finalBitmap = filteredBitmap.copy(Bitmap.Config.ARGB_8888,true);
    }

    private void display_thumbnail(final Bitmap bitmap) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                Bitmap thumbImg = null;
                if (bitmap != null){
                    thumbImg = Bitmap.createScaledBitmap(bitmap,
                            300,
                            300,false);
                }
                ThumbnailsManager.clearThumbs();
                listOfFilters.clear();

                //Add normal bitmap first
                ThumbnailItem thumbnailItem = new ThumbnailItem();
                thumbnailItem.image = thumbImg;
                thumbnailItem.filterName = "Normal";
                ThumbnailsManager.addThumb(thumbnailItem);
                List<Filter> filters = FilterPack.getFilterPack(EditStatusActivity.this);
                for (Filter filter:filters){
                    ThumbnailItem t1 = new ThumbnailItem();
                    t1.image = thumbImg;
                    t1.filter = filter;
                    t1.filterName = filter.getName();
                    ThumbnailsManager.addThumb(t1);
                }

                listOfFilters.addAll(ThumbnailsManager.processThumbs(EditStatusActivity.this));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        filtersAdapter.notifyDataSetChanged();
                    }
                });
            }
        };
        new Thread(r).start();

    }

    @Override
    public void onAddTextButtonClick(Typeface typeface, String text, int color) {
        photoEditor.addText(typeface,text,color);
    }

    private void compressImage(File imageFile){
        new Compressor(EditStatusActivity.this)
                .setQuality(75)
                .setDestinationDirectoryPath("/sdcard/")
                .compressToFileAsFlowable(imageFile)
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<File>() {
                    @Override
                    public void accept(File file) {
                        if (file.exists()){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(EditStatusActivity.this, getResources().getString(R.string.create_status_creating), Toast.LENGTH_SHORT).show();
                                }
                            });
                            BaseFunctions.uploadStory(EditStatusActivity.this,StoryObject.TYPE_IMAGE,"story",file.getPath());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        throwable.printStackTrace();
                        Log.i("error_image", "accept: "+throwable.getMessage());
                    }
                });
    }
}
