package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ColorsAdapter extends  RecyclerView.Adapter<ColorsAdapter.ViewHolder> {
    private Context context;
    private List<Integer> list;
    private IMove iMove;
    private int selected_item = -1;
    public ColorsAdapter(Context context,List<Integer> list,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View v_view;
        private ImageView checked;

        public ViewHolder(View view) {
            super(view);
            v_view = view.findViewById(R.id.item_color_view);
            checked = view.findViewById(R.id.checked);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_color, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        if (selected_item == position){
            holder.checked.setVisibility(View.VISIBLE);
        }else {
            holder.checked.setVisibility(View.GONE);
        }
        holder.v_view.setBackgroundColor(list.get(position));
        holder.v_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_item = position;
                notifyDataSetChanged();
                iMove.move(position);
            }
        });

    }
}
