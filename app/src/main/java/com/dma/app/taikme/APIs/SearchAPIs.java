package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SearchAPIs {
    @FormUrlEncoded
    @POST("search")
    Call<BaseResponse> search(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Query("page") int page,
            @Field("keyword") String keywords,
            @Field("advanced_search") String advanced_search,
            @Field("category") String category,
            @Field("sub_category") String sub_category,
            @Field("seating") String seating,
            @Field("atmosphere") String atmosphere,
            @Field("music_genre") String music_genre,
            @Field("cuisine") String cuisine,
            @Field("kids_area") String kids_area,
            @Field("parking") String parking,
            @Field("handicapped_entrance") String handicapped_entrance,
            @Field("takeaway") String takeaway,
            @Field("rest_rooms") String rest_rooms,
            @Field("price_range") String price_range
    );

    @POST("searchByHashTag")
    Call<BaseResponse> get_by_hashtag(
            @Header("Accept") String accept,
            @Field("hash_tag") String hash_tag
            //,
            //@Query("page") int page
    );
}
