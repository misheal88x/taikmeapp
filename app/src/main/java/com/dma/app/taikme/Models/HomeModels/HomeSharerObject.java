package com.dma.app.taikme.Models.HomeModels;

import com.google.gson.annotations.SerializedName;

public class HomeSharerObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("image") private String image = "";
    @SerializedName("city_name") private String city_name = "";
    @SerializedName("country_name") private String country_name= "";
    @SerializedName("cover_image") private String cover_image = "";
    @SerializedName("reviews_rating") private String reviews_rating = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

    public String getReviews_rating() {
        return reviews_rating;
    }

    public void setReviews_rating(String reviews_rating) {
        this.reviews_rating = reviews_rating;
    }
}
