package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StatisticsObject {
    @SerializedName("upvoted_count") private int upvoted_count = 0;
    @SerializedName("downvoted_count") private int downvoted_count = 0;
    @SerializedName("commented_count") private int commented_count = 0;
    @SerializedName("shared_count") private int shared_count = 0;
    @SerializedName("remaining_time") private int remaining_time = 0;
    @SerializedName("upvoted_array") private List<StatisticDayObject> upvoted_array = new ArrayList<>();
    @SerializedName("downvoted_array") private List<StatisticDayObject> downvoted_array = new ArrayList<>();
    @SerializedName("commented_array") private List<StatisticDayObject> commented_array = new ArrayList<>();
    @SerializedName("shared_array") private List<StatisticDayObject> shared_array = new ArrayList<>();

    public int getUpvoted_count() {
        return upvoted_count;
    }

    public void setUpvoted_count(int upvoted_count) {
        this.upvoted_count = upvoted_count;
    }

    public int getDownvoted_count() {
        return downvoted_count;
    }

    public void setDownvoted_count(int downvoted_count) {
        this.downvoted_count = downvoted_count;
    }

    public int getCommented_count() {
        return commented_count;
    }

    public void setCommented_count(int commented_count) {
        this.commented_count = commented_count;
    }

    public int getShared_count() {
        return shared_count;
    }

    public void setShared_count(int shared_count) {
        this.shared_count = shared_count;
    }

    public int getRemaining_time() {
        return remaining_time;
    }

    public void setRemaining_time(int remaining_time) {
        this.remaining_time = remaining_time;
    }

    public List<StatisticDayObject> getUpvoted_array() {
        return upvoted_array;
    }

    public void setUpvoted_array(List<StatisticDayObject> upvoted_array) {
        this.upvoted_array = upvoted_array;
    }

    public List<StatisticDayObject> getDownvoted_array() {
        return downvoted_array;
    }

    public void setDownvoted_array(List<StatisticDayObject> downvoted_array) {
        this.downvoted_array = downvoted_array;
    }

    public List<StatisticDayObject> getCommented_array() {
        return commented_array;
    }

    public void setCommented_array(List<StatisticDayObject> commented_array) {
        this.commented_array = commented_array;
    }

    public List<StatisticDayObject> getShared_array() {
        return shared_array;
    }

    public void setShared_array(List<StatisticDayObject> shared_array) {
        this.shared_array = shared_array;
    }
}
