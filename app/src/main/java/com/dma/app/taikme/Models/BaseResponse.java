package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("data") private Object data = new Object();
    @SerializedName("error_message") private String error_message = "";
    @SerializedName("error_code") private int error_code = 0;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }
}
