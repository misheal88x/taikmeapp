package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Activities.ShowLocationMapActivity;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.UserObject;
import com.dma.app.taikme.Models.VisitedPlaceObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Misheal on 15/11/2019.
 */

public class SearchResultsAdapter extends  RecyclerView.Adapter<SearchResultsAdapter.ViewHolder> {
    private Context context;
    private List<VisitedPlaceObject> list;
    private IMove iMove;

    public SearchResultsAdapter(Context context,List<VisitedPlaceObject> list,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView img_image;
        private TextView tv_name;
        private ImageView radio_1,radio_2,radio_3,radio_4,radio_5;
        private SimpleRatingBar rb_rate;
        private CardView btn_takeme;
        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_search_result_image);
            tv_name = view.findViewById(R.id.item_search_result_name);
            radio_1 = view.findViewById(R.id.item_search_result_price_1);
            radio_2 = view.findViewById(R.id.item_search_result_price_2);
            radio_3 = view.findViewById(R.id.item_search_result_price_3);
            radio_4 = view.findViewById(R.id.item_search_result_price_4);
            radio_5 = view.findViewById(R.id.item_search_result_price_5);
            rb_rate = view.findViewById(R.id.item_search_result_rate);
            btn_takeme = view.findViewById(R.id.item_search_result_takeme);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_result, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final VisitedPlaceObject place = list.get(position);

        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();
        holder.tv_name.setText(place.getName()!=null?place.getName():"");
        /*
        if (lan.equals("en")){
            if (place.getEn_name()!=null){
                holder.tv_name.setText(place.getEn_name());
            }else {
                if (place.getAr_name()!=null){
                    holder.tv_name.setText(place.getAr_name());
                }
            }
        }else if (lan.equals("ar")){
            if (place.getAr_name()!=null){
                holder.tv_name.setText(place.getAr_name());
            }else {
                if (place.getEn_name()!=null){
                    holder.tv_name.setText(place.getEn_name());
                }
            }
        }

         */
        float int_price = place.getPrice_range();
        if (int_price==1){
            holder.radio_1.setImageResource(R.drawable.ic_radio_on);
            holder.radio_2.setImageResource(R.drawable.ic_radio_off);
            holder.radio_3.setImageResource(R.drawable.ic_radio_off);
            holder.radio_4.setImageResource(R.drawable.ic_radio_off);
            holder.radio_5.setImageResource(R.drawable.ic_radio_off);
        }else if (int_price==2){
            holder.radio_1.setImageResource(R.drawable.ic_radio_on);
            holder.radio_2.setImageResource(R.drawable.ic_radio_on);
            holder.radio_3.setImageResource(R.drawable.ic_radio_off);
            holder.radio_4.setImageResource(R.drawable.ic_radio_off);
            holder.radio_5.setImageResource(R.drawable.ic_radio_off);
        }else if (int_price==3){
            holder.radio_1.setImageResource(R.drawable.ic_radio_on);
            holder.radio_2.setImageResource(R.drawable.ic_radio_on);
            holder.radio_3.setImageResource(R.drawable.ic_radio_on);
            holder.radio_4.setImageResource(R.drawable.ic_radio_off);
            holder.radio_5.setImageResource(R.drawable.ic_radio_off);
        }else if (int_price==4){
            holder.radio_1.setImageResource(R.drawable.ic_radio_on);
            holder.radio_2.setImageResource(R.drawable.ic_radio_on);
            holder.radio_3.setImageResource(R.drawable.ic_radio_on);
            holder.radio_4.setImageResource(R.drawable.ic_radio_on);
            holder.radio_5.setImageResource(R.drawable.ic_radio_off);
        }else if (int_price==5){
            holder.radio_1.setImageResource(R.drawable.ic_radio_on);
            holder.radio_2.setImageResource(R.drawable.ic_radio_on);
            holder.radio_3.setImageResource(R.drawable.ic_radio_on);
            holder.radio_4.setImageResource(R.drawable.ic_radio_on);
            holder.radio_5.setImageResource(R.drawable.ic_radio_on);
        }else {
            holder.radio_1.setVisibility(View.GONE);
            holder.radio_2.setVisibility(View.GONE);
            holder.radio_3.setVisibility(View.GONE);
            holder.radio_4.setVisibility(View.GONE);
            holder.radio_5.setVisibility(View.GONE);
        }
        try {
            if (place.getBusiness_avg_review()!=null&&!place.getBusiness_avg_review().equals("")){
                holder.rb_rate.setRating(Float.valueOf(place.getBusiness_avg_review()));
            }else {
                if (place.getAvg_review()!=null&&!place.getAvg_review().equals("")){
                    holder.rb_rate.setRating(Float.valueOf(place.getAvg_review()));
                }else {
                    holder.rb_rate.setRating(0f);
                }
            }
        }catch (Exception e){}

        BaseFunctions.setGlideImage(context,holder.img_image,place.getThumb_image());

        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)) {
                    if (place.getUser_type()!=null&&!place.getUser_type().equals("")){
                        if (place.getUser_type().equals(UserObject.USER_TYPE)){
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","other");
                            intent.putExtra("profile_id",place.getId());
                            context.startActivity(intent);
                        }else if (place.getUser_type().equals(UserObject.BUSINESS_TYPE)){
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "facility_info");
                            intent.putExtra("place_id", place.getId());
                            context.startActivity(intent);
                        }
                    }else {
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.putExtra("type", "facility_info");
                        intent.putExtra("place_id", place.getId());
                        context.startActivity(intent);
                    }
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.img_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)) {
                    if (place.getUser_type()!=null&&!place.getUser_type().equals("")){
                        if (place.getUser_type().equals(UserObject.USER_TYPE)){
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","other");
                            intent.putExtra("profile_id",place.getId());
                            context.startActivity(intent);
                        }else if (place.getUser_type().equals(UserObject.BUSINESS_TYPE)){
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "facility_info");
                            intent.putExtra("place_id", place.getId());
                            context.startActivity(intent);
                        }
                    }else {
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.putExtra("type", "facility_info");
                        intent.putExtra("place_id", place.getId());
                        context.startActivity(intent);
                    }
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.btn_takeme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);

            }
        });

    }

}
