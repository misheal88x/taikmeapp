package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.EventsAPIs;
import com.dma.app.taikme.APIs.PlacesAPIs;
import com.dma.app.taikme.APIs.UserAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Part;

public class EventsAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void getAllEvents(final Context context,
                                        String device_id,
                                        int page,
                                        final int type,
                                        boolean mine,
                                        IResponse onResponse1,
                                        final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        if (type == 0){
            dialog = new NewProgressDialog(context);
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(context);
        EventsAPIs api = retrofit.create(EventsAPIs.class);
        Call<BaseResponse> call = api.getAll("application/json",
                device_id,
                mine,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if(type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void addEvent(final Context context,
                                String device_id,
                                String business_location_id,
                                String ar_title,
                                String en_title,
                                String event_date,
                                String file_image,
                                    IResponse onResponse1,
                                    final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        File file = new File(file_image);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file_image",file.getName(),requestBody);

        //Business_location_id
        RequestBody locationIdRequest = RequestBody.create(MultipartBody.FORM,business_location_id);
        //Ar title
        RequestBody arTitleRequest = RequestBody.create(MultipartBody.FORM,ar_title);
        //En title
        RequestBody enTitleRequest = RequestBody.create(MultipartBody.FORM,en_title);
        //Event date
        RequestBody eventDateRequest = RequestBody.create(MultipartBody.FORM,event_date);

        Retrofit retrofit = configureRetrofitWithBearer(context);
        EventsAPIs api = retrofit.create(EventsAPIs.class);
        Call<BaseResponse> call = api.addEvent("application/json",
                device_id,
                locationIdRequest,
                arTitleRequest,
                enTitleRequest,
                eventDateRequest,
                body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void joinEvent(final Context context,
                                   String device_id,
                                   int id,
                                   int cancel,
                                   IResponse onResponse1,
                                   final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        EventsAPIs api = retrofit.create(EventsAPIs.class);
        Call<BaseResponse> call = api.joinEvent("application/json",
                device_id,
                String.valueOf(id),
                cancel);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
