package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.ShareAPIsClass;
import com.dma.app.taikme.Adapters.HomePostsAdapter;
import com.dma.app.taikme.Dialogs.AllCommentsDialog;
import com.dma.app.taikme.Dialogs.AllReviewsDialog;
import com.dma.app.taikme.Dialogs.SharePostDialog;
import com.dma.app.taikme.Dialogs.ViewHomeVideoDialog;
import com.dma.app.taikme.Dialogs.ViewImageDialog;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.HomeModels.HomePostsObject;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

public class ViewOriginalPostActivity extends BaseActivity {

    private RelativeLayout toolbar,back;

    //Image
    private LinearLayout image_layout;
    private SimpleDraweeView image_img_profile_image;
    private TextView image_tv_username,image_tv_address,image_tv_time,image_tv_desc,image_tv_see_comments,image_tv_see_reviews;
    private TextView image_tv_votes_count;
    private TextView image_tv_comment_user;
    private SimpleRatingBar image_rb_rate;
    private ImageView image_btn_up,image_btn_down,image_btn_rolling;
    private SimpleDraweeView image_img_image;

    //Video
    private LinearLayout video_layout;
    private SimpleDraweeView video_img_profile_image;
    private TextView video_tv_username,video_tv_address,video_tv_time,video_tv_desc,video_tv_see_comments,video_tv_see_reviews;
    private TextView video_tv_votes_count;
    private TextView video_tv_comment_user;
    private SimpleRatingBar video_rb_rate;
    private ImageView video_btn_up,video_btn_down,video_btn_rolling,video_btn_three_dots,video_thumbnail,video_play;

    //Event
    private LinearLayout event_layout;
    private SimpleDraweeView event_img_profile_image;
    private RelativeLayout event_btn_add;
    private TextView event_tv_username,event_tv_address,event_tv_time,event_tv_desc,event_tv_see_comments,event_tv_see_reviews,event_tv_location,event_tv_date_and_going;
    private TextView event_tv_votes_count;
    private TextView event_tv_comment_user;
    private SimpleRatingBar event_rb_rate;
    private ImageView event_btn_up,event_btn_down,event_btn_rolling;
    private SimpleDraweeView event_img_image;
    private ImageView event_going_image;
    private RelativeLayout event_going_layout;

    //Review
    private LinearLayout review_layout;
    private SimpleDraweeView review_img_profile_image,review_img_image;
    private TextView review_tv_username,review_tv_title,review_tv_address,review_tv_time,review_tv_name,review_tv_rate,review_tv_text;
    private TextView review_tv_votes_count;
    private SimpleRatingBar review_rb_rate,review_rb_rate2;
    private ImageView review_btn_up,review_btn_down,review_btn_rolling,review_btn_three_dots,review_img_broadcast;

    //Check
    private LinearLayout check_layout;
    private SimpleDraweeView check_img_profile_image,check_img_image;
    private TextView check_tv_username,check_tv_address,check_tv_time,check_tv_name,check_tv_rate,check_tv_caption,check_tv_see_comments,check_tv_see_reviews;
    private TextView check_tv_votes_count;
    private TextView check_tv_comment_user;
    private SimpleRatingBar check_rb_rate,check_rb_rate2;
    private ImageView check_btn_up,check_btn_down,check_btn_rolling,check_btn_three_dots,check_img_broadcast;

    //AD
    private LinearLayout ad_layout;
    private SimpleDraweeView ad_img_profile_image;
    private TextView ad_tv_username,ad_tv_address,ad_tv_time,ad_tv_sposored,ad_tv_desc,ad_tv_see_comments,ad_tv_see_reviews;
    private TextView ad_tv_votes_count;
    private TextView ad_tv_comment_user;
    private SimpleRatingBar ad_rb_rate;
    private ImageView ad_btn_up,ad_btn_down,ad_btn_rolling,ad_btn_three_dots;
    private SimpleDraweeView ad_img_image;

    //Offer
    private LinearLayout offer_layout;
    private SimpleDraweeView offer_img_profile_image;
    private TextView offer_tv_username,offer_tv_address,offer_tv_time,offer_tv_name,offer_tv_rate,offer_tv_percent,offer_tv_till,offer_tv_see_comments,offer_tv_see_reviews;
    private TextView offer_tv_comment_user;
    private TextView offer_tv_votes_count;
    private SimpleRatingBar offer_rb_rate;
    private ImageView offer_btn_up,offer_btn_down,offer_btn_rolling,offer_btn_three_dots,offer_add_img;
    private RelativeLayout offer_img_add;

    private HomePostsObject dpo;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_view_original_post);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        getPost();
    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        back = toolbar.findViewById(R.id.toolbar_back_btn);

        //Image
        image_layout = findViewById(R.id.image_layout);
        image_img_profile_image = findViewById(R.id.image_item_media_profile_image);
        image_tv_username = findViewById(R.id.image_item_media_user_name);
        image_tv_address = findViewById(R.id.image_item_media_address);
        image_tv_time = findViewById(R.id.image_item_media_time);
        image_tv_desc = findViewById(R.id.image_item_media_desc);
        image_tv_comment_user = findViewById(R.id.image_item_media_comment_user);
        image_tv_see_comments = findViewById(R.id.image_item_media_read_comments);
        image_tv_see_reviews = findViewById(R.id.image_item_media_read_reviews);
        image_tv_votes_count = findViewById(R.id.image_votes_count);
        image_rb_rate = findViewById(R.id.image_item_media_rate);
        image_img_image = findViewById(R.id.image_item_media_image);
        image_btn_up = findViewById(R.id.image_item_media_top_arrow);
        image_btn_down = findViewById(R.id.image_item_media_down_arrow);
        image_btn_rolling = findViewById(R.id.image_item_media_rolling_circle);

        //Video
        video_layout = findViewById(R.id.video_layout);
        video_img_profile_image = findViewById(R.id.video_item_media_profile_image);
        video_tv_username = findViewById(R.id.video_item_media_user_name);
        video_tv_address = findViewById(R.id.video_item_media_address);
        video_tv_time = findViewById(R.id.video_item_media_time);
        video_tv_desc = findViewById(R.id.video_item_media_desc);
        video_tv_comment_user = findViewById(R.id.video_item_media_comment_user);
        video_tv_see_comments = findViewById(R.id.video_item_media_read_comments);
        video_tv_see_reviews = findViewById(R.id.video_item_media_read_reviews);
        video_tv_votes_count = findViewById(R.id.video_votes_count);
        video_rb_rate = findViewById(R.id.video_item_media_rate);
        video_thumbnail = findViewById(R.id.video_item_media_video);
        video_btn_up = findViewById(R.id.video_item_media_top_arrow);
        video_btn_down = findViewById(R.id.video_item_media_down_arrow);
        video_btn_rolling = findViewById(R.id.video_item_media_rolling_circle);
        video_play = findViewById(R.id.video_play_video);


        //Event
        event_layout = findViewById(R.id.event_layout);
        event_img_profile_image = findViewById(R.id.event_item_media_profile_image);
        event_btn_add = findViewById(R.id.event_item_home_event_add_btn);
        event_tv_username = findViewById(R.id.event_item_media_user_name);
        event_tv_address = findViewById(R.id.event_item_media_address);
        event_tv_time = findViewById(R.id.event_item_media_time);
        event_tv_desc = findViewById(R.id.event_item_media_desc);
        event_tv_comment_user = findViewById(R.id.event_item_media_comment_user);
        event_tv_see_comments = findViewById(R.id.event_item_media_read_comments);
        event_tv_see_reviews = findViewById(R.id.event_item_media_read_reviews);
        event_tv_location = findViewById(R.id.event_item_home_event_location);
        event_tv_date_and_going = findViewById(R.id.event_item_home_event_date_and_going);
        event_tv_votes_count = findViewById(R.id.event_votes_count);
        event_rb_rate = findViewById(R.id.event_item_media_rate);
        event_img_image = findViewById(R.id.event_item_media_image);
        event_btn_up = findViewById(R.id.event_item_media_top_arrow);
        event_btn_down = findViewById(R.id.event_item_media_down_arrow);
        event_btn_rolling = findViewById(R.id.event_item_media_rolling_circle);
        event_going_layout = findViewById(R.id.event_going_layout);
        event_going_image = findViewById(R.id.event_going_image);

        //Review
        review_layout = findViewById(R.id.review_layout);
        review_img_profile_image = findViewById(R.id.review_item_home_post_rate_profile_image);
        review_img_image = findViewById(R.id.review_item_home_post_rate_image);
        review_tv_username = findViewById(R.id.review_item_home_post_rate_user_name);
        review_tv_title = findViewById(R.id.review_item_home_post_rate_title);
        review_tv_address = findViewById(R.id.review_item_home_post_rate_address);
        review_tv_time = findViewById(R.id.review_item_home_post_rate_time);
        review_tv_name = findViewById(R.id.review_item_home_post_rate_name);
        review_tv_rate = findViewById(R.id.review_item_home_post_rate_rate_value);
        review_tv_votes_count = findViewById(R.id.review_votes_count);
        review_rb_rate = findViewById(R.id.review_item_home_post_rate_rate);
        review_rb_rate2 = findViewById(R.id.review_item_home_post_rate_rate2);
        review_btn_up = findViewById(R.id.review_item_home_post_rate_top_arrow);
        review_btn_down = findViewById(R.id.review_item_home_post_rate_down_arrow);
        review_btn_rolling = findViewById(R.id.review_item_home_post_rate_rolling_circle);
        review_img_broadcast = findViewById(R.id.review_item_home_post_rate_broadcast);
        review_tv_text = findViewById(R.id.review_item_home_post_rate_text);

        //Check
        check_layout = findViewById(R.id.check_layout);
        check_img_profile_image = findViewById(R.id.check_item_home_post_is_at_profile_image);
        check_img_image = findViewById(R.id.check_item_home_post_is_at_image);
        check_tv_username = findViewById(R.id.check_item_home_post_is_at_user_name);
        check_tv_address = findViewById(R.id.check_item_home_post_is_at_address);
        check_tv_time = findViewById(R.id.check_item_home_post_is_at_time);
        check_tv_name = findViewById(R.id.check_item_home_post_is_at_name);
        check_tv_rate = findViewById(R.id.check_item_home_post_is_at_rate_value);
        check_rb_rate = findViewById(R.id.check_item_home_post_is_at_rate);
        check_rb_rate2 = findViewById(R.id.check_item_home_post_is_at_rate2);
        check_tv_caption = findViewById(R.id.check_item_home_post_is_at_desc);
        check_tv_votes_count = findViewById(R.id.check_votes_count);
        check_btn_up = findViewById(R.id.check_item_home_post_is_at_top_arrow);
        check_btn_down = findViewById(R.id.check_item_home_post_is_at_down_arrow);
        check_btn_rolling = findViewById(R.id.check_item_home_post_is_at_rolling_circle);
        check_img_broadcast = findViewById(R.id.check_item_home_post_is_at_broadcast);
        check_tv_comment_user = findViewById(R.id.check_item_media_comment_user);
        check_tv_see_comments = findViewById(R.id.check_item_media_read_comments);
        check_tv_see_reviews = findViewById(R.id.check_item_media_read_reviews);

        //AD
        ad_layout = findViewById(R.id.ad_item_media_layout);
        ad_img_profile_image = findViewById(R.id.ad_item_media_profile_image);
        ad_tv_username = findViewById(R.id.ad_item_media_user_name);
        ad_tv_address = findViewById(R.id.ad_item_media_address);
        ad_tv_time = findViewById(R.id.ad_item_media_time);
        ad_tv_sposored = findViewById(R.id.ad_item_media_sponsored);
        ad_tv_desc = findViewById(R.id.ad_item_media_desc);
        ad_tv_comment_user = findViewById(R.id.ad_item_media_comment_user);
        ad_tv_see_comments = findViewById(R.id.ad_item_media_read_comments);
        ad_tv_see_reviews = findViewById(R.id.ad_item_media_read_reviews);
        ad_tv_votes_count = findViewById(R.id.ad_votes_count);
        ad_rb_rate = findViewById(R.id.ad_item_media_rate);
        ad_img_image = findViewById(R.id.ad_item_media_image);
        ad_btn_up = findViewById(R.id.ad_item_media_top_arrow);
        ad_btn_down = findViewById(R.id.ad_item_media_down_arrow);
        ad_btn_rolling = findViewById(R.id.ad_item_media_rolling_circle);

        //Offer
        offer_layout = findViewById(R.id.offer_layout);
        offer_img_profile_image = findViewById(R.id.offer_item_home_post_offer_profile_image);
        offer_tv_username = findViewById(R.id.offer_item_home_post_offer_user_name);
        offer_tv_address = findViewById(R.id.offer_item_home_post_offer_address);
        offer_tv_time = findViewById(R.id.offer_item_home_post_offer_time);
        offer_tv_name = findViewById(R.id.offer_item_home_post_offer_name);
        offer_tv_rate = findViewById(R.id.offer_item_home_post_offer_rate_value);
        offer_tv_percent = findViewById(R.id.offer_item_home_post_offer_percent);
        offer_tv_till = findViewById(R.id.offer_item_home_post_offer_rate_value);
        offer_tv_comment_user = findViewById(R.id.offer_item_home_post_offer_comment_user);
        offer_tv_see_comments = findViewById(R.id.offer_item_home_post_offer_read_comments);
        offer_tv_see_reviews = findViewById(R.id.offer_item_home_post_offer_read_reviews);
        offer_tv_votes_count = findViewById(R.id.offer_votes_count);
        offer_rb_rate = findViewById(R.id.offer_item_home_post_offer_rate);
        offer_btn_up = findViewById(R.id.offer_item_home_post_offer_top_arrow);
        offer_btn_down = findViewById(R.id.offer_item_home_post_offer_down_arrow);
        offer_btn_rolling = findViewById(R.id.offer_item_home_post_offer_rolling_circle);
        offer_img_add = findViewById(R.id.offer_item_home_post_offer_add);
        offer_add_img = findViewById(R.id.offer_item_home_post_offer_add_img);
    }

    @Override
    public void init_events() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(ViewOriginalPostActivity.this);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void getPost(){
        String j = getIntent().getStringExtra("post");
        dpo = new Gson().fromJson(j,HomePostsObject.class);
        int type = getIntent().getIntExtra("type",0);
        String lan = SharedPrefManager.getInstance(ViewOriginalPostActivity.this).getDeviceSetting().getView_language();

        switch (type){
            //Image
            case 1:{
                image_layout.setVisibility(View.VISIBLE);
                //Username
                if (dpo.getPost_object().getUser_name()!=null){
                    image_tv_username.setText(dpo.getPost_object().getUser_name());
                }
                //Profile image
                if (dpo.getPost_object().getUser_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(image_img_profile_image,dpo.getPost_object().getUser_thumb_image());
                    //BaseFunctions.setGlideImage(context,photoViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                image_tv_address.setText(dpo.getPost_object().getUser_city_name()!=null?dpo.getPost_object().getUser_city_name():"");
                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    try {
                        image_rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                    }catch (Exception e){}
                }
                //Time
                if (dpo.getPost_object().getCreated_at()!=null){
                    image_tv_time.setText(BaseFunctions.processDate(ViewOriginalPostActivity.this,dpo.getPost_object().getCreated_at()));
                }
                //Image
                if (dpo.getPost_object().getContent()!=null){
                    BaseFunctions.setFrescoImage(image_img_image,dpo.getPost_object().getContent());
                    //BaseFunctions.setGlideImage(context,photoViewHolder.img_image,dpo.getReal_object().getContent().toString());
                }
                //Description
                if (dpo.getPost_object().getCaption()!=null){
                    setTags(image_tv_desc,dpo.getPost_object().getCaption());
                }
                //Votes count
                image_tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    image_btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    image_btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    image_btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    image_btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                    image_tv_comment_user.setVisibility(View.GONE);
                }
                if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                    image_btn_down.setVisibility(View.GONE);
                    image_btn_up.setVisibility(View.GONE);
                }

                image_tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            //AllCommentsDialog dialog = new AllCommentsDialog(ViewOriginalPostActivity.this,"post",dpo.getPost_object().getId());
                            //dialog.show();
                            Intent intent = new Intent(ViewOriginalPostActivity.this,SeeCommentsActivity.class);
                            intent.putExtra("id",dpo.getPost_object().getId());
                            intent.putExtra("type","post");
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                image_tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            //AllReviewsDialog dialog = new AllReviewsDialog(ViewOriginalPostActivity.this,"post",dpo.getPost_object().getId());
                            //dialog.show();
                            Intent intent = new Intent(ViewOriginalPostActivity.this,SeeReviewsActivity.class);
                            intent.putExtra("id",dpo.getPost_object().getId());
                            intent.putExtra("type","post");
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                image_tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        startActivity(intent);
                    }
                });
                image_img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            try{
                                ViewImageDialog dialog = new ViewImageDialog(ViewOriginalPostActivity.this,dpo.getPost_object().getContent());
                                dialog.show();
                            }catch (Exception e){}

                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                image_img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                image_tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                image_btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)){
                            SharePostDialog dialog = new SharePostDialog(ViewOriginalPostActivity.this, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            //Video
            case 2:{
                video_layout.setVisibility(View.VISIBLE);
                //Username
                video_tv_username.setText(dpo.getPost_object().getUser_name());
                //Profile image
                BaseFunctions.setFrescoImage(video_img_profile_image,dpo.getPost_object().getUser_thumb_image());
                //Address
                video_tv_address.setText(dpo.getPost_object().getUser_city_name()!=null?dpo.getPost_object().getUser_city_name():"");
                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    video_rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                video_tv_time.setText(BaseFunctions.processDate(ViewOriginalPostActivity.this,dpo.getPost_object().getCreated_at()));
                //Description
                setTags(video_tv_desc,dpo.getPost_object().getCaption());
                //Votes Count
                video_tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    video_btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    video_btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    video_btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    video_btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                    video_tv_comment_user.setVisibility(View.GONE);
                }
                if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                    video_btn_down.setVisibility(View.GONE);
                    video_btn_up.setVisibility(View.GONE);
                }
                video_tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        startActivity(intent);
                    }
                });
                video_play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHomeVideoDialog dialog = new ViewHomeVideoDialog(ViewOriginalPostActivity.this,dpo.getPost_object().getContent());
                        dialog.show();
                    }
                });
                video_thumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHomeVideoDialog dialog = new ViewHomeVideoDialog(ViewOriginalPostActivity.this,dpo.getPost_object().getContent());
                        dialog.show();
                    }
                });
                video_tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AllCommentsDialog dialog = new AllCommentsDialog(ViewOriginalPostActivity.this,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this,SeeCommentsActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        startActivity(intent);
                    }
                });

                video_tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AllReviewsDialog dialog = new AllReviewsDialog(ViewOriginalPostActivity.this,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this,SeeReviewsActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        startActivity(intent);
                    }
                });
                video_tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        startActivity(intent);
                    }
                });

                video_img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        startActivity(intent);
                    }
                });
                video_tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        startActivity(intent);
                    }
                });
                video_btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)){
                            SharePostDialog dialog = new SharePostDialog(ViewOriginalPostActivity.this, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            //Event
            case 3:{
                event_layout.setVisibility(View.VISIBLE);
                //Username
                if (dpo.getPost_object().getBusiness_name()!=null){
                    event_tv_username.setText(dpo.getPost_object().getBusiness_name());
                }
                //Profile image
                if (dpo.getPost_object().getBusiness_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(event_img_profile_image,dpo.getPost_object().getBusiness_thumb_image());
                    //BaseFunctions.setGlideImage(context,eventViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                event_tv_location.setText(dpo.getPost_object().getBusiness_city_name());

                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    event_rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                if (dpo.getPost_object().getCreated_at()!=null){
                    event_tv_time.setText(BaseFunctions.processDate(ViewOriginalPostActivity.this,dpo.getPost_object().getCreated_at()));
                }
                //Going
                if (dpo.getPost_object().getBusiness_id()!=SharedPrefManager.getInstance(ViewOriginalPostActivity.this).getUser().getId()){
                    if (dpo.getPost_object().getIs_going() == 0){
                        event_going_layout.setBackgroundResource(R.drawable.primary_circle);
                        event_going_image.setImageResource(R.drawable.ic_plus);
                    }else if (dpo.getPost_object().getIs_going() == 1){
                        event_going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                        event_going_image.setImageResource(R.drawable.ic_minus);
                    }else {
                        event_going_layout.setVisibility(View.GONE);
                    }
                }else {
                    event_going_layout.setVisibility(View.GONE);
                }
                //Image
                if (dpo.getPost_object().getImage()!=null){
                    try {
                        BaseFunctions.setFrescoImage(event_img_image,dpo.getPost_object().getImage());
                    }catch (Exception e){}
                    //BaseFunctions.setGlideImage(context,eventViewHolder.img_image,dpo.getReal_object().getImage());
                }
                //Event title
                if (lan.equals("en")){
                    event_tv_desc.setText(dpo.getPost_object().getEn_title()!=null?dpo.getPost_object().getEn_title():"");
                }else if (lan.equals("ar")){
                    event_tv_desc.setText(dpo.getPost_object().getAr_title()!=null?dpo.getPost_object().getAr_title():dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }
                //Event location
                event_tv_location.setText(dpo.getPost_object().getBusiness_location_address()!=null?dpo.getPost_object().getBusiness_location_address():"");
                //Event date and going count
                try {
                    event_tv_date_and_going.setText(BaseFunctions.dateExtractor(dpo.getPost_object().getEvent_date())+"-"+dpo.getPost_object().getGoing_count()+" "+
                            getResources().getString(R.string.create_event_going));
                }catch (Exception e){}
                //Votes count
                event_tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //Voted Value
                if (dpo.getPost_object().getUser_vote()==1){
                    event_btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    event_btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    event_btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    event_btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                    event_tv_comment_user.setVisibility(View.GONE);
                }
                if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                    event_btn_down.setVisibility(View.GONE);
                    event_btn_up.setVisibility(View.GONE);
                }

                event_tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)){
                            //AllCommentsDialog dialog = new AllCommentsDialog(ViewOriginalPostActivity.this,"event",dpo.getPost_object().getId());
                            //dialog.show();
                            Intent intent = new Intent(ViewOriginalPostActivity.this,SeeCommentsActivity.class);
                            intent.putExtra("id",dpo.getPost_object().getId());
                            intent.putExtra("type","event");
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                event_tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)){
                            //AllReviewsDialog dialog = new AllReviewsDialog(ViewOriginalPostActivity.this,"event",dpo.getPost_object().getId());
                            //dialog.show();
                            Intent intent = new Intent(ViewOriginalPostActivity.this,SeeReviewsActivity.class);
                            intent.putExtra("id",dpo.getPost_object().getId());
                            intent.putExtra("type","event");
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                event_going_layout.setVisibility(View.GONE);

                event_tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","event");
                        startActivity(intent);
                    }
                });
                event_img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            ViewImageDialog dialog = new ViewImageDialog(ViewOriginalPostActivity.this, dpo.getPost_object().getImage());
                            dialog.show();
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                event_img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                            intent.putExtra("type","facility_info");
                            intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                event_tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                            intent.putExtra("type","facility_info");
                            intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                event_btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)){
                            SharePostDialog dialog = new SharePostDialog(ViewOriginalPostActivity.this, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"event",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                event_btn_add.setVisibility(View.GONE);
            }break;
            //Rate
            case 4:{}break;
            //Review
            case 5:{
                review_layout.setVisibility(View.VISIBLE);
                //Username
                review_tv_username.setText(dpo.getPost_object().getUser_name());

                //User Profile Image
                BaseFunctions.setFrescoImage(review_img_profile_image,dpo.getPost_object().getUser_thumb_image());

                //Title
                review_tv_title.setText(getResources().getString(R.string.home_review)+
                        " "+String.valueOf(dpo.getPost_object().getUser_business_rating())+" "+getResources().getString(R.string.home_out_of)+" 5");

                //Address
                review_tv_address.setText(dpo.getPost_object().getUser_city_name()!=null?
                        dpo.getPost_object().getUser_city_name():"");

                //Rating
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    review_rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }

                //Time
                review_tv_time.setText(BaseFunctions.processDate(ViewOriginalPostActivity.this,dpo.getPost_object().getCreated_at()));

                //Facility Image
                BaseFunctions.setFrescoImage(review_img_image,dpo.getPost_object().getBusiness_thumb_image());

                //Facility name
                review_tv_name.setText(dpo.getPost_object().getBusiness_name());

                //Facility rate
                review_rb_rate2.setRating(dpo.getPost_object().getUser_business_rating());

                //Facility Rate text
                review_tv_rate.setText(String.valueOf(dpo.getPost_object().getUser_business_rating())+" "+
                        getResources().getString(R.string.home_out_of)+" 5");

                //Review text
                review_tv_text.setText(dpo.getPost_object().getUser_business_review_text());

                //Votes count
                review_tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");

                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    review_btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    review_btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    review_btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    review_btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                    review_btn_down.setVisibility(View.GONE);
                    review_btn_up.setVisibility(View.GONE);
                }
                //todo just for now because ahmed didn't supported it yet
                review_btn_down.setVisibility(View.GONE);
                review_btn_up.setVisibility(View.GONE);
                review_btn_rolling.setVisibility(View.GONE);
                review_tv_votes_count.setVisibility(View.GONE);
                review_img_broadcast.setVisibility(View.GONE);
                /*
                review_img_broadcast.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callFollowAPI(String.valueOf(dpo.getPost_object().getBusinesses_id()),"user",
                                dpo.getPost_object().getIs_business_following()==0?"yes":"no",
                                context.getResources().getString(R.string.follow_place_true),
                                context.getResources().getString(R.string.follow_place_false),position);
                    }
                });
                 */
                review_img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusinesses_id());
                        startActivity(intent);

                    }
                });

                review_tv_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusinesses_id());
                        startActivity(intent);
                    }
                });

                review_img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        startActivity(intent);
                    }
                });
                review_tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        startActivity(intent);
                    }
                });

                review_btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)){
                            SharePostDialog dialog = new SharePostDialog(ViewOriginalPostActivity.this, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getReview_id()),"post",name);
                                }
                            });
                            dialog.show();
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            //Check In
            case 6:{
                check_layout.setVisibility(View.VISIBLE);
                //Username & Facility name
                check_tv_username.setText(Html.fromHtml("<b>"+dpo.getPost_object().getUser_name()+"</b>"+" "+
                        getResources().getString(R.string.home_is_at)+" "+"<b>"+dpo.getPost_object().getBusiness_name()+"</b>"));
                //Profile image
                if (dpo.getPost_object().getUser_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(check_img_profile_image,dpo.getPost_object().getUser_thumb_image());
                    //BaseFunctions.setGlideImage(context,checkInViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                check_tv_address.setText(dpo.getPost_object().getUser_city_name());
                //Rating
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    check_rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                if (dpo.getPost_object().getCreated_at()!=null){
                    check_tv_time.setText(BaseFunctions.processDate(ViewOriginalPostActivity.this,dpo.getPost_object().getCreated_at()));
                }
                //Facility icon
                BaseFunctions.setFrescoImage(check_img_image,dpo.getPost_object().getBusiness_thumb_image());
                //Facility Name
                check_tv_name.setText(dpo.getPost_object().getBusiness_name());
                //Facility Rate
                check_rb_rate2.setRating(Float.valueOf(dpo.getPost_object().getCheck_in_business_avg_review()));
                //Caption
                check_tv_caption.setText(dpo.getPost_object().getCaption());
                //Votes count
                check_tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    check_btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    check_btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    check_btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    check_btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                    check_tv_comment_user.setVisibility(View.GONE);
                }
                if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                    check_btn_down.setVisibility(View.GONE);
                    check_btn_up.setVisibility(View.GONE);
                }

                check_img_broadcast.setVisibility(View.GONE);

                check_tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AllCommentsDialog dialog = new AllCommentsDialog(ViewOriginalPostActivity.this,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this,SeeCommentsActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        startActivity(intent);
                    }
                });

                check_tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AllReviewsDialog dialog = new AllReviewsDialog(ViewOriginalPostActivity.this,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this,SeeReviewsActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        startActivity(intent);
                    }
                });
                check_tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        startActivity(intent);
                    }
                });

                check_img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            Intent intent = new Intent(ViewOriginalPostActivity.this, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                check_tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            Intent intent = new Intent(ViewOriginalPostActivity.this, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                check_img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        startActivity(intent);
                    }
                });
                check_tv_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        startActivity(intent);
                    }
                });
                check_btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)){
                            SharePostDialog dialog = new SharePostDialog(ViewOriginalPostActivity.this, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            //Ad
            case 7:{
                ad_layout.setVisibility(View.VISIBLE);
                //Layout
                ad_layout.setBackgroundColor(getResources().getColor(R.color.light_grey));
                //Facility name
                ad_tv_username.setText(dpo.getPost_object().getBusiness_name());
                //Facility image
                BaseFunctions.setGlideImage(ViewOriginalPostActivity.this, ad_img_profile_image, dpo.getPost_object().getBusiness_thumb_image());
                //Address
                ad_tv_address.setText(dpo.getPost_object().getBusiness_city_name()!=null?
                        dpo.getPost_object().getBusiness_city_name():"");
                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    ad_rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                ad_tv_time.setText(BaseFunctions.processDate(ViewOriginalPostActivity.this, dpo.getPost_object().getCreated_at()));
                //Sponsored
                ad_tv_sposored.setVisibility(View.VISIBLE);
                //Image
                try {
                    BaseFunctions.setFrescoImage(ad_img_image,dpo.getPost_object().getContent());
                }catch (Exception e){}

                //Sponsored
                ad_tv_sposored.setVisibility(View.VISIBLE);
                //Description
                setTags(ad_tv_desc, dpo.getPost_object().getCaption());
                //Votes count
                ad_tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    ad_btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    ad_btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    ad_btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    ad_btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                    ad_tv_comment_user.setVisibility(View.GONE);
                }
                if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                    ad_btn_down.setVisibility(View.GONE);
                    ad_btn_up.setVisibility(View.GONE);
                }

                ad_tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            //AllCommentsDialog dialog = new AllCommentsDialog(ViewOriginalPostActivity.this,"post",dpo.getPost_object().getId());
                            //dialog.show();
                            Intent intent = new Intent(ViewOriginalPostActivity.this,SeeCommentsActivity.class);
                            intent.putExtra("id",dpo.getPost_object().getId());
                            intent.putExtra("type","post");
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                ad_tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            //AllReviewsDialog dialog = new AllReviewsDialog(ViewOriginalPostActivity.this,"post",dpo.getPost_object().getId());
                            //dialog.show();
                            Intent intent = new Intent(ViewOriginalPostActivity.this,SeeReviewsActivity.class);
                            intent.putExtra("id",dpo.getPost_object().getId());
                            intent.putExtra("type","post");
                            startActivity(intent);
                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                ad_tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        startActivity(intent);
                    }
                });

                ad_img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)) {
                            try{
                                ViewImageDialog dialog = new ViewImageDialog(ViewOriginalPostActivity.this,dpo.getPost_object().getContent());
                                dialog.show();
                            }catch (Exception e){}

                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                ad_img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        startActivity(intent);
                    }
                });
                ad_tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        startActivity(intent);
                    }
                });
                ad_btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)){
                            SharePostDialog dialog = new SharePostDialog(ViewOriginalPostActivity.this, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            //Offer
            case 8:{
                offer_layout.setVisibility(View.VISIBLE);
                //Title
                offer_tv_username.setText(Html.fromHtml("<b>"+dpo.getPost_object().getBusiness_name()+"</b>"+" "+
                        getResources().getString(R.string.home_created_offer)));
                //Facility icon
                BaseFunctions.setFrescoImage(offer_img_profile_image,dpo.getPost_object().getBusiness_thumb_image());
                //Address
                offer_tv_address.setText(dpo.getPost_object().getBusiness_city_name());
                //Rating
                offer_rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                //Time
                offer_tv_time.setText(BaseFunctions.processDate(ViewOriginalPostActivity.this,dpo.getPost_object().getCreated_at()));
                //Percent
                offer_tv_percent.setText(dpo.getPost_object().getDiscount()+"%");
                //Product name
                if (lan.equals("en")) {
                    offer_tv_name.setText(dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }else if (lan.equals("ar")){
                    offer_tv_name.setText(dpo.getPost_object().getAr_title()!=null?
                            dpo.getPost_object().getAr_title():dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }
                //End date
                offer_tv_till.setText(getResources().getString(R.string.home_valid_till)+" "+
                        BaseFunctions.dateExtractorOld(dpo.getPost_object().getExpiry_date()));
                //Votes Count
                offer_tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    offer_btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    offer_btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    offer_btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    offer_btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                offer_img_add.setVisibility(View.GONE);
                if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                    offer_tv_comment_user.setVisibility(View.GONE);
                }
                if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                    offer_btn_down.setVisibility(View.GONE);
                    offer_btn_up.setVisibility(View.GONE);
                }
                offer_img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        startActivity(intent);

                    }
                });
                offer_tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ViewOriginalPostActivity.this,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        startActivity(intent);
                    }
                });
                offer_tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AllCommentsDialog dialog = new AllCommentsDialog(ViewOriginalPostActivity.this,"offer",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this,SeeCommentsActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","offer");
                        startActivity(intent);
                    }
                });
                offer_tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AllReviewsDialog dialog = new AllReviewsDialog(ViewOriginalPostActivity.this,"offer",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this,SeeReviewsActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","offer");
                        startActivity(intent);
                    }
                });
                offer_tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"offer",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(ViewOriginalPostActivity.this, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","offer");
                        startActivity(intent);
                    }
                });

                offer_btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(ViewOriginalPostActivity.this)){
                            SharePostDialog dialog = new SharePostDialog(ViewOriginalPostActivity.this, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"offer",name);
                                }
                            });
                            dialog.show();
                            dialog.show();

                        }else {
                            Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
        }
    }

    private void setTags(TextView pTextView, String pTagString) {
        SpannableString string = new SpannableString(pTagString);

        int start = -1;
        for (int i = 0; i < pTagString.length(); i++) {
            if (pTagString.charAt(i) == '#') {
                start = i;
            } else if (pTagString.charAt(i) == ' ' || (i == pTagString.length() - 1 && start != -1)) {
                if (start != -1) {
                    if (i == pTagString.length() - 1) {
                        i++; // case for if hash is last word and there is no
                        // space after word
                    }

                    final String tag = pTagString.substring(start, i);
                    string.setSpan(new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {
                            //iHash.onHashTagClicked(tag);
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            // link color
                            ds.setColor(Color.parseColor("#F7931D"));
                            ds.setUnderlineText(false);
                        }
                    }, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    start = -1;
                }
            }
        }

        pTextView.setMovementMethod(LinkMovementMethod.getInstance());
        pTextView.setText(string);
    }

    private void callShareAPI(String id,String type,String sharing_text){
        ShareAPIsClass.share(
                ViewOriginalPostActivity.this,
                BaseFunctions.getDeviceId(ViewOriginalPostActivity.this),
                id,
                type,
                sharing_text,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.home_share_success), Toast.LENGTH_SHORT).show();
                        BaseFunctions.playMusic(ViewOriginalPostActivity.this,R.raw.added);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(ViewOriginalPostActivity.this, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
