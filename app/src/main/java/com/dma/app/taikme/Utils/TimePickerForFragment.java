package com.dma.app.taikme.Utils;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;

import androidx.fragment.app.DialogFragment;

public class TimePickerForFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    private TimePickerForFragment.OnTimeReceiveCallBack mListener;
    private Context context;

    public interface OnTimeReceiveCallBack {
        public void onTimeReceive(int hh ,int mm);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        try {
            mListener = (TimePickerForFragment.OnTimeReceiveCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnTimeSetListener");
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mListener.onTimeReceive(hourOfDay,minute);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of DatePickerDialog and return it
        return new TimePickerDialog(context, this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }
}
