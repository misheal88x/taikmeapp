package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class UpdateFCMResponse {
    @SerializedName("status") private boolean status = false;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
