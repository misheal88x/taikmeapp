package com.dma.app.taikme.Models.HomeModels;

import com.dma.app.taikme.Models.DeviceSettingsObject;
import com.google.gson.annotations.SerializedName;

public class HomePostObject {
    //Offer
    //Event
    //Post
    @SerializedName("id") private int id = 0;
    //Offer
    //Event
    @SerializedName("business_id") private int business_id = 0;
    //Offer
    //Event
    @SerializedName("en_title") private String en_title = "";
    //Offer
    //Event
    @SerializedName("ar_title") private String ar_title = "";
    //Offer
    //Event
    @SerializedName("en_description") private String en_description = "";
    //Offer
    //Event
    @SerializedName("ar_description") private String ar_description = "";
    //Offer
    @SerializedName("expiry_date") private String expiry_date = "";
    //Offer
    @SerializedName("discount") private float discount = 0f;
    //Offer
    //Event
    @SerializedName("image") private String image = "";
    //Offer
    //Event
    //Post
    @SerializedName("created_at") private String created_at = " ";
    //Offer
    //Event
    //Post
    @SerializedName("updated_at") private String updated_at = "";
    //Offer
    //Event
    //Rate
    @SerializedName("business_name") private String business_name = "";
    //Offer
    //Event
    @SerializedName("business_mobile_number") private String business_mobile_number = "";
    //Offer
    //Event
    @SerializedName("business_email") private String business_email = "";
    //Offer
    //Event
    //Rate
    @SerializedName("business_image") private String business_image = "";
    //Offer
    //Event
    //Rate
    @SerializedName("business_thumb_image") private String business_thumb_image = "";
    //Offer
    //Event
    //Rate
    @SerializedName("business_cover_image") private String business_cover_image = "";
    //Offer
    //Event
    //Rate
    @SerializedName("business_country_name") private String business_country_name = "";
    //Offer
    //Event
    //Rate
    @SerializedName("business_city_name") private String business_city_name = "";
    //Offer
    //Event
    @SerializedName("reviews_count") private int reviews_count = 0;
    //Offer
    //Event
    @SerializedName("reviews_rating") private String reviews_rating = "";
    //Offer
    //Event
    @SerializedName("following_count") private int following_count = 0;
    //Offer
    //Event
    @SerializedName("upvote_count") private int upvoting_count = 0;
    //Offer
    //Event
    @SerializedName("downvote_count") private int downvote_count = 0;
    //Offer
    //Event
    //Post
    //Rate
    @SerializedName("business_avg_review") private String business_avg_review = "";
    //Offer
    //Event
    @SerializedName("user_vote") private int user_vote = 0;
    //Offer
    //Event
    @SerializedName("since_minute") private int since_minute = 0;
    //Offer
    @SerializedName("sharer_id") int sharer_id = 0;
    //Event
    @SerializedName("business_location_id") private int business_location_id = 0;
    //Event
    @SerializedName("event_date") private String event_date = "";
    //Event
    @SerializedName("business_location_address") private String business_location_address = "";
    //Event
    @SerializedName("business_location_lat") private float business_location_lat = 0f;
    //Event
    @SerializedName("business_location_lng") private float business_location_lng = 0f;
    //Event
    @SerializedName("going_count") private int going_count = 0;
    //Event
    @SerializedName("is_going") private int is_going = 0;
    //Post
    //Rate
    @SerializedName("user_id") private int user_id = 0;
    //Post
    @SerializedName("caption") private String caption = "";
    //Post
    @SerializedName("post_type") private int post_type = 0;
    //Post
    @SerializedName("content") private String content = "";
    //Post
    @SerializedName("checkin_business_id") private int checkin_business_id = 0;
    //Post
    @SerializedName("checkin_business_location_id") private int checkin_business_location_id = 0;
    //Post
    //Rate
    @SerializedName("user_name") private String user_name = "";
    //Post
    //Rate
    @SerializedName("user_city_name") private String user_city_name = "";
    //Post
    //Rate
    @SerializedName("user_country_name") private String user_country_name = "";
    //Post
    //Rate
    @SerializedName("user_image") private String user_image = "";
    //Post
    //Rate
    @SerializedName("user_thumb_image") private String user_thumb_image = "";
    //Post
    @SerializedName("location_id") private int location_id = 0;
    //Post
    @SerializedName("location_address") private String location_address = "";
    //Post
    @SerializedName("location_lat") private float location_lat = 0f;
    //Post
    @SerializedName("location_lng") private float location_lng = 0f;
    //Post check in
    @SerializedName("check_in_business_avg_review") private String check_in_business_avg_review = "";
    //Post
    //Event
    //Offer
    @SerializedName("is_business_following") private int is_business_following = 0;
    //Offer
    //Post check in
    //Rate
    @SerializedName("is_following") private int is_following = 0;
    //Rate
    @SerializedName("user_business_rating") private float user_business_rating = 0.0f;
    //Rate
    @SerializedName("user_business_review_text") private String user_business_review_text = "";
    //Rate
    @SerializedName("businesses_id") private int businesses_id = 0;
    @SerializedName("allow_people_to_follow") private int allow_people_to_follow = 1;
    @SerializedName("is_profile_public") private int is_profile_public = 1;
    @SerializedName("is_stories_visible") private int is_stories_visible = 1;
    @SerializedName("people_can_comment_on_posts") private int people_can_comment_on_posts = 1;
    @SerializedName("people_can_vote_posts") private int people_can_vote_posts = 1;
    //Event
    //Offer
    //Rate
    //Post
    @SerializedName("device_setting") private DeviceSettingsObject device_setting = new DeviceSettingsObject();
    //Rate
    @SerializedName("review_id") private int review_id = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(int business_id) {
        this.business_id = business_id;
    }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
    }

    public String getAr_title() {
        return ar_title;
    }

    public void setAr_title(String ar_title) {
        this.ar_title = ar_title;
    }

    public String getEn_description() {
        return en_description;
    }

    public void setEn_description(String en_description) {
        this.en_description = en_description;
    }

    public String getAr_description() {
        return ar_description;
    }

    public void setAr_description(String ar_description) {
        this.ar_description = ar_description;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getBusiness_mobile_number() {
        return business_mobile_number;
    }

    public void setBusiness_mobile_number(String business_mobile_number) {
        this.business_mobile_number = business_mobile_number;
    }

    public String getBusiness_email() {
        return business_email;
    }

    public void setBusiness_email(String business_email) {
        this.business_email = business_email;
    }

    public String getBusiness_image() {
        return business_image;
    }

    public void setBusiness_image(String business_image) {
        this.business_image = business_image;
    }

    public String getBusiness_thumb_image() {
        return business_thumb_image;
    }

    public void setBusiness_thumb_image(String business_thumb_image) {
        this.business_thumb_image = business_thumb_image;
    }

    public String getBusiness_cover_image() {
        return business_cover_image;
    }

    public void setBusiness_cover_image(String business_cover_image) {
        this.business_cover_image = business_cover_image;
    }

    public String getBusiness_country_name() {
        return business_country_name;
    }

    public void setBusiness_country_name(String business_country_name) {
        this.business_country_name = business_country_name;
    }

    public String getBusiness_city_name() {
        return business_city_name;
    }

    public void setBusiness_city_name(String business_city_name) {
        this.business_city_name = business_city_name;
    }

    public int getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(int reviews_count) {
        this.reviews_count = reviews_count;
    }

    public String getReviews_rating() {
        return reviews_rating;
    }

    public void setReviews_rating(String reviews_rating) {
        this.reviews_rating = reviews_rating;
    }

    public int getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(int following_count) {
        this.following_count = following_count;
    }

    public int getUpvoting_count() {
        return upvoting_count;
    }

    public void setUpvoting_count(int upvoting_count) {
        this.upvoting_count = upvoting_count;
    }

    public int getDownvote_count() {
        return downvote_count;
    }

    public void setDownvote_count(int downvote_count) {
        this.downvote_count = downvote_count;
    }

    public String getBusiness_avg_review() {
        return business_avg_review;
    }

    public void setBusiness_avg_review(String business_avg_review) {
        this.business_avg_review = business_avg_review;
    }

    public int getUser_vote() {
        return user_vote;
    }

    public void setUser_vote(int user_vote) {
        this.user_vote = user_vote;
    }

    public int getSince_minute() {
        return since_minute;
    }

    public void setSince_minute(int since_minute) {
        this.since_minute = since_minute;
    }

    public int getSharer_id() {
        return sharer_id;
    }

    public void setSharer_id(int sharer_id) {
        this.sharer_id = sharer_id;
    }

    public int getBusiness_location_id() {
        return business_location_id;
    }

    public void setBusiness_location_id(int business_location_id) {
        this.business_location_id = business_location_id;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getBusiness_location_address() {
        return business_location_address;
    }

    public void setBusiness_location_address(String business_location_address) {
        this.business_location_address = business_location_address;
    }

    public float getBusiness_location_lat() {
        return business_location_lat;
    }

    public void setBusiness_location_lat(float business_location_lat) {
        this.business_location_lat = business_location_lat;
    }

    public float getBusiness_location_lng() {
        return business_location_lng;
    }

    public void setBusiness_location_lng(float business_location_lng) {
        this.business_location_lng = business_location_lng;
    }

    public int getGoing_count() {
        return going_count;
    }

    public void setGoing_count(int going_count) {
        this.going_count = going_count;
    }

    public int getIs_going() {
        return is_going;
    }

    public void setIs_going(int is_going) {
        this.is_going = is_going;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public int getPost_type() {
        return post_type;
    }

    public void setPost_type(int post_type) {
        this.post_type = post_type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getCheckin_business_id() {
        return checkin_business_id;
    }

    public void setCheckin_business_id(int checkin_business_id) {
        this.checkin_business_id = checkin_business_id;
    }

    public int getCheckin_business_location_id() {
        return checkin_business_location_id;
    }

    public void setCheckin_business_location_id(int checkin_business_location_id) {
        this.checkin_business_location_id = checkin_business_location_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_city_name() {
        return user_city_name;
    }

    public void setUser_city_name(String user_city_name) {
        this.user_city_name = user_city_name;
    }

    public String getUser_country_name() {
        return user_country_name;
    }

    public void setUser_country_name(String user_country_name) {
        this.user_country_name = user_country_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_thumb_image() {
        return user_thumb_image;
    }

    public void setUser_thumb_image(String user_thumb_image) {
        this.user_thumb_image = user_thumb_image;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public String getLocation_address() {
        return location_address;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public float getLocation_lat() {
        return location_lat;
    }

    public void setLocation_lat(float location_lat) {
        this.location_lat = location_lat;
    }

    public float getLocation_lng() {
        return location_lng;
    }

    public void setLocation_lng(float location_lng) {
        this.location_lng = location_lng;
    }

    public String getCheck_in_business_avg_review() {
        return check_in_business_avg_review;
    }

    public void setCheck_in_business_avg_review(String check_in_business_avg_review) {
        this.check_in_business_avg_review = check_in_business_avg_review;
    }

    public int getIs_business_following() {
        return is_business_following;
    }

    public void setIs_business_following(int is_business_following) {
        this.is_business_following = is_business_following;
    }

    public int getIs_following() {
        return is_following;
    }

    public void setIs_following(int is_following) {
        this.is_following = is_following;
    }

    public float getUser_business_rating() {
        return user_business_rating;
    }

    public void setUser_business_rating(float user_business_rating) {
        this.user_business_rating = user_business_rating;
    }

    public String getUser_business_review_text() {
        return user_business_review_text;
    }

    public void setUser_business_review_text(String user_business_review_text) {
        this.user_business_review_text = user_business_review_text;
    }

    public int getBusinesses_id() {
        return businesses_id;
    }

    public void setBusinesses_id(int businesses_id) {
        this.businesses_id = businesses_id;
    }

    public DeviceSettingsObject getDevice_setting() {
        return device_setting;
    }

    public void setDevice_setting(DeviceSettingsObject device_setting) {
        this.device_setting = device_setting;
    }

    public int getAllow_people_to_follow() {
        return allow_people_to_follow;
    }

    public void setAllow_people_to_follow(int allow_people_to_follow) {
        this.allow_people_to_follow = allow_people_to_follow;
    }

    public int getIs_profile_public() {
        return is_profile_public;
    }

    public void setIs_profile_public(int is_profile_public) {
        this.is_profile_public = is_profile_public;
    }

    public int getIs_stories_visible() {
        return is_stories_visible;
    }

    public void setIs_stories_visible(int is_stories_visible) {
        this.is_stories_visible = is_stories_visible;
    }

    public int getPeople_can_comment_on_posts() {
        return people_can_comment_on_posts;
    }

    public void setPeople_can_comment_on_posts(int people_can_comment_on_posts) {
        this.people_can_comment_on_posts = people_can_comment_on_posts;
    }

    public int getPeople_can_vote_posts() {
        return people_can_vote_posts;
    }

    public void setPeople_can_vote_posts(int people_can_vote_posts) {
        this.people_can_vote_posts = people_can_vote_posts;
    }

    public int getReview_id() {
        return review_id;
    }

    public void setReview_id(int review_id) {
        this.review_id = review_id;
    }
}
