package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ProfilePayloadObject {
    @SerializedName("places") private ProfilePlacesResponse places = new ProfilePlacesResponse();
    @SerializedName("media") private ProfileMediaResponse media = new ProfileMediaResponse();
    @SerializedName("reviews") private ProfileReviewsResponse reviews = new ProfileReviewsResponse();

    public ProfilePlacesResponse getPlaces() {
        return places;
    }

    public void setPlaces(ProfilePlacesResponse places) {
        this.places = places;
    }

    public ProfileMediaResponse getMedia() {
        return media;
    }

    public void setMedia(ProfileMediaResponse media) {
        this.media = media;
    }

    public ProfileReviewsResponse getReviews() {
        return reviews;
    }

    public void setReviews(ProfileReviewsResponse reviews) {
        this.reviews = reviews;
    }
}
