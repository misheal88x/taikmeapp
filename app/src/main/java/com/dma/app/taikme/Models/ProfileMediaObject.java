package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class ProfileMediaObject {
    //ok
    @SerializedName("id") private int id = 0;
    //ok
    @SerializedName("user_id") private int user_id = 0;
    //ok
    @SerializedName("caption") private String caption = "";
    //ok
    @SerializedName("content") private String content = "";
    //ok
    @SerializedName("post_type") private int post_type = 0;
    @SerializedName("posted_since_minute") private int posted_since_minute = 0;
    //ok
    @SerializedName("upvote_count") private int upvoting_count = 0;
    //ok
    @SerializedName("downvote_count") private int downvote_count = 0;
    //ok
    @SerializedName("user_vote") private int user_vote = 0;
    //ok
    @SerializedName("created_at") private String created_at = "";
    //ok
    @SerializedName("checkin_business_id") private int checkin_business_id = 0;
    //ok
    @SerializedName("checkin_business_location_id") private int checkin_business_location_id = 0;
    //ok
    @SerializedName("user_name") private String user_name = "";
    //ok
    @SerializedName("user_city_name") private String user_city_name = "";
    //ok
    @SerializedName("user_country_name") private String user_country_name = "";
    //ok
    @SerializedName("user_image") private String user_image = "";
    //ok
    @SerializedName("user_thumb_image") private String user_thumb_image = "";
    //ok
    @SerializedName("location_id") private int location_id = 0;
    //ok
    @SerializedName("location_address") private String location_address = "";
    //ok
    @SerializedName("location_lat") private float location_lat = 0f;
    //ok
    @SerializedName("location_lng") private float location_lng = 0f;
    //ok
    @SerializedName("business_avg_review") private String business_avg_review = "";
    //ok
    @SerializedName("business_name") private String business_name = "";
    //ok
    @SerializedName("business_image") private String business_image = "";
    //ok
    @SerializedName("business_thumb_image") private String business_thumb_image = "";
    //ok
    @SerializedName("business_city_name") private String business_city_name = "";
    //ok
    @SerializedName("business_id") private int business_id = 0;
    //ok
    @SerializedName("check_in_business_avg_review") private String check_in_business_avg_review = "";
    //ok
    @SerializedName("is_following") private int is_following = 0;
    //ok
    @SerializedName("device_setting") private DeviceSettingsObject deviceSettingsObject = new DeviceSettingsObject();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getPost_type() {
        return post_type;
    }

    public void setPost_type(int post_type) {
        this.post_type = post_type;
    }

    public int getPosted_since_minute() {
        return posted_since_minute;
    }

    public void setPosted_since_minute(int posted_since_minute) {
        this.posted_since_minute = posted_since_minute;
    }

    //public int getUpvoting_count() {
    //    return upvoting_count;
    //}

    //public void setUpvoting_count(int upvoting_count) {
     //   this.upvoting_count = upvoting_count;
   // }

    //public int getDownvote_count() {
    //    return downvoting_count;
   // }

   // public void setDownvote_count(int downvoting_count) {
    //    this.downvoting_count = downvoting_count;
    //}

   // public int getUser_vote() {
   //     return user_vote;
   // }

   // public void setUser_vote(int user_vote) {
    //    this.user_vote = user_vote;
   // }


    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getCheckin_business_id() {
        return checkin_business_id;
    }

    public void setCheckin_business_id(int checkin_business_id) {
        this.checkin_business_id = checkin_business_id;
    }

    public int getCheckin_business_location_id() {
        return checkin_business_location_id;
    }

    public void setCheckin_business_location_id(int checkin_business_location_id) {
        this.checkin_business_location_id = checkin_business_location_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_city_name() {
        return user_city_name;
    }

    public void setUser_city_name(String user_city_name) {
        this.user_city_name = user_city_name;
    }

    public String getUser_country_name() {
        return user_country_name;
    }

    public void setUser_country_name(String user_country_name) {
        this.user_country_name = user_country_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_thumb_image() {
        return user_thumb_image;
    }

    public void setUser_thumb_image(String user_thumb_image) {
        this.user_thumb_image = user_thumb_image;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public String getLocation_address() {
        return location_address;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public float getLocation_lat() {
        return location_lat;
    }

    public void setLocation_lat(float location_lat) {
        this.location_lat = location_lat;
    }

    public float getLocation_lng() {
        return location_lng;
    }

    public void setLocation_lng(float location_lng) {
        this.location_lng = location_lng;
    }

    public int getUpvoting_count() {
        return upvoting_count;
    }

    public void setUpvoting_count(int upvoting_count) {
        this.upvoting_count = upvoting_count;
    }

    public int getDownvote_count() {
        return downvote_count;
    }

    public void setDownvote_count(int downvote_count) {
        this.downvote_count = downvote_count;
    }

    public int getUser_vote() {
        return user_vote;
    }

    public void setUser_vote(int user_vote) {
        this.user_vote = user_vote;
    }

    public DeviceSettingsObject getDeviceSettingsObject() {
        return deviceSettingsObject;
    }

    public void setDeviceSettingsObject(DeviceSettingsObject deviceSettingsObject) {
        this.deviceSettingsObject = deviceSettingsObject;
    }

    public String getBusiness_avg_review() {
        return business_avg_review;
    }

    public void setBusiness_avg_review(String business_avg_review) {
        this.business_avg_review = business_avg_review;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getBusiness_image() {
        return business_image;
    }

    public void setBusiness_image(String business_image) {
        this.business_image = business_image;
    }

    public String getBusiness_thumb_image() {
        return business_thumb_image;
    }

    public void setBusiness_thumb_image(String business_thumb_image) {
        this.business_thumb_image = business_thumb_image;
    }

    public String getBusiness_city_name() {
        return business_city_name;
    }

    public void setBusiness_city_name(String business_city_name) {
        this.business_city_name = business_city_name;
    }

    public int getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(int business_id) {
        this.business_id = business_id;
    }

    public String getCheck_in_business_avg_review() {
        return check_in_business_avg_review;
    }

    public void setCheck_in_business_avg_review(String check_in_business_avg_review) {
        this.check_in_business_avg_review = check_in_business_avg_review;
    }

    public int getIs_following() {
        return is_following;
    }

    public void setIs_following(int is_following) {
        this.is_following = is_following;
    }
}
