package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class SharePostDialog extends AlertDialog {
    private Context context;
    private Button btn_share;
    private EditText edt_share_text;
    private IEditName inter;

    public SharePostDialog(@NonNull Context context, IEditName inter) {
        super(context);
        this.context = context;
        this.inter = inter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_share_post);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //Button
        btn_share = findViewById(R.id.share);
        //EditText
        edt_share_text = findViewById(R.id.share_text);

    }

    private void init_events(){
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_share_text.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.share_no_text), Toast.LENGTH_SHORT).show();
                    return;
                }
                inter.onNameWritten(edt_share_text.getText().toString());
                dismiss();
            }
        });
    }

    private void init_dialog(){

    }
}
