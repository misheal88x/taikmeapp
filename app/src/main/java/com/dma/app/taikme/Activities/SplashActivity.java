package com.dma.app.taikme.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.BasicAPIsClass;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.StartupObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

public class SplashActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private RelativeLayout root;

    private static int SPLASH_TIME_OUT=3000;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 10;
    //---------------------------------------------------------------------------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    //---------------------------------------------------------------------------

    @Override
    public void set_layout() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FullScreencall();
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
       // if (isOnline()){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                            ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
                        requestRuntimePermission();
                    }else {
                        if (checkPlayServices()){
                            //---------------------------------------------------------------------------
                            mGoogleApiClient = new GoogleApiClient.Builder(SplashActivity.this)
                                    .addApi(LocationServices.API)
                                    .addConnectionCallbacks(SplashActivity.this)
                                    .addOnConnectionFailedListener(SplashActivity.this).build();
                            mGoogleApiClient.connect();

                            //-----------------------------------------------------------------------------
                        }
                    }
                }
            },0);
            /*
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
            builder.setMessage(getResources().getString(R.string.no_internet_dialog)).
                    setCancelable(false).
                    setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }).show();
        }
        **/
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode!= ConnectionResult.SUCCESS){
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();

            }else {
                Toast.makeText(this, getResources().getString(R.string.no_google_play_services), Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    public void FullScreencall() {
        if(Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if(Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    private void requestRuntimePermission() {
        ActivityCompat.requestPermissions(this,new String[]
                {
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                },11);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 11:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    if (checkPlayServices()){
                        //---------------------------------------------------------------------------
                        mGoogleApiClient = new GoogleApiClient.Builder(SplashActivity.this)
                                .addApi(LocationServices.API)
                                .addConnectionCallbacks(SplashActivity.this)
                                .addOnConnectionFailedListener(SplashActivity.this).build();
                        mGoogleApiClient.connect();

                        //-----------------------------------------------------------------------------
                    }
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                    builder.setMessage(getResources().getString(R.string.permission_denied)).setCancelable(false).
                            setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(Intent.ACTION_MAIN);
                                    intent.addCategory(Intent.CATEGORY_HOME);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }).show();
                }
                break;
            }
        }
    }

    public boolean isOnline(){
        try{
            ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conMgr != null){
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null){
                    return false;
                }else {
                    return true;
                }
            }else {
                return false;
            }
        }catch (Exception e){
            Toast.makeText(this, "Crashed because of isOnline function", Toast.LENGTH_SHORT).show();
            return false;
        }
    }



    @Override
    public void init_views() {
        //RelativeLayout
        root = findViewById(R.id.splash_layout);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        //handleSplash();
                        callAPI();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                        try {
                            status.startResolutionForResult(
                                    SplashActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                    default:{
                        callAPI();
                        //handleSplash();
                    }
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode)
        {
            case REQUEST_LOCATION:
                switch (resultCode)
                {
                    case Activity.RESULT_OK:
                    {
                        Toast.makeText(SplashActivity.this, getResources().getString(R.string.gps_on), Toast.LENGTH_LONG).show();
                        //handleSplash();
                         callAPI();
                        break;
                    }
                    case Activity.RESULT_CANCELED:
                    {
                        callAPI();
                        /*
                        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                        builder.setMessage(getResources().getString(R.string.gps_off)).setCancelable(false).
                                setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                        intent.addCategory(Intent.CATEGORY_HOME);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                }).show();
                                **/
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
        }
    }

    private void handleSplash(){
        if (SharedPrefManager.getInstance(SplashActivity.this).getUser()!=null
                &&SharedPrefManager.getInstance(SplashActivity.this).getUser().getId() == 0){
            startActivity(new Intent(SplashActivity.this,LoginActivity.class));
            finish();
        } else if (SharedPrefManager.getInstance(SplashActivity.this).getUser()!=null){
            if (SharedPrefManager.getInstance(SplashActivity.this).getAccountActivated()) {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                intent.putExtra("type", "home");
                startActivity(intent);
                finish();
            }else {
                startActivity(new Intent(SplashActivity.this,ConfirmAccountActivity.class));
                finish();
            }
        }else {
            if (SharedPrefManager.getInstance(SplashActivity.this).getAccountActivated()){
                startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                finish();
            }else {
                startActivity(new Intent(SplashActivity.this,ConfirmAccountActivity.class));
                finish();
            }
        }
    }

    private void callAPI(){
        BasicAPIsClass.startup(SplashActivity.this,
                BaseFunctions.getDeviceId(SplashActivity.this),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            StartupObject success = new Gson().fromJson(j,StartupObject.class);
                            SharedPrefManager.getInstance(SplashActivity.this).setStartUp(success);
                            handleSplash();
                        }else {
                            Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            callAPI();
                                        }
                                    }).setActionTextColor(getResources().getColor(R.color.white)).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
