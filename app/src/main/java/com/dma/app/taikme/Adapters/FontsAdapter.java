package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class FontsAdapter extends  RecyclerView.Adapter<FontsAdapter.ViewHolder> {
    private Context context;
    private List<String> list;
    private FontAdapterClickListener listener;
    private int row_selected = -1;
    public FontsAdapter(Context context,FontAdapterClickListener listener) {
        this.context = context;
        this.list = loadFontList();
        this.listener = listener;
    }

    public interface FontAdapterClickListener{
        void onFontSelected(String fontName);
    }

    private List<String> loadFontList(){
        List<String> result = new ArrayList<>();
        result.add("Antonio.ttf");
        result.add("Granada.ttf");
        result.add("GreatVibes.otf");
        result.add("Lobster.otf");
        result.add("Nurkholis.ttf");
        result.add("OpenSans.ttf");
        result.add("Pacifico.ttf");
        result.add("STC.ttf");
        result.add("tahoma.ttf");
        result.add("Vtks.ttf");
        return result;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_check;
        private TextView tv_demo;
        private TextView tv_title;
        private CardView layout;

        public ViewHolder(View view) {
            super(view);
            iv_check = view.findViewById(R.id.item_font_check);
            tv_demo = view.findViewById(R.id.item_font_demo);
            tv_title = view.findViewById(R.id.item_font_title);
            layout = view.findViewById(R.id.item_font_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_font, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        if (row_selected == position){
            holder.iv_check.setVisibility(View.VISIBLE);
        }else {
            holder.iv_check.setVisibility(View.GONE);
        }
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),new StringBuilder("fonts/").
                append(list.get(position)).toString());
        holder.tv_title.setText(list.get(position).substring(0,list.get(position).length()-4));
        holder.tv_demo.setTypeface(typeface);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onFontSelected(list.get(position));
                row_selected = position;
                notifyDataSetChanged();
            }
        });

    }
}
