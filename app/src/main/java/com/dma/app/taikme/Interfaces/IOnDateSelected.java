package com.dma.app.taikme.Interfaces;

public interface IOnDateSelected {
    void onDateSelected(String selected_date);
}
