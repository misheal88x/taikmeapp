package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class InputValueDialog  extends AlertDialog {
    private Context context;
    private TextView tv_title,tv_ok;
    private EditText edt_value;
    private IEditName iEditName;
    private String title = "",hint = "",type = "";
    public InputValueDialog(@NonNull Context context,String title,String hint,String type,IEditName iEditName) {
        super(context);
        this.context = context;
        this.title = title;
        this.hint = hint;
        this.type = type;
        this.iEditName = iEditName;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_input_value);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //EditText
        edt_value = findViewById(R.id.value);
        //TextView
        tv_title = findViewById(R.id.title);
        tv_ok = findViewById(R.id.ok_btn);
    }

    private void init_events(){
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_value.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.input_value_no_text), Toast.LENGTH_SHORT).show();
                    return;
                }
                iEditName.onNameWritten(edt_value.getText().toString());
            }
        });
    }

    private void init_dialog(){
        tv_title.setText(title);
        edt_value.setHint(hint);
        switch (type){
            case "number":{
                edt_value.setInputType(InputType.TYPE_CLASS_NUMBER);
            }break;
        }
    }
}
