package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;
import rx.Emitter;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Cancellable;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.dma.app.taikme.Models.StoryObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Services.UploadStoryService;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.github.tcking.giraffecompressor.GiraffeCompressor;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.io.File;

public class VideoStatusActivity extends BaseActivity {

    private VideoView vv_video;
    private ImageView btn_play;
    private TextView btn_upload;
    private Intent myIntent;
    private boolean isVideoStarted = false;
    private boolean is_video_ended = true;
    private int stopPosition = 0;
    private String videoPath = "";

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_video_status);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        videoPath = myIntent.getStringExtra("video_path");
        vv_video.setVideoPath(myIntent.getStringExtra("video_path"));
        vv_video.seekTo(1);
    }

    @Override
    public void init_views() {
        //VideoView
        vv_video = findViewById(R.id.video_status_view);
        //ImageView
        btn_play = findViewById(R.id.video_status_play);
        //TextView
        btn_upload = findViewById(R.id.video_status_upload);
        //Intent
        myIntent = getIntent();
    }

    @Override
    public void init_events() {
        vv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isVideoStarted){
                    vv_video.pause();
                    stopPosition = vv_video.getCurrentPosition();
                    btn_play.setVisibility(View.VISIBLE);
                    isVideoStarted = false;
                }else {
                    if (is_video_ended){
                        vv_video.start();
                        is_video_ended = false;
                    }else {
                        vv_video.seekTo(stopPosition);
                        vv_video.start();
                    }

                    btn_play.setVisibility(View.GONE);
                    isVideoStarted = true;
                }
            }
        });
        vv_video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                vv_video.setVideoPath(myIntent.getStringExtra("video_path"));
                is_video_ended = true;
                isVideoStarted = false;
                btn_play.setVisibility(View.VISIBLE);
            }
        });
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(VideoStatusActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.create_status_prepare_video));
                progressDialog.setCancelable(false);
                progressDialog.show();

                File oldFile = new File(videoPath);
                final String newPath = String.format("/sdcard/%d.mp4", System.currentTimeMillis());
                File newFile = new File(newPath);
                GiraffeCompressor.init(VideoStatusActivity.this);
                GiraffeCompressor.create("ffmpeg")
                        .input(oldFile)
                        .output(newFile)
                        .bitRate(690000)
                        .resizeFactor(1.0f)
                        .ready()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Emitter<GiraffeCompressor.Result>() {
                            @Override
                            public void setSubscription(Subscription s) {

                            }

                            @Override
                            public void setCancellation(Cancellable c) {

                            }

                            @Override
                            public long requested() {
                                return 0;
                            }

                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(GiraffeCompressor.Result s) {
                                progressDialog.cancel();
                                BaseFunctions.uploadStory(VideoStatusActivity.this,StoryObject.TYPE_VIDEO,"story",newPath);
                                Toast.makeText(VideoStatusActivity.this, getResources().getString(R.string.create_status_creating), Toast.LENGTH_SHORT).show();
                                /*
                                Intent intent = new Intent(VideoStatusActivity.this, UploadStoryService.class);
                                intent.putExtra("service_type","story");
                                intent.putExtra("file_type",StoryObject.TYPE_VIDEO);
                                intent.putExtra("content","story");
                                intent.putExtra("file_path",newPath);
                                startService(intent);
                                **/
                                Intent intent1 = new Intent(VideoStatusActivity.this,HomeActivity.class);
                                intent1.putExtra("type","home");
                                startActivity(intent1);
                                finish();

                            }
                        });
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }


}
