package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dma.app.taikme.APIs.BasicAPIs;
import com.dma.app.taikme.APIsClass.BasicAPIsClass;
import com.dma.app.taikme.Adapters.QuestionsAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.FaqObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class QuestionsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private RelativeLayout btn_back;
    private TextView tv_title;
    private RecyclerView rv_questions;
    private List<FaqObject> listOfFaqs;
    private QuestionsAdapter adapter;
    private LinearLayoutManager layoutManager;
    private RelativeLayout root;
    private LinearLayout no_data;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_questions);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.questions_title));
        init_recycler();
    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = findViewById(R.id.questions_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        //Recycler view
        rv_questions = findViewById(R.id.questions_recycler);
        //LinearLayout
        root = findViewById(R.id.questions_layout);
        no_data = findViewById(R.id.no_data_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(QuestionsActivity.this);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        listOfFaqs = new ArrayList<>();
        adapter = new QuestionsAdapter(QuestionsActivity.this,listOfFaqs);
        layoutManager = new LinearLayoutManager(QuestionsActivity.this,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_questions.setLayoutManager(layoutManager);
        rv_questions.setAdapter(adapter);
        callAPI();
    }

    private void callAPI(){
        no_data.setVisibility(View.VISIBLE);
        BasicAPIsClass.getQuestions(
                QuestionsActivity.this,
                BaseFunctions.getDeviceId(QuestionsActivity.this),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        no_data.setVisibility(View.VISIBLE);
                        Snackbar.make(root, getResources().getString(R.string.error_occurred), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            FaqObject[] success = new Gson().fromJson(j,FaqObject[].class);
                            if (success.length > 0){
                                no_data.setVisibility(View.GONE);
                                for (FaqObject fo : success){
                                    listOfFaqs.add(fo);
                                    adapter.notifyDataSetChanged();
                                }
                                BaseFunctions.runAnimation(rv_questions,0,adapter);
                            }else {
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }else {
                            no_data.setVisibility(View.VISIBLE);
                            Snackbar.make(root, getResources().getString(R.string.error_occurred), Snackbar.LENGTH_INDEFINITE)
                                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            callAPI();
                                        }
                                    }).setActionTextColor(getResources().getColor(R.color.white)).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        no_data.setVisibility(View.VISIBLE);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }
}
