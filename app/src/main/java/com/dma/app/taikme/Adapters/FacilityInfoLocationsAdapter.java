package com.dma.app.taikme.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dma.app.taikme.Activities.ShowLocationMapActivity;
import com.dma.app.taikme.Models.BusinessLocatonNewObject;
import com.dma.app.taikme.R;

import java.util.List;

public class FacilityInfoLocationsAdapter extends  RecyclerView.Adapter<FacilityInfoLocationsAdapter.ViewHolder> {
    private Context context;
    private List<BusinessLocatonNewObject> list;
    private String[] texts = new String[]{
            "Floor, Building, Street, \n Area, District, Governorate"
    };
    public FacilityInfoLocationsAdapter(Context context,List<BusinessLocatonNewObject> list) {
        this.context = context;
        this.list = list;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_text;
        private CardView btn_takeme;

        public ViewHolder(View view) {
            super(view);
            tv_text = view.findViewById(R.id.item_facility_info_locations_text);
            btn_takeme = view.findViewById(R.id.item_facility_info_locations_btn);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_facility_info_locations, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final BusinessLocatonNewObject o = list.get(position);
        holder.tv_text.setText(o.getLocation_address()!=null?o.getLocation_address():"");
        holder.btn_takeme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ShowLocationMapActivity.class);
                intent.putExtra("lat",o.getLat());
                intent.putExtra("lng",o.getLng());
                context.startActivity(intent);
            }
        });
    }
}
