package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class EditNameDialog extends AlertDialog {
    private Context context;
    private String name;
    private EditText edt_name;
    private Button btn_save;
    private IEditName inter;

    public EditNameDialog(@NonNull Context context,String name,IEditName inter) {
        super(context);
        this.context = context;
        this.name = name;
        this.inter = inter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_edit_name);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //EditText
        edt_name = findViewById(R.id.edit_name_user_name);
        //Button
        btn_save = findViewById(R.id.edit_name_save);

    }

    private void init_events(){
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_name.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.edit_name_no_name), Toast.LENGTH_SHORT).show();
                    return;
                }
                callUpdateNameAPI();
            }
        });
    }

    private void init_dialog(){
        edt_name.setText(name);
    }

    private void callUpdateNameAPI(){
        UserAPIsClass.updateName(
                context,
                BaseFunctions.getDeviceId(context),
                edt_name.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        Toast.makeText(context, context.getResources().getString(R.string.edit_name_success), Toast.LENGTH_SHORT).show();
                        inter.onNameWritten(edt_name.getText().toString());
                        dismiss();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
