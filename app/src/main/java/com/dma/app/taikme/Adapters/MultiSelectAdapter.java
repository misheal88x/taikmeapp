package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Models.MultiSelectObject;
import com.dma.app.taikme.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MultiSelectAdapter  extends  RecyclerView.Adapter<MultiSelectAdapter.ViewHolder>{
    private Context context;
    private List<MultiSelectObject> list;
    //private List<String> list;
    private List<Integer> checked;

    public MultiSelectAdapter(Context context,List<MultiSelectObject> list) {
        this.context = context;
        this.list = list;
        this.checked = new ArrayList<>();
        /*
        if (this.list.size()>0){
            for (int i = 0; i < this.list.size(); i++) {
                if (this.list.get(i).isIs_checked()){
                    checked.add(i);
                }
            }
        }
         */
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CheckBox chk_check;
        public ViewHolder(View view) {
            super(view);
            chk_check = view.findViewById(R.id.item_check);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_check, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.chk_check.setText(list.get(position).getName());
        if (list.get(position).isIs_checked()){
            holder.chk_check.setChecked(true);
            if (!checked.contains(position)) {
                checked.add(position);
            }
        }else {
            holder.chk_check.setChecked(false);
        }
        holder.chk_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checked.add(position);
                    MultiSelectObject o = list.get(position);
                    o.setIs_checked(true);
                    list.set(position,o);
                }else {
                    try {
                        checked.remove(checked.indexOf(position));
                        MultiSelectObject o = list.get(position);
                        o.setIs_checked(false);
                        list.set(position, o);
                    }catch (Exception e){
                        Log.i("jffhbgv", "onCheckedChanged: "+e.getMessage());
                    }
                }
            }
        });
    }

    public List<Integer> getCheckedItems(){
        return checked;
    }
}
