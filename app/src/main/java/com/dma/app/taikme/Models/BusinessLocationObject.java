package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class BusinessLocationObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("business_id") private int business_id = 0;
    @SerializedName("location_address") private String location_address = "";
    @SerializedName("lat") private float lat = 0f;
    @SerializedName("lng") private float lng = 0f;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(int business_id) {
        this.business_id = business_id;
    }

    public String getLocation_address() {
        return location_address;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }
}
