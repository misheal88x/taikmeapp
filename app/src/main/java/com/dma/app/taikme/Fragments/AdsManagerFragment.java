package com.dma.app.taikme.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dma.app.taikme.APIsClass.AmplifyAPIsClass;
import com.dma.app.taikme.APIsClass.PlacesAPIsClass;
import com.dma.app.taikme.Adapters.AdsManagerAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.AdObject;
import com.dma.app.taikme.Models.AdsResponse;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.PlacesResponse;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.EndlessRecyclerViewScrollListener;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AdsManagerFragment extends BaseFragment {

    private RecyclerView rv_ads;
    private List<AdObject> list;
    private AdsManagerAdapter adapter_ads;
    private LinearLayoutManager ads_layout_manager;
    private LinearLayout no_data_layout;
    private RelativeLayout root;
    private AVLoadingIndicatorView more;
    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ads_manager,container,false);
    }

    @Override
    public void init_views() {
        //RecyclerView
        rv_ads = base.findViewById(R.id.ads_manager_recycler);
        //RelativeLayout
        root = base.findViewById(R.id.ads_manager_layout);
        //LinearLayout
        no_data_layout = base.findViewById(R.id.no_data_layout);
        //LoadingBar
        more = base.findViewById(R.id.more);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        callAdsAPI(currentPage,0);
    }

    private void init_recycler(){
        list = new ArrayList<>();
        ads_layout_manager = new LinearLayoutManager(base,RecyclerView.VERTICAL,false);
        adapter_ads = new AdsManagerAdapter(base, list,new IMove() {
            @Override
            public void move() {

            }

            @Override
            public void move(int position) {
                String lan = SharedPrefManager.getInstance(base).getDeviceSetting().getView_language();
                InsightsFragment insightsFragment = new InsightsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("title",lan.equals("ar")?list.get(position).getAr_name():
                        list.get(position).getEn_name());
                bundle.putString("date",BaseFunctions.dateExtractor(list.get(position).getCreated_at()));
                insightsFragment.setArguments(bundle);
                base.open_fragment(insightsFragment);
            }
        });
        rv_ads.setLayoutManager(ads_layout_manager);
        rv_ads.setAdapter(adapter_ads);
        rv_ads.addOnScrollListener(new EndlessRecyclerViewScrollListener(ads_layout_manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (list.size()>=per_page){
                    if (continue_paginate){
                        currentPage++;
                        callAdsAPI(currentPage,1);
                    }
                }
            }
        });
    }

    private void callAdsAPI(final int page,final int type){
        if (type == 1){
            more.smoothToShow();
        }
        AmplifyAPIsClass.get_all(
                base,
                BaseFunctions.getDeviceId(base),
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            AdsResponse success = new Gson().fromJson(j,AdsResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (AdObject po : success.getData()){
                                        list.add(po);
                                        adapter_ads.notifyDataSetChanged();
                                    }
                                    if (type == 0){
                                        BaseFunctions.runAnimation(rv_ads,0,adapter_ads);
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAdsAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            more.smoothToHide();
        }else {
            no_data_layout.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.GONE);
            rv_ads.setVisibility(View.VISIBLE);
        }else {
            more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.VISIBLE);
        }else {
            more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
