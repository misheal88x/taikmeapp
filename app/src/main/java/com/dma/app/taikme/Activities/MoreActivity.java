package com.dma.app.taikme.Activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.CategoriesAPIsClass;
import com.dma.app.taikme.Adapters.CategoriesAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.ILoadImage;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.CategoryObject;
import com.dma.app.taikme.Models.DeviceObject;
import com.dma.app.taikme.Models.DeviceSettingsObject;
import com.dma.app.taikme.Models.UserObject;
import com.dma.app.taikme.Others.App;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class MoreActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private RelativeLayout btn_back;
    private CircleImageView img_profile;
    private LinearLayout lyt_trending_places,lyt_people_to_follow,lyt_my_business,
            lyt_ads_manager,lyt_events,lyt_logout;
    private RelativeLayout btn_questions,btn_settings,btn_places_visited,btn_places_reviewed;

    private RecyclerView recycler;
    private AVLoadingIndicatorView loading;
    private List<CategoryObject> list;
    private CategoriesAdapter adapter;
    private LinearLayoutManager layoutManager;

    private LinearLayout root;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_more);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        if (SharedPrefManager.getInstance(MoreActivity.this).getUser().getImage()!=null){
            if (!SharedPrefManager.getInstance(MoreActivity.this).getUser().getImage().equals("")){
                BaseFunctions.setGlideImageWithEvents(MoreActivity.this,
                        img_profile,
                        SharedPrefManager.getInstance(MoreActivity.this).getUser().getImage(), new ILoadImage() {
                            @Override
                            public void onLoaded() {

                            }

                            @Override
                            public void onFailed() {
                                img_profile.setImageDrawable(getResources().getDrawable(R.drawable.dark_grey_circle));
                            }
                        });
            }
        }
        init_recycler();
        if (App.categories_json.equals("")) {
            callCategoriesAPI();
        }else {
            CategoryObject[] c_array = new Gson().fromJson(App.categories_json,CategoryObject[].class);
            if (c_array.length>0){
                for (CategoryObject c : c_array){
                    list.add(c);
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = findViewById(R.id.more_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        //Circle Image Views
        img_profile = findViewById(R.id.more_profile);
        //Linear Layout
        lyt_trending_places = findViewById(R.id.more_trending_places);
        lyt_people_to_follow = findViewById(R.id.more_people_to_follow);
        lyt_my_business = findViewById(R.id.more_my_business);
        lyt_ads_manager = findViewById(R.id.more_ads_manager);
        lyt_events = findViewById(R.id.more_events);
        root = findViewById(R.id.layout);
        lyt_logout = findViewById(R.id.more_logout);
        //RelativeLayouts
        btn_questions = findViewById(R.id.more_questions);
        btn_settings = findViewById(R.id.more_settings);
        btn_places_visited = findViewById(R.id.more_places_visited);
        btn_places_reviewed = findViewById(R.id.more_places_reviewed);
        //RecyclerView
        recycler = findViewById(R.id.recycler);
        //ProgressBar
        loading = findViewById(R.id.loading);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MoreActivity.this,HomeActivity.class);
                intent.putExtra("type","home");
                startActivity(intent);
                finish();

                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MoreActivity.this,HomeActivity.class);
                intent.putExtra("type","profile");
                intent.putExtra("profile_type","me");
                startActivity(intent);
            }
        });
        lyt_trending_places.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MoreActivity.this,MoreListActivity.class);
                intent.putExtra("image",R.drawable.ic_more_globe);
                intent.putExtra("title",getResources().getString(R.string.more_trending_places));
                intent.putExtra("type","trending_places");
                startActivity(intent);
            }
        });
        lyt_people_to_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MoreActivity.this,PeopleToFollowActivity.class));
            }
        });
        btn_questions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MoreActivity.this,QuestionsActivity.class));
            }
        });
        btn_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MoreActivity.this,SettingsActivity.class));
            }
        });
        btn_places_visited.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MoreActivity.this,HomeActivity.class);
                intent.putExtra("type","places_visited");
                startActivity(intent);
            }
        });
        btn_places_reviewed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MoreActivity.this,HomeActivity.class);
                intent.putExtra("type","places_reviewed");
                startActivity(intent);
            }
        });
        lyt_my_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefManager.getInstance(MoreActivity.this).getUser().getUser_type().equals(UserObject.BUSINESS_TYPE)){
                    Intent intent = new Intent(MoreActivity.this,HomeActivity.class);
                    intent.putExtra("type","facility_info");
                    startActivity(intent);
                }else {
                    startActivity(new Intent(MoreActivity.this,MyBusinessActivity.class));
                    finish();
                }

            }
        });
        lyt_ads_manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!SharedPrefManager.getInstance(MoreActivity.this).getUser().getUser_type().equals("NORMAL_USER")){
                    Intent intent = new Intent(MoreActivity.this,HomeActivity.class);
                    intent.putExtra("type","ads_manager");
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(MoreActivity.this, getResources().getString(R.string.not_business_owner), Toast.LENGTH_SHORT).show();
                }
            }
        });
        lyt_events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MoreActivity.this,HomeActivity.class);
                intent.putExtra("type","events");
                startActivity(intent);
                finish();
            }
        });
        lyt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefManager.getInstance(MoreActivity.this).setUser(new UserObject());
                SharedPrefManager.getInstance(MoreActivity.this).setDevice(new DeviceObject());
                SharedPrefManager.getInstance(MoreActivity.this).setDeviceSetting(new DeviceSettingsObject());
                SharedPrefManager.getInstance(MoreActivity.this).setAccessToken("");
                startActivity(new Intent(MoreActivity.this,SplashActivity.class));
                finish();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new CategoriesAdapter(MoreActivity.this,list);
        layoutManager = new LinearLayoutManager(MoreActivity.this);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
    }

    private void callCategoriesAPI(){
        loading.smoothToShow();
        CategoriesAPIsClass.getCategories(
                MoreActivity.this,
                BaseFunctions.getDeviceId(MoreActivity.this),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.smoothToHide();
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.smoothToHide();
                        String j = new Gson().toJson(json);
                        App.categories_json = j;
                        try {
                            CategoryObject[] success = new Gson().fromJson(j, CategoryObject[].class);
                            if (success.length > 0) {
                                for (CategoryObject c : success) {
                                    if (c.getParent_id() == 0) {
                                        list.add(c);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }catch (Exception e){
                            callCategoriesAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.smoothToHide();
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callCategoriesAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MoreActivity.this,HomeActivity.class);
        intent.putExtra("type","home");
        startActivity(intent);
        finish();
    }
}
