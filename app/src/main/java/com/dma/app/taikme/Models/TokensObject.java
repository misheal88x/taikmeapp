package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class TokensObject {
    @SerializedName("access_token") private String access_token = "";

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
