package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserObject {

    public static final String USER_TYPE = "NORMAL_USER";
    public static final String BUSINESS_TYPE = "BUSINESS";

    //ok
    @SerializedName("id") private int id = 0;
    //ok
    @SerializedName("name") private String name = "";
    //ok
    @SerializedName("email") private String email = "";
    //ok
    @SerializedName("gender") private String gender = "";
    //ok
    @SerializedName("birthday") private String birthday = "";
    //Not used
    //@SerializedName("country_id") private int country_id = 0;
    //ok
    @SerializedName("city_name") private String city_name = "";
    //todo missing
    @SerializedName("best_cuisines") private CuisineObject best_cuisines = new CuisineObject();
    //todo missing
    @SerializedName("best_music_genders") private MusicGenderObject best_music_genders = new MusicGenderObject();
    //ok
    @SerializedName("prefer") private Object prefer = new Object();
    //ok
    @SerializedName("bio") private String bio = "";
    //todo missing
    @SerializedName("login_type") private int login_type = 0;
    //ok
    @SerializedName("user_type") private String user_type = "";
    //not used
    @SerializedName("access_token") private String access_token = "";
    //todo Missing
    @SerializedName("profile_image") private String profile_image = "";
    //todo Missing
    @SerializedName("cover_image") private String cover_image = "";
    //ok
    @SerializedName("mobile_number") private String mobile_number = "";
    //todo New
    @SerializedName("image") private String image = "";
    //todo New
    @SerializedName("thumb_image") private String thumb_image = "";
    //todo New
    @SerializedName("age") private String age = "";
    //todo New
    @SerializedName("country_name") private String country_name = "";
    //todo New
    @SerializedName("first_best_cuisines") private String first_best_cuisines = "";
    //todo New
    @SerializedName("second_best_cuisines") private String second_best_cuisines = "";
    //todo New
    @SerializedName("third_best_cuisines") private String third_best_cuisines = "";
    //todo New
    @SerializedName("first_best_music_genders") private String first_best_music_genders = "";
    //todo New
    @SerializedName("second_best_music_genders") private String second_best_music_genders = "";
    //todo New
    @SerializedName("third_best_music_genders") private String third_best_music_genders = "";
    //todo New
    @SerializedName("website") private String website = "";
    //todo New
    @SerializedName("atmosphere") private String atmosphere = "";
    //todo New
    @SerializedName("first_music_genre") private String first_music_genre = "";
    //todo New
    @SerializedName("second_music_genre") private String second_music_genre = "";
    //todo New
    @SerializedName("third_music_genre") private String third_music_genre = "";
    //todo New
    @SerializedName("cuisine") private String cuisine = "";
    //todo New
    @SerializedName("kids_area") private String kids_area = "";
    //todo New
    @SerializedName("handicapped_entrance") private String handicapped_entrance = "";
    //todo New
    @SerializedName("takeaway") private String takeaway = "";
    //todo New
    @SerializedName("rest_rooms") private String rest_rooms = "";
    //todo New
    @SerializedName("price_range") private String price_range = "";
    //todo New
    @SerializedName("better_for") private int better_for = 0;
    //todo New
    @SerializedName("signature") private String signature = "";
    //todo New
    @SerializedName("account_status") private String account_status = "";
    @SerializedName("seating") private List<Integer> seating = new ArrayList<>();
    @SerializedName("parking") private int parking = 0;
    @SerializedName("going_on") private List<String> going_on = new ArrayList<>();
    @SerializedName("categories") private List<CategoryObject> categories = new ArrayList<>();
    @SerializedName("working_hours") private List<WorkingHourObject> working_hours = new ArrayList<>();
    @SerializedName("account_verified_at") private String account_verified_at = "";


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    //public int getCountry_id() {
    //    return country_id;
    //}

    //public void setCountry_id(int country_id) {
    //    this.country_id = country_id;
    //}

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public CuisineObject getBest_cuisines() {
        return best_cuisines;
    }

    public void setBest_cuisines(CuisineObject best_cuisines) {
        this.best_cuisines = best_cuisines;
    }

    public MusicGenderObject getBest_music_genders() {
        return best_music_genders;
    }

    public void setBest_music_genders(MusicGenderObject best_music_genders) {
        this.best_music_genders = best_music_genders;
    }

    public Object getPrefer() {
        return prefer;
    }

    public void setPrefer(Object prefer) {
        this.prefer = prefer;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public int getLogin_type() {
        return login_type;
    }

    public void setLogin_type(int login_type) {
        this.login_type = login_type;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getFirst_best_cuisines() {
        return first_best_cuisines;
    }

    public void setFirst_best_cuisines(String first_best_cuisines) {
        this.first_best_cuisines = first_best_cuisines;
    }

    public String getSecond_best_cuisines() {
        return second_best_cuisines;
    }

    public void setSecond_best_cuisines(String second_best_cuisines) {
        this.second_best_cuisines = second_best_cuisines;
    }

    public String getThird_best_cuisines() {
        return third_best_cuisines;
    }

    public void setThird_best_cuisines(String third_best_cuisines) {
        this.third_best_cuisines = third_best_cuisines;
    }

    public String getFirst_best_music_genders() {
        return first_best_music_genders;
    }

    public void setFirst_best_music_genders(String first_best_music_genders) {
        this.first_best_music_genders = first_best_music_genders;
    }

    public String getSecond_best_music_genders() {
        return second_best_music_genders;
    }

    public void setSecond_best_music_genders(String second_best_music_genders) {
        this.second_best_music_genders = second_best_music_genders;
    }

    public String getThird_best_music_genders() {
        return third_best_music_genders;
    }

    public void setThird_best_music_genders(String third_best_music_genders) {
        this.third_best_music_genders = third_best_music_genders;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAtmosphere() {
        return atmosphere;
    }

    public void setAtmosphere(String atmosphere) {
        this.atmosphere = atmosphere;
    }

    public String getFirst_music_genre() {
        return first_music_genre;
    }

    public void setFirst_music_genre(String first_music_genre) {
        this.first_music_genre = first_music_genre;
    }

    public String getSecond_music_genre() {
        return second_music_genre;
    }

    public void setSecond_music_genre(String second_music_genre) {
        this.second_music_genre = second_music_genre;
    }

    public String getThird_music_genre() {
        return third_music_genre;
    }

    public void setThird_music_genre(String third_music_genre) {
        this.third_music_genre = third_music_genre;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public String getKids_area() {
        return kids_area;
    }

    public void setKids_area(String kids_area) {
        this.kids_area = kids_area;
    }

    public String getHandicapped_entrance() {
        return handicapped_entrance;
    }

    public void setHandicapped_entrance(String handicapped_entrance) {
        this.handicapped_entrance = handicapped_entrance;
    }

    public String getTakeaway() {
        return takeaway;
    }

    public void setTakeaway(String takeaway) {
        this.takeaway = takeaway;
    }

    public String getRest_rooms() {
        return rest_rooms;
    }

    public void setRest_rooms(String rest_rooms) {
        this.rest_rooms = rest_rooms;
    }

    public String getPrice_range() {
        return price_range;
    }

    public void setPrice_range(String price_range) {
        this.price_range = price_range;
    }

    public int getBetter_for() {
        return better_for;
    }

    public void setBetter_for(int better_for) {
        this.better_for = better_for;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }

    public static String getUserType() {
        return USER_TYPE;
    }

    public static String getBusinessType() {
        return BUSINESS_TYPE;
    }

    public List<Integer> getSeating() {
        return seating;
    }

    public void setSeating(List<Integer> seating) {
        this.seating = seating;
    }

    public int getParking() {
        return parking;
    }

    public void setParking(int parking) {
        this.parking = parking;
    }

    public List<String> getGoing_on() {
        return going_on;
    }

    public void setGoing_on(List<String> going_on) {
        this.going_on = going_on;
    }

    public List<CategoryObject> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryObject> categories) {
        this.categories = categories;
    }

    public List<WorkingHourObject> getWorking_hours() {
        return working_hours;
    }

    public void setWorking_hours(List<WorkingHourObject> working_hours) {
        this.working_hours = working_hours;
    }

    public String getAccount_verified_at() {
        return account_verified_at;
    }

    public void setAccount_verified_at(String account_verified_at) {
        this.account_verified_at = account_verified_at;
    }
}
