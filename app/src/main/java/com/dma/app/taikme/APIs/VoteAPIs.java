package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface VoteAPIs {

    @POST("votes/{type}/{id}/{value}")
    Call<BaseResponse> vote(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("type") String type,
            @Path("id") String id,
            @Path("value") String value
    );
}
