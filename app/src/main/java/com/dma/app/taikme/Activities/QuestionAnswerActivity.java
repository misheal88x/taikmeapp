package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;

public class QuestionAnswerActivity extends BaseActivity {

    private RelativeLayout toolbar,btn_back;
    private TextView tv_title;
    private String answer = "";
    private Intent myIntent;
    private WebView wv_answer;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_question_answer);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(myIntent.getStringExtra("question"));
        answer = myIntent.getStringExtra("answer");
        init_webView();

    }

    @Override
    public void init_views() {
        //RelativeLayout
        toolbar = findViewById(R.id.question_answer_toolbar);
        btn_back = findViewById(R.id.toolbar_back_btn);
        //TextView
        tv_title = findViewById(R.id.question_answer_title);
        //Intent
        myIntent = getIntent();
        //Web view
        wv_answer = findViewById(R.id.question_answer_answer);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(QuestionAnswerActivity.this);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_webView() {
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        Spanned spn= Html.fromHtml(answer);
        wv_answer.loadDataWithBaseURL("", "<html dir=\"rtl\" lang=\"\"><body>" +spn.toString()+"</body></html>", mimeType, encoding, "");
    }
}
