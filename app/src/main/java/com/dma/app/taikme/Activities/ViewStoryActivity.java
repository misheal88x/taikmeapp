package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.Interfaces.ILoadImage;
import com.dma.app.taikme.Models.HomeModels.HomeStoryObject;
import com.dma.app.taikme.Models.ImageObject;
import com.dma.app.taikme.Models.StoryObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.OnSwipeTouchListener;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class ViewStoryActivity extends BaseActivity implements ExoPlayer.EventListener{

    private RelativeLayout life_1,life_2,life_3,life_4,life_5,life_6,life_7,life_8,life_9,life_10;
    private ImageView img_image;
    private PlayerView v_video;
    private LinearLayout navigation_layout;
    private SimpleExoPlayer simpleExoPlayer;
    private ProgressBar load;
    private CircleImageView img_profile;
    private TextView tv_name,tv_time;
    private int num_stories = 0;//num of stories for the current user
    private int current_story = 0;//current user, in our case its current story
    private int totalNumOfStories = 0;//Number of users, in our case each user can has one story only
    private RelativeLayout btn_previous,btn_next;
    private List<HomeStoryObject> listOfStories;
    private int currentvolume = 0;

    private static Handler handler;
    private Runnable mStatusChecker;
    private boolean handler_started = false;
    private int counter = 0;
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_view_story);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        setupExoPlayer();
        setUpListOfStories();
        //todo the current story order,total num of stories will be came from the previous activity
        totalNumOfStories = getIntent().getIntExtra("total_num",1);
        current_story = getIntent().getIntExtra("position",0);
        num_stories = 1;
        setUpHandler();
        viewStory(current_story,num_stories);
    }

    @Override
    public void init_views() {
        //RelativeLayout
        life_1 = findViewById(R.id.life_1);
        life_2 = findViewById(R.id.life_2);
        life_3 = findViewById(R.id.life_3);
        life_4 = findViewById(R.id.life_4);
        life_5 = findViewById(R.id.life_5);
        life_6 = findViewById(R.id.life_6);
        life_7 = findViewById(R.id.life_7);
        life_8 = findViewById(R.id.life_8);
        life_9 = findViewById(R.id.life_9);
        life_10 = findViewById(R.id.life_10);
        btn_previous = findViewById(R.id.view_story_previous);
        btn_next = findViewById(R.id.view_story_next);
        navigation_layout = findViewById(R.id.view_story_navigation);
        //ImageView
        img_image = findViewById(R.id.view_story_image);
        //VideoView
        v_video = findViewById(R.id.view_story_video);
        //ProgressBar
        load = findViewById(R.id.view_story_indicator);
        //CircleImageView
        img_profile = findViewById(R.id.view_story_profile);
        //TextView
        tv_name = findViewById(R.id.view_story_name);
        tv_time = findViewById(R.id.view_story_time);
    }

    @Override
    public void init_events() {
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current_story++;
                counter = 0;
                if (current_story==totalNumOfStories){
                    finish();
                }else {
                    viewStory(current_story,num_stories);
                }
            }
        });
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current_story--;
                counter = 0;
                if (current_story == -1){
                    finish();
                }else {
                    viewStory(current_story,num_stories);
                }
            }
        });
        navigation_layout.setOnTouchListener(new OnSwipeTouchListener(ViewStoryActivity.this) {
            public void onSwipeTop() {

            }
            public void onSwipeRight() {
                current_story--;
                if (current_story == -1){
                    finish();
                }else {
                    viewStory(current_story,num_stories);
                }
            }
            public void onSwipeLeft() {
                current_story++;
                if (current_story==totalNumOfStories){
                    finish();
                }else {
                    viewStory(current_story,num_stories);
                }
            }
            public void onSwipeBottom() {

            }

        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (simpleExoPlayer!=null){
            simpleExoPlayer.release();
        }
        if (handler_started) {
            stopRepeatingTask();
        }
    }

    private void setupExoPlayer(){
        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this);
        v_video.setPlayer(simpleExoPlayer);
    }

    private void setNumOfLifes(int num){
        switch (num){
            case 1 : {
                life_1.setVisibility(View.VISIBLE);
                life_2.setVisibility(View.GONE);
                life_3.setVisibility(View.GONE);
                life_4.setVisibility(View.GONE);
                life_5.setVisibility(View.GONE);
                life_6.setVisibility(View.GONE);
                life_7.setVisibility(View.GONE);
                life_8.setVisibility(View.GONE);
                life_9.setVisibility(View.GONE);
                life_10.setVisibility(View.GONE);
            }break;
            case 2 : {
                life_1.setVisibility(View.VISIBLE);
                life_2.setVisibility(View.VISIBLE);
                life_3.setVisibility(View.GONE);
                life_4.setVisibility(View.GONE);
                life_5.setVisibility(View.GONE);
                life_6.setVisibility(View.GONE);
                life_7.setVisibility(View.GONE);
                life_8.setVisibility(View.GONE);
                life_9.setVisibility(View.GONE);
                life_10.setVisibility(View.GONE);
            }break;
            case 3 : {
                life_1.setVisibility(View.VISIBLE);
                life_2.setVisibility(View.VISIBLE);
                life_3.setVisibility(View.VISIBLE);
                life_4.setVisibility(View.GONE);
                life_5.setVisibility(View.GONE);
                life_6.setVisibility(View.GONE);
                life_7.setVisibility(View.GONE);
                life_8.setVisibility(View.GONE);
                life_9.setVisibility(View.GONE);
                life_10.setVisibility(View.GONE);
            }break;
            case 4 : {
                life_1.setVisibility(View.VISIBLE);
                life_2.setVisibility(View.VISIBLE);
                life_3.setVisibility(View.VISIBLE);
                life_4.setVisibility(View.VISIBLE);
                life_5.setVisibility(View.GONE);
                life_6.setVisibility(View.GONE);
                life_7.setVisibility(View.GONE);
                life_8.setVisibility(View.GONE);
                life_9.setVisibility(View.GONE);
                life_10.setVisibility(View.GONE);
            }break;
            case 5 : {
                life_1.setVisibility(View.VISIBLE);
                life_2.setVisibility(View.VISIBLE);
                life_3.setVisibility(View.VISIBLE);
                life_4.setVisibility(View.VISIBLE);
                life_5.setVisibility(View.VISIBLE);
                life_6.setVisibility(View.GONE);
                life_7.setVisibility(View.GONE);
                life_8.setVisibility(View.GONE);
                life_9.setVisibility(View.GONE);
                life_10.setVisibility(View.GONE);
            }break;
            case 6 : {
                life_1.setVisibility(View.VISIBLE);
                life_2.setVisibility(View.VISIBLE);
                life_3.setVisibility(View.VISIBLE);
                life_4.setVisibility(View.VISIBLE);
                life_5.setVisibility(View.VISIBLE);
                life_6.setVisibility(View.VISIBLE);
                life_7.setVisibility(View.GONE);
                life_8.setVisibility(View.GONE);
                life_9.setVisibility(View.GONE);
                life_10.setVisibility(View.GONE);
            }break;
            case 7 : {
                life_1.setVisibility(View.VISIBLE);
                life_2.setVisibility(View.VISIBLE);
                life_3.setVisibility(View.VISIBLE);
                life_4.setVisibility(View.VISIBLE);
                life_5.setVisibility(View.VISIBLE);
                life_6.setVisibility(View.VISIBLE);
                life_7.setVisibility(View.VISIBLE);
                life_8.setVisibility(View.GONE);
                life_9.setVisibility(View.GONE);
                life_10.setVisibility(View.GONE);
            }break;
            case 8 : {
                life_1.setVisibility(View.VISIBLE);
                life_2.setVisibility(View.VISIBLE);
                life_3.setVisibility(View.VISIBLE);
                life_4.setVisibility(View.VISIBLE);
                life_5.setVisibility(View.VISIBLE);
                life_6.setVisibility(View.VISIBLE);
                life_7.setVisibility(View.VISIBLE);
                life_8.setVisibility(View.VISIBLE);
                life_9.setVisibility(View.GONE);
                life_10.setVisibility(View.GONE);
            }break;
            case 9 : {
                life_1.setVisibility(View.VISIBLE);
                life_2.setVisibility(View.VISIBLE);
                life_3.setVisibility(View.VISIBLE);
                life_4.setVisibility(View.VISIBLE);
                life_5.setVisibility(View.VISIBLE);
                life_6.setVisibility(View.VISIBLE);
                life_7.setVisibility(View.VISIBLE);
                life_8.setVisibility(View.VISIBLE);
                life_9.setVisibility(View.VISIBLE);
                life_10.setVisibility(View.GONE);
            }break;
            case 10 : {
                life_1.setVisibility(View.VISIBLE);
                life_2.setVisibility(View.VISIBLE);
                life_3.setVisibility(View.VISIBLE);
                life_4.setVisibility(View.VISIBLE);
                life_5.setVisibility(View.VISIBLE);
                life_6.setVisibility(View.VISIBLE);
                life_7.setVisibility(View.VISIBLE);
                life_8.setVisibility(View.VISIBLE);
                life_9.setVisibility(View.VISIBLE);
                life_10.setVisibility(View.VISIBLE);
            }break;
        }
    }

    private void setActiveLife(int num){
        switch (num){
            case 1 : {
                life_1.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_white));
                life_2.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_3.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_4.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_5.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_6.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_7.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_8.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_9.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_10.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
            }break;
            case 2 : {
                life_1.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_2.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_white));
                life_3.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_4.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_5.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_6.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_7.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_8.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_9.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_10.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
            }break;
            case 3 : {
                life_1.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_2.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_3.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_white));
                life_4.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_5.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_6.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_7.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_8.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_9.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_10.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
            }break;
            case 4 : {
                life_1.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_2.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_3.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_4.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_white));
                life_5.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_6.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_7.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_8.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_9.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_10.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
            }break;
            case 5 : {
                life_1.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_2.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_3.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_4.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_5.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_white));
                life_6.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_7.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_8.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_9.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_10.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
            }break;
            case 6 : {
                life_1.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_2.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_3.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_4.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_5.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_6.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_white));
                life_7.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_8.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_9.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_10.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
            }break;
            case 7 : {
                life_1.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_2.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_3.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_4.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_5.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_6.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_7.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_white));
                life_8.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_9.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_10.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
            }break;
            case 8 : {
                life_1.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_2.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_3.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_4.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_5.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_6.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_7.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_8.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_white));
                life_9.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_10.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
            }break;
            case 9 : {
                life_1.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_2.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_3.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_4.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_5.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_6.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_7.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_8.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_9.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_white));
                life_10.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
            }break;
            case 10 : {
                life_1.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_2.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_3.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_4.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_5.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_6.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_7.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_8.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_9.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_grey));
                life_10.setBackground(getResources().getDrawable(R.drawable.rounded_story_life_white));
            }break;

        }
    }

    private void viewImageStory(String url){
        if (simpleExoPlayer!=null){
            simpleExoPlayer.release();
            v_video.setVisibility(View.GONE);
        }
        img_image.setImageResource(0);
        img_image.setVisibility(View.VISIBLE);
        load.setVisibility(View.VISIBLE);
        BaseFunctions.setGlideImageWithEvents(ViewStoryActivity.this,
                img_image,
                url,
                new ILoadImage() {
                    @Override
                    public void onLoaded() {
                        load.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailed() {
                        load.setVisibility(View.GONE);
                    }
                });
    }

    private void viewVideoStory(String url){
        img_image.setVisibility(View.GONE);
        v_video.setVisibility(View.VISIBLE);
        simpleExoPlayer.release();
        setupExoPlayer();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this,"appname"));
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).
                createMediaSource(Uri.parse(url));
        simpleExoPlayer.prepare(videoSource);
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.addListener(this);
    }

    private void setProfileInfo(String url,String name,String time){
        try {
            BaseFunctions.setGlideImage(ViewStoryActivity.this,img_profile,url);
            tv_name.setText(name);
            tv_time.setText(BaseFunctions.processDate(ViewStoryActivity.this,time));
        }catch (Exception e){
            finish();
        }
    }

    private void setUpListOfStories(){
        HomeStoryObject[] stories = new Gson().fromJson(getIntent().getStringExtra("stories"),HomeStoryObject[].class);
        listOfStories = new ArrayList<>();
        if (stories.length>0){
            for (HomeStoryObject so : stories){
                listOfStories.add(so);
            }
        }

    }

    private void viewStory(int current,int num_stories){

        setNumOfLifes(num_stories);
        setActiveLife(num_stories);
        HomeStoryObject so = listOfStories.get(current);
        if (so.getContent_type().equals("2")){
            try {
                //stopRepeatingTask();
                //handler_started = false;
                viewImageStory(so.getContent());
                if (!handler_started) {
                    startRepeatingTask();
                }
            }catch (Exception e){}

        }else {
            try {
                stopRepeatingTask();
                viewVideoStory(so.getContent().toString());
            }catch (Exception e){}

        }
        setProfileInfo(so.getUser_profile_thumb_image(),so.getUser_name(),so.getCreated_at());
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState){
            case ExoPlayer.STATE_READY:{
                Log.i("video_event", "onPlayerStateChanged: "+simpleExoPlayer.getDuration());
                //load.smoothToHide();
            }break;
            case ExoPlayer.STATE_BUFFERING:{
                //Log.i("video_event", "onPlayerStateChanged: "+"buffering");
                //load.smoothToShow();
            }break;
            case ExoPlayer.STATE_ENDED:{
                current_story++;
                if (current_story==totalNumOfStories){
                    finish();
                }else {
                    viewStory(current_story,num_stories);
                }
                //Log.i("video_event", "onPlayerStateChanged: "+"ended");
                //load.smoothToHide();
            }break;
        }
    }

    void setUpHandler(){
        handler = new Handler();
        mStatusChecker = new Runnable() {
            @Override
            public void run() {
                try {
                    Log.i("jdfnjdnf", "run: "+"handler fired");
                    counter++;
                    if (counter%5==0){
                        current_story++;
                        if (current_story==totalNumOfStories){
                             finish();
                        }else {
                             viewStory(current_story,num_stories);
                        }
                    }
                } finally {
                    handler.postDelayed(mStatusChecker, 1000);
                }
            }
        };
    }

    void startRepeatingTask() {
        counter = 0;
        mStatusChecker.run();
        handler_started = true;
    }

    void stopRepeatingTask() {
        counter = 0;
        try {
            if (handler!=null) {
                handler.removeCallbacks(mStatusChecker);
            }
        }catch (Exception e){

        }
    }

    @Override
    public void onPause() {
        stopRepeatingTask();
        super.onPause();
    }
}
