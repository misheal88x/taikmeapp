package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.dma.app.taikme.Models.CategoryObject;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MultiSelectCategoriesAdapter extends  RecyclerView.Adapter<MultiSelectCategoriesAdapter.ViewHolder> {
    private Context context;
    private List<CategoryObject> source,selected,checked;

    public MultiSelectCategoriesAdapter(Context context,List<CategoryObject> source,List<CategoryObject> selected) {
        this.context = context;
        this.source = source;
        this.selected = selected;
        this.checked = new ArrayList<>();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CheckBox chk_check;
        public ViewHolder(View view) {
            super(view);
            chk_check = view.findViewById(R.id.item_check);
        }
    }

    @Override
    public int getItemCount() {
        return source.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_check, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final CategoryObject co = source.get(position);
        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();
        if (lan.equals("en")){
            if (co.getEn_name()!=null){
                holder.chk_check.setText(co.getEn_name());
            }else {
                if (co.getAr_name()!=null){
                    holder.chk_check.setText(co.getAr_name());
                }
            }
        }else if (lan.equals("ar")){
            if (co.getAr_name()!=null){
                holder.chk_check.setText(co.getAr_name());
            }else {
                if (co.getEn_name()!=null){
                    holder.chk_check.setText(co.getEn_name());
                }
            }
        }
        if (selected.size()>0){
            for (CategoryObject coo : selected){
                if (coo.getId() == co.getId()){
                    holder.chk_check.setChecked(true);
                    checked.add(co);
                    return;
                }
            }
        }
        holder.chk_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checked.add(co);
                }else {
                    try {
                        checked.remove(checked.indexOf(co));
                    }catch (Exception e){}
                }
            }
        });
    }

    public List<CategoryObject> getCheckedItems(){
        return checked;
    }
}
