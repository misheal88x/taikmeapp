package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Models.UserObject;
import com.dma.app.taikme.Models.VisitedPlaceObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class SearchTagAdapter extends RecyclerView.Adapter<SearchTagAdapter.ViewHolder> {
    private Context context;
    private List<VisitedPlaceObject> list;
    private IMove iMove;

    public SearchTagAdapter(Context context,List<VisitedPlaceObject> list,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView profile_image;
        private TextView name;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            profile_image = view.findViewById(R.id.profile_image);
            name = view.findViewById(R.id.name);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tag_search, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final VisitedPlaceObject place = list.get(position);

        BaseFunctions.setGlideImage(context,holder.profile_image,place.getThumb_image());
        holder.name.setText(place.getName());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
    }

}
