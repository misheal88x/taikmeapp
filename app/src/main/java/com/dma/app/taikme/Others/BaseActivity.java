package com.dma.app.taikme.Others;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.ViewGroup;

import com.dma.app.taikme.Fragments.HomeFragment;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;

//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Misheal on 11/12/2019.
 */

public abstract class BaseActivity extends AppCompatActivity {
    protected ViewGroup fragment_place;
    public BaseFragment current_fragment;
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private App app;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences("Main",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        app = App.getInstance();
        if (SharedPrefManager.getInstance(BaseActivity.this).getSettings()!=null){
            LocaleHelper.setLocale(this, SharedPrefManager.getInstance(BaseActivity.this).getSettings().getView_language());
        }else {
            LocaleHelper.setLocale(this, "en");
        }
        set_layout();
        init_views();
        set_fragment_place();
        init_events();
        init_activity(savedInstanceState);
        //Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    public abstract void set_layout();

    public abstract void init_activity(Bundle savedInstanceState);

    public abstract void init_views();

    public abstract void init_events();

    public abstract void set_fragment_place();


   /* public void send_data(DataMessage message){
        message.get_receiver().onReceive(message.get_extra());
    }

    public void send_data(ObjectMessage message){
        message.get_receiver().onReceive(message.get_object());
    }*/

    //To open specific fragment
    public void open_fragment(final BaseFragment fragment) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (current_fragment != fragment) {
                    try {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
                        transaction.replace(fragment_place.getId(), fragment);
                        transaction.commit();
                        fragmentManager.executePendingTransactions();
                        current_fragment = fragment;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        };
        runOnUiThread(runnable);
    }

    @Override
    public void onBackPressed() {
        if (current_fragment != null && current_fragment.get_previous_fragment() != null) {
            open_fragment(current_fragment.get_previous_fragment());
        }else {
            super.onBackPressed();
        }
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
