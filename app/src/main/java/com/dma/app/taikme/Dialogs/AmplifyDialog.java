package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.dma.app.taikme.APIsClass.AmplifyAPIsClass;
import com.dma.app.taikme.APIsClass.BusinessLocationsAPIsClass;
import com.dma.app.taikme.APIsClass.CustomAudienceAPIsClass;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BusinessLocationObject;
import com.dma.app.taikme.Models.BusinessLocatonNewObject;
import com.dma.app.taikme.Models.CustomAudienceObject;
import com.dma.app.taikme.Models.CustomAudiencesResponse;
import com.dma.app.taikme.Models.LebanonCityObject;
import com.dma.app.taikme.Models.LocationObject;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Models.StartupObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class AmplifyDialog extends AlertDialog {

    private Context context;
    private EditText edt_name;
    private Spinner budget_spinner,locations_spinner,audience_spinner,followers_spinner,gender_spinner;
    private CustomSpinnerAdapter budget_adapter,locations_adapter,audience_adapter,followers_adapter,gender_adapter;
    private List<String> budget_list_string,locations_list_string,audience_list_string,followers_list_string,gender_list_string;
    private List<StartupItemObject> followers_list,gender_list;
    private List<BusinessLocatonNewObject> locations_list;
    private List<CustomAudienceObject> audiences_list;
    private CrystalRangeSeekbar rb_range;
    private TextView tv_min,tv_max;
    private TextView btn_amplify;
    private AVLoadingIndicatorView loading;
    private RelativeLayout root;


    private int selected_budget = -1;
    private int selected_locations = -1;
    private int selected_audience = -1;
    private int selected_followers = -1;
    private String selected_gender = "";
    private int selected_low_age = -1;
    private int selected_high_age = -1;

    private String id = "";
    private String type = "";
    public AmplifyDialog(@NonNull Context context,String id,String type) {
        super(context);
        this.context = context;
        this.id = id;
        this.type = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_amplify);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //EditText
        edt_name = findViewById(R.id.dialog_amplify_name);
        //CrystalRangeBar
        rb_range = findViewById(R.id.dialog_amplify_range);
        //TextView
        tv_min = findViewById(R.id.dialog_amplify_min_value);
        tv_max = findViewById(R.id.dialog_amplify_max_value);
        btn_amplify = findViewById(R.id.dialog_amplify_btn);
        //Spinner
        budget_spinner = findViewById(R.id.dialog_amplify_budget_spinner);
        locations_spinner = findViewById(R.id.dialog_amplify_locations_spinner);
        audience_spinner = findViewById(R.id.dialog_amplify_audience_spinner);
        followers_spinner = findViewById(R.id.dialog_amplify_followers_spinner);
        gender_spinner = findViewById(R.id.dialog_amplify_gender_spinner);
        //Loading Indicator
        loading = findViewById(R.id.loading);
        //RelativeLayout
        root = findViewById(R.id.layout);
    }

    private void init_events(){
        budget_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    switch (p){
                        case 1:{selected_budget = 3;}break;
                        case 2:{selected_budget = 5;}break;
                        case 3:{selected_budget = 8;}break;
                    }
                }else {
                    selected_budget = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        followers_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_followers = Integer.valueOf(followers_list.get(p).getId());
                }else {
                    selected_followers = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        locations_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_locations = locations_list.get(p).getId();
                }else {
                    selected_locations = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        audience_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_audience = audiences_list.get(p).getId();
                }else {
                    selected_audience = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    switch (p){
                        case 1:{
                            selected_gender = "m";
                        }break;
                        case 2:{
                            selected_gender = "f";
                        }break;
                        case 3:{
                            selected_gender = "b";
                        }break;
                        case 4:{
                            selected_gender = "a";
                        }break;
                    }
                }else {
                    selected_gender = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        rb_range.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tv_min.setText(String.valueOf(minValue));
                selected_low_age = Integer.valueOf(String.valueOf(minValue));
                selected_high_age = Integer.valueOf(String.valueOf(maxValue));
                tv_max.setText(String.valueOf(maxValue));
            }
        });
        btn_amplify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_name.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.amplify_dialog_no_name), Toast.LENGTH_SHORT).show();
                    edt_name.requestFocus();
                    return;
                }
                if (selected_budget == -1){
                    Toast.makeText(context, context.getResources().getString(R.string.amplify_dialog_no_budget), Toast.LENGTH_SHORT).show();
                    budget_spinner.requestFocus();
                    return;
                }
                if (selected_locations == -1){
                    Toast.makeText(context, context.getResources().getString(R.string.amplify_dialog_no_locations), Toast.LENGTH_SHORT).show();
                    locations_spinner.requestFocus();
                    return;
                }
                if (selected_audience == -1){
                    Toast.makeText(context, context.getResources().getString(R.string.amplify_dialog_no_audience), Toast.LENGTH_SHORT).show();
                    audience_spinner.requestFocus();
                    return;
                }
                if (selected_followers == -1){
                    Toast.makeText(context, context.getResources().getString(R.string.amplify_dialog_no_followers), Toast.LENGTH_SHORT).show();
                    followers_spinner.requestFocus();
                    return;
                }
                if (selected_gender.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.amplify_dialog_no_gender), Toast.LENGTH_SHORT).show();
                    gender_spinner.requestFocus();
                    return;
                }
                if (selected_low_age == -1){
                    Toast.makeText(context, context.getResources().getString(R.string.amplify_dialog_no_min), Toast.LENGTH_SHORT).show();
                    rb_range.requestFocus();
                    return;
                }
                if (selected_high_age == -1){
                    Toast.makeText(context, context.getResources().getString(R.string.amplify_dialog_no_max), Toast.LENGTH_SHORT).show();
                    rb_range.requestFocus();
                    return;
                }
                callAmplifyAPI();
            }
        });
    }

    private void init_dialog(){
        init_spinners();
        callAudiencesAPI(2);
    }

    private void init_spinners(){
        StartupObject so = SharedPrefManager.getInstance(context).getStartUp();
        String language = SharedPrefManager.getInstance(context).getSettings().getView_language();
        budget_list_string = new ArrayList<>();
        locations_list_string = new ArrayList<>();
        locations_list = new ArrayList<>();
        audience_list_string = new ArrayList<>();
        audiences_list = new ArrayList<>();
        followers_list_string = new ArrayList<>();
        followers_list = new ArrayList<>();
        gender_list_string = new ArrayList<>();
        gender_list = new ArrayList<>();

        budget_list_string.add(context.getResources().getString(R.string.amplify_dialog_budget));
        budget_list_string.add("3$");
        budget_list_string.add("5$");
        budget_list_string.add("8$");

        locations_list_string.add(context.getResources().getString(R.string.amplify_dialog_locations));
        locations_list.add(new BusinessLocatonNewObject());

        audience_list_string.add(context.getResources().getString(R.string.amplify_dialog_audience));
        audiences_list.add(new CustomAudienceObject());

        followers_list_string.add(context.getResources().getString(R.string.amplify_dialog_followers));
        if (so.getFollow_you()!=null && so.getFollow_you().size()>0){
            for (StartupItemObject sio : so.getFollow_you()){
                followers_list.add(sio);
                followers_list_string.add(sio.getName());
            }
        }

        gender_list_string.add(context.getResources().getString(R.string.amplify_dialog_gender));
        if (so.getGenders()!=null && so.getGenders().size()>0){
            for (StartupItemObject sio : so.getGenders()){
                gender_list.add(sio);
                gender_list_string.add(sio.getName());
            }
        }

        BaseFunctions.init_spinner(context,budget_spinner,budget_adapter,budget_list_string);
        locations_adapter = new CustomSpinnerAdapter(context,R.layout.spinner_item,locations_list_string);
        locations_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locations_spinner.setAdapter(locations_adapter);
        //BaseFunctions.init_spinner(context,audience_spinner,audience_adapter,audience_list_string);
        BaseFunctions.init_spinner(context,followers_spinner,followers_adapter,followers_list_string);
        BaseFunctions.init_spinner(context,gender_spinner,gender_adapter,gender_list_string);
        audience_adapter = new CustomSpinnerAdapter(context,R.layout.spinner_item,audience_list_string);
        audience_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        audience_spinner.setAdapter(audience_adapter);
    }

    private void callAudiencesAPI(final int tries){
        btn_amplify.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        CustomAudienceAPIsClass.getAll(
                context,
                BaseFunctions.getDeviceId(context),
                1,
                1,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        if (tries<2)
                            callAudiencesAPI(tries+1);
                        else {
                            loading.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onResponse(Object json) {
                        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();
                        loading.setVisibility(View.GONE);
                        if (json!=null){
                            btn_amplify.setVisibility(View.VISIBLE);
                            String j = new Gson().toJson(json);
                            CustomAudiencesResponse success = new Gson().fromJson(j,CustomAudiencesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    for (CustomAudienceObject cao : success.getData()){
                                        audiences_list.add(cao);
                                        audience_list_string.add(lan.equals("ar")?cao.getAr_name():cao.getEn_name());
                                        audience_adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                            callLocationAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        if (tries<2)
                            callAudiencesAPI(tries+1);
                        else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                            loading.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void callAmplifyAPI(){
        AmplifyAPIsClass.create(
                context,
                BaseFunctions.getDeviceId(context),
                edt_name.getText().toString(),
                edt_name.getText().toString(),
                selected_budget,
                selected_locations,
                selected_audience,
                selected_gender,
                selected_low_age,
                selected_high_age,
                String.valueOf(selected_followers),
                id,type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Toast.makeText(context, context.getResources().getString(R.string.amplify_dialog_success), Toast.LENGTH_SHORT).show();
                            BaseFunctions.playMusic(context,R.raw.added);
                            cancel();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callLocationAPI(){
        btn_amplify.setVisibility(View.GONE);
        loading.smoothToShow();
        BusinessLocationsAPIsClass.getAll(
                context,
                BaseFunctions.getDeviceId(context),
                "-1",
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        btn_amplify.setVisibility(View.VISIBLE);
                        loading.smoothToHide();
                        String j = new Gson().toJson(json);
                        BusinessLocatonNewObject[] success = new Gson().fromJson(j,BusinessLocatonNewObject[].class);
                        if (success.length>0){
                            for (BusinessLocatonNewObject o : success){
                                locations_list.add(o);
                                locations_list_string.add(o.getLocation_address());
                                locations_adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        btn_amplify.setVisibility(View.VISIBLE);
                        loading.smoothToHide();
                        Snackbar.make(root, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callLocationAPI();
                                    }
                                }).setActionTextColor(context.getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }
}


