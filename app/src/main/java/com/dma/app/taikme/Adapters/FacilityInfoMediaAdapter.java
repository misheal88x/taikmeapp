package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.CommentsAPIsClass;
import com.dma.app.taikme.APIsClass.FollowersAPIsClass;
import com.dma.app.taikme.APIsClass.ShareAPIsClass;
import com.dma.app.taikme.APIsClass.VoteAPIsClass;
import com.dma.app.taikme.Activities.AddCommentActivity;
import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Activities.SeeCommentsActivity;
import com.dma.app.taikme.Activities.SeeReviewsActivity;
import com.dma.app.taikme.Activities.ViewOriginalPostActivity;
import com.dma.app.taikme.Dialogs.AddCommentDialog;
import com.dma.app.taikme.Dialogs.AllCommentsDialog;
import com.dma.app.taikme.Dialogs.AllReviewsDialog;
import com.dma.app.taikme.Dialogs.AmplifyDialog;
import com.dma.app.taikme.Dialogs.SharePostDialog;
import com.dma.app.taikme.Dialogs.ViewHomeVideoDialog;
import com.dma.app.taikme.Dialogs.ViewImageDialog;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.EventObject;
import com.dma.app.taikme.Models.ImageObject;
import com.dma.app.taikme.Models.ProfileMediaObject;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.Profile;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FacilityInfoMediaAdapter  extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<ProfileMediaObject> list;

    private String type;

    public FacilityInfoMediaAdapter(Context context,
                               List<ProfileMediaObject> list,
                               String type) {
        this.context = context;
        this.list = list;
        this.type = type;
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {
        private SimpleDraweeView img_profile_image;
        private TextView tv_username,tv_address,tv_time,tv_desc,tv_see_comments,tv_see_reviews;
        private TextView tv_votes_count;
        private TextView tv_comment_user;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,btn_speaker;
        private SimpleDraweeView img_image;

        public PhotoViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            tv_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_media_time);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            tv_comment_user = itemView.findViewById(R.id.item_media_comment_user);
            tv_see_comments = itemView.findViewById(R.id.item_media_read_comments);
            tv_see_reviews = itemView.findViewById(R.id.item_media_read_reviews);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            img_image = itemView.findViewById(R.id.item_media_image);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);
            btn_speaker = itemView.findViewById(R.id.item_media_speaker);

        }
    }

    public static class VideoViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image;
        private TextView tv_username,tv_address,tv_time,tv_desc,tv_see_comments,tv_see_reviews;
        private TextView tv_votes_count;
        private TextView tv_comment_user;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,thumbnail,play,btn_speaker;
        private FrameLayout media_container;


        public VideoViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            tv_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_media_time);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            tv_comment_user = itemView.findViewById(R.id.item_media_comment_user);
            tv_see_comments = itemView.findViewById(R.id.item_media_read_comments);
            tv_see_reviews = itemView.findViewById(R.id.item_media_read_reviews);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            thumbnail = itemView.findViewById(R.id.item_media_video);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);
            media_container = itemView.findViewById(R.id.media_container);
            play = itemView.findViewById(R.id.play_video);
            btn_speaker = itemView.findViewById(R.id.item_media_speaker);
        }
    }

    class CheckInViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_image;
        private TextView tv_username,tv_address,tv_time,tv_name,tv_rate,tv_caption,tv_see_comments,tv_see_reviews;
        private TextView tv_votes_count;
        private TextView tv_comment_user;
        private SimpleRatingBar rb_rate,rb_rate2;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,img_broadcast;

        public CheckInViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_home_post_is_at_profile_image);
            img_image = itemView.findViewById(R.id.item_home_post_is_at_image);
            tv_username = itemView.findViewById(R.id.item_home_post_is_at_user_name);
            tv_address = itemView.findViewById(R.id.item_home_post_is_at_address);
            tv_time = itemView.findViewById(R.id.item_home_post_is_at_time);
            tv_name = itemView.findViewById(R.id.item_home_post_is_at_name);
            tv_rate = itemView.findViewById(R.id.item_home_post_is_at_rate_value);
            rb_rate = itemView.findViewById(R.id.item_home_post_is_at_rate);
            rb_rate2 = itemView.findViewById(R.id.item_home_post_is_at_rate2);
            tv_caption = itemView.findViewById(R.id.item_home_post_is_at_desc);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            btn_up = itemView.findViewById(R.id.item_home_post_is_at_top_arrow);
            btn_down = itemView.findViewById(R.id.item_home_post_is_at_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_home_post_is_at_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_home_post_is_at_three_dots);
            img_broadcast = itemView.findViewById(R.id.item_home_post_is_at_broadcast);
            tv_comment_user = itemView.findViewById(R.id.item_media_comment_user);
            tv_see_comments = itemView.findViewById(R.id.item_media_read_comments);
            tv_see_reviews = itemView.findViewById(R.id.item_media_read_reviews);
        }
    }

    @Override
    public int getItemViewType(int position) {
        ProfileMediaObject o = list.get(position);
        int result_type = 0;
        int image_post = 0;
        int video_post = 0;
        int check_in_post = 0;
        for (StartupItemObject s : SharedPrefManager.getInstance(context).getStartUp().getPosts_types()){
            switch (s.getName()){
                case "Photo":{
                    image_post = Integer.valueOf(s.getId());
                }break;
                case "Video":{
                    video_post = Integer.valueOf(s.getId());
                }break;
                case "Check_in":{
                    check_in_post = Integer.valueOf(s.getId());
                }break;
            }
        }
        if (o.getPost_type() == image_post){
            result_type = 1;
        }else if (o.getPost_type() == video_post){
            result_type = 2;
        }else if (o.getPost_type() == check_in_post){
            result_type = 3;
        }
        return result_type;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType){
            case 1:{
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_photo, parent, false);
                return new PhotoViewHolder(itemView);
            }
            case 2:{
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_video, parent, false);
                return new VideoViewHolder(itemView);
            }
            case 3:{
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_post_is_at_location, parent, false);
                return new CheckInViewHolder(itemView);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final ProfileMediaObject dpo = list.get(position);

        switch (holder.getItemViewType()) {
            case 1:{
                final PhotoViewHolder photoViewHolder = (PhotoViewHolder) holder;
                //Username
                if (dpo.getUser_name()!=null){
                    photoViewHolder.tv_username.setText(dpo.getUser_name());
                }
                //Profile image
                if (dpo.getUser_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(photoViewHolder.img_profile_image,dpo.getUser_thumb_image());
                    //BaseFunctions.setGlideImage(context,photoViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                photoViewHolder.tv_address.setText(dpo.getUser_city_name()!=null?dpo.getUser_city_name():"");
                //UserRate
                if (dpo.getBusiness_avg_review()!=null){
                    try {
                        photoViewHolder.rb_rate.setRating(Float.valueOf(dpo.getBusiness_avg_review()));
                    }catch (Exception e){}
                }
                //Time
                if (dpo.getCreated_at()!=null){
                    photoViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getCreated_at()));
                }
                //Image
                if (dpo.getContent()!=null){
                    BaseFunctions.setFrescoImage(photoViewHolder.img_image,dpo.getContent());
                    //BaseFunctions.setGlideImage(context,photoViewHolder.img_image,dpo.getReal_object().getContent().toString());
                }
                //Description
                if (dpo.getCaption()!=null){
                    setTags(photoViewHolder.tv_desc,dpo.getCaption());
                }
                //Votes count
                photoViewHolder.tv_votes_count.setText((dpo.getUpvoting_count()-dpo.getDownvote_count())+"");
                //User vote
                if (dpo.getUser_vote()==1){
                    photoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getUser_vote()==-1){
                    photoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    photoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    photoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                if (dpo.getUser_id() != SharedPrefManager.getInstance(context).getUser().getId()){
                    if (dpo.getDeviceSettingsObject()!=null){
                        if (dpo.getDeviceSettingsObject().getPeople_can_comment_on_posts()==0){
                            photoViewHolder.tv_comment_user.setVisibility(View.GONE);
                        }
                        if (dpo.getDeviceSettingsObject().getPeople_can_vote_posts() == 0){
                            photoViewHolder.btn_down.setVisibility(View.GONE);
                            photoViewHolder.btn_up.setVisibility(View.GONE);
                        }
                    }
                }
                photoViewHolder.tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            //AllCommentsDialog dialog = new AllCommentsDialog(context,"post",dpo.getId());
                            //dialog.show();
                            Intent intent = new Intent(context, SeeCommentsActivity.class);
                            intent.putExtra("id",dpo.getId());
                            intent.putExtra("type","post");
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                photoViewHolder.tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            //AllReviewsDialog dialog = new AllReviewsDialog(context,"post",dpo.getId());
                            //dialog.show();
                            Intent intent = new Intent(context, SeeReviewsActivity.class);
                            intent.putExtra("id",dpo.getId());
                            intent.putExtra("type","post");
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getId());
                       // dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                /*
                photoViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (photoViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("post",dpo.getId(),photoViewHolder.tv_comment_user.getText().toString());
                            photoViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });

                 */
                photoViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            try{
                                String j = new Gson().toJson(dpo.getContent());
                                ImageObject s = new Gson().fromJson(j,ImageObject.class);
                                ViewImageDialog dialog = new ViewImageDialog(context,s.getImage());
                                dialog.show();
                            }catch (Exception e){}

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.btn_three_dots.setVisibility(View.GONE);
                if (type.equals("with")) {
                    photoViewHolder.btn_speaker.setVisibility(View.VISIBLE);
                    photoViewHolder.btn_speaker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AmplifyDialog dialog = new AmplifyDialog(context,String.valueOf(dpo.getId()),"post");
                            dialog.show();
                        }
                    });
                }
                photoViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                /*
                photoViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (photoViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("post",dpo.getId(),photoViewHolder.tv_comment_user.getText().toString());
                            return true;
                        }
                        return false;
                    }
                });
                 */
                photoViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getUser_vote()==0){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getId()),
                                    "1");
                        }else if (dpo.getUser_vote() == 1){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getId()),
                                    "0");
                        }
                    }
                });
                photoViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getUser_vote()==0){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getId()),
                                    "-1");
                        }else if (dpo.getUser_vote() == -1){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getId()),
                                    "0");
                        }
                    }
                });
            }break;
            case 2:{
                final VideoViewHolder videoViewHolder = (VideoViewHolder) holder;
                //Username
                videoViewHolder.tv_username.setText(dpo.getUser_name());
                //Profile image
                BaseFunctions.setFrescoImage(videoViewHolder.img_profile_image,dpo.getUser_thumb_image());
                //BaseFunctions.setGlideImage(context,videoViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                //Address
                videoViewHolder.tv_address.setText(dpo.getUser_city_name()!=null?dpo.getUser_city_name():"");
                //UserRate
                if (dpo.getBusiness_avg_review()!=null) {
                    videoViewHolder.rb_rate.setRating(Float.valueOf(dpo.getBusiness_avg_review()));
                }
                //Time
                videoViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getCreated_at()));
                //Description
                setTags(videoViewHolder.tv_desc,dpo.getCaption());
                //Votes Count
                videoViewHolder.tv_votes_count.setText((dpo.getUpvoting_count()-dpo.getDownvote_count())+"");
                //User vote
                if (dpo.getUser_vote()==1){
                    videoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getUser_vote()==-1){
                    videoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    videoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    videoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                if (dpo.getUser_id() != SharedPrefManager.getInstance(context).getUser().getId()){
                    if (dpo.getDeviceSettingsObject()!=null){
                        if (dpo.getDeviceSettingsObject().getPeople_can_comment_on_posts()==0){
                            videoViewHolder.tv_comment_user.setVisibility(View.GONE);
                        }
                        if (dpo.getDeviceSettingsObject().getPeople_can_vote_posts() == 0){
                            videoViewHolder.btn_down.setVisibility(View.GONE);
                            videoViewHolder.btn_up.setVisibility(View.GONE);
                        }
                    }
                }
                videoViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getUser_vote()==0){
                            callVoteAPI(holder,2,position,"post",String.valueOf(dpo.getId()),
                                    "1");
                        }else if (dpo.getUser_vote() == 1){
                            callVoteAPI(holder,2,position,"post",String.valueOf(dpo.getId()),
                                    "0");
                        }
                    }
                });

                videoViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getUser_vote()==0){
                            callVoteAPI(videoViewHolder,2,position,"post",String.valueOf(dpo.getId()),
                                    "-1");
                        }else if (dpo.getUser_vote() == -1){
                            callVoteAPI(videoViewHolder,2,position,"post",String.valueOf(dpo.getId()),
                                    "0");
                        }
                    }
                });
                videoViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                /*
                videoViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (videoViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("post",dpo.getId(),videoViewHolder.tv_comment_user.getText().toString());
                            videoViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });

                 */
                videoViewHolder.play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHomeVideoDialog dialog = new ViewHomeVideoDialog(context,dpo.getContent());
                        dialog.show();
                    }
                });
                videoViewHolder.thumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHomeVideoDialog dialog = new ViewHomeVideoDialog(context,dpo.getContent());
                        dialog.show();
                    }
                });
                videoViewHolder.tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AllCommentsDialog dialog = new AllCommentsDialog(context,"post",dpo.getId());
                        //dialog.show();
                        Intent intent = new Intent(context,SeeCommentsActivity.class);
                        intent.putExtra("id",dpo.getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });

                videoViewHolder.tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AllReviewsDialog dialog = new AllReviewsDialog(context,"post",dpo.getId());
                        //dialog.show();
                        Intent intent = new Intent(context, SeeReviewsActivity.class);
                        intent.putExtra("id",dpo.getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                videoViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                /*
                videoViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (videoViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("post",dpo.getId(),videoViewHolder.tv_comment_user.getText().toString());
                            videoViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });

                 */

                videoViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getUser_id());
                        context.startActivity(intent);
                    }
                });
                videoViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getUser_id());
                        context.startActivity(intent);
                    }
                });
                videoViewHolder.btn_three_dots.setVisibility(View.GONE);
                if (type.equals("with")) {
                    videoViewHolder.btn_speaker.setVisibility(View.VISIBLE);
                    videoViewHolder.btn_speaker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AmplifyDialog dialog = new AmplifyDialog(context,String.valueOf(dpo.getId()),"post");
                            dialog.show();
                        }
                    });
                }
                videoViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            case 3:{
                final CheckInViewHolder checkInViewHolder = (CheckInViewHolder) holder;

                //Username & Facility name

                checkInViewHolder.tv_username.setText(Html.fromHtml("<b>"+dpo.getUser_name()+"</b>"+" "+
                        context.getResources().getString(R.string.home_is_at)+" "+"<b>"+dpo.getBusiness_name()+"</b>"));
                //Profile image
                if (dpo.getUser_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(checkInViewHolder.img_profile_image,dpo.getUser_thumb_image());
                }
                //Address
                checkInViewHolder.tv_address.setText(dpo.getUser_city_name());
                //Rating
                if (dpo.getBusiness_avg_review()!=null){
                    try {
                        checkInViewHolder.rb_rate.setRating(Float.valueOf(dpo.getBusiness_avg_review()));
                    }catch (Exception e){}
                }
                //Time
                if (dpo.getCreated_at()!=null){
                    checkInViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getCreated_at()));
                }
                //Facility icon
                BaseFunctions.setFrescoImage(checkInViewHolder.img_image,dpo.getBusiness_thumb_image());
                //Facility Name
                checkInViewHolder.tv_name.setText(dpo.getBusiness_name());
                //Facility Rate
                checkInViewHolder.rb_rate2.setRating(Float.valueOf(dpo.getCheck_in_business_avg_review()));
                //Caption
                checkInViewHolder.tv_caption.setText(dpo.getCaption());
                //Votes count
                checkInViewHolder.tv_votes_count.setText((dpo.getUpvoting_count()-dpo.getDownvote_count())+"");
                //User vote
                if (dpo.getUser_vote()==1){
                    checkInViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getUser_vote()==-1){
                    checkInViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    checkInViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    checkInViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                if (dpo.getUser_id() != SharedPrefManager.getInstance(context).getUser().getId()){
                    if (dpo.getDeviceSettingsObject()!=null){
                        if (dpo.getDeviceSettingsObject().getPeople_can_comment_on_posts()==0){
                            checkInViewHolder.tv_comment_user.setVisibility(View.GONE);
                        }
                        if (dpo.getDeviceSettingsObject().getPeople_can_vote_posts() == 0){
                            checkInViewHolder.btn_down.setVisibility(View.GONE);
                            checkInViewHolder.btn_up.setVisibility(View.GONE);
                        }
                    }
                }
                checkInViewHolder.img_broadcast.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callFollowAPI(String.valueOf(dpo.getBusiness_id()),"user",
                                dpo.getIs_following()==0?"yes":"no",
                                context.getResources().getString(R.string.follow_place_true),
                                context.getResources().getString(R.string.follow_place_false),position);
                    }
                });

                checkInViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getUser_vote()==0){
                            callVoteAPI(checkInViewHolder,6,position,"post",String.valueOf(dpo.getId()),
                                    "1");
                        }else if (dpo.getUser_vote() == 1){
                            callVoteAPI(checkInViewHolder,6,position,"post",String.valueOf(dpo.getId()),
                                    "0");
                        }
                    }
                });

                checkInViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getUser_vote()==0){
                            callVoteAPI(checkInViewHolder,6,position,"post",String.valueOf(dpo.getId()),
                                    "-1");
                        }else if (dpo.getUser_vote() == -1){
                            callVoteAPI(checkInViewHolder,6,position,"post",String.valueOf(dpo.getId()),
                                    "0");
                        }
                    }
                });

                checkInViewHolder.tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AllCommentsDialog dialog = new AllCommentsDialog(context,"post",dpo.getId());
                        //dialog.show();
                        Intent intent = new Intent(context,SeeCommentsActivity.class);
                        intent.putExtra("id",dpo.getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });

                checkInViewHolder.tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AllReviewsDialog dialog = new AllReviewsDialog(context,"post",dpo.getId());
                        //dialog.show();
                        Intent intent = new Intent(context, SeeReviewsActivity.class);
                        intent.putExtra("id",dpo.getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                checkInViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                /*
                checkInViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (checkInViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("post",dpo.getId(),checkInViewHolder.tv_comment_user.getText().toString());
                            checkInViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });

                 */

                checkInViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                checkInViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                checkInViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                checkInViewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                checkInViewHolder.btn_three_dots.setVisibility(View.GONE);
                checkInViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
        }
    }

    private void setTags(TextView pTextView, String pTagString) {
        SpannableString string = new SpannableString(pTagString);

        int start = -1;
        for (int i = 0; i < pTagString.length(); i++) {
            if (pTagString.charAt(i) == '#') {
                start = i;
            } else if (pTagString.charAt(i) == ' ' || (i == pTagString.length() - 1 && start != -1)) {
                if (start != -1) {
                    if (i == pTagString.length() - 1) {
                        i++; // case for if hash is last word and there is no
                        // space after word
                    }

                    final String tag = pTagString.substring(start, i);
                    string.setSpan(new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            // link color
                            ds.setColor(Color.parseColor("#F7931D"));
                            ds.setUnderlineText(false);
                        }
                    }, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    start = -1;
                }
            }
        }

        pTextView.setMovementMethod(LinkMovementMethod.getInstance());
        pTextView.setText(string);
    }

    private void callShareAPI(String id,
                              String type,
                              String sharing_text){
        ShareAPIsClass.share(
                context,
                BaseFunctions.getDeviceId(context),
                id,
                type,
                sharing_text,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        Toast.makeText(context, context.getResources().getString(R.string.home_share_success), Toast.LENGTH_SHORT).show();
                        BaseFunctions.playMusic(context,R.raw.added);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callVoteAPI(final RecyclerView.ViewHolder holder,final int view_holder_type, final int position,final String type,final String id,final String value){
        VoteAPIsClass.vote(context,
                BaseFunctions.getDeviceId(context),
                type,
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        Boolean success = new Gson().fromJson(j,Boolean.class);
                        if (success){
                            switch (view_holder_type){
                                //PhotoViewHolder
                                case 1:{
                                    if (value.equals("1")){
                                        ((PhotoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                                        ((PhotoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                        ProfileMediaObject e = list.get(position);
                                        e.setUser_vote(1);
                                        e.setUpvoting_count(e.getUpvoting_count()+1);
                                        ((PhotoViewHolder)holder).tv_votes_count.setText((e.getUpvoting_count()-e.getDownvote_count())+"");
                                        list.set(position,e);
                                        notifyItemChanged(position);
                                        Toast.makeText(context, context.getResources().getString(R.string.vote_on), Toast.LENGTH_SHORT).show();
                                    }else if (value.equals("0")){
                                        ((PhotoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                        ((PhotoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                        ProfileMediaObject o = list.get(position);
                                        if (o.getUser_vote() == 1){
                                            o.setUpvoting_count(o.getUpvoting_count()-1);
                                        }else if (o.getUser_vote() == -1){
                                            o.setDownvote_count(o.getDownvote_count()-1);
                                        }
                                        ((PhotoViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()-o.getDownvote_count())+"");
                                        o.setUser_vote(0);
                                        list.set(position,o);
                                        notifyItemChanged(position);
                                        Toast.makeText(context, context.getResources().getString(R.string.vote_removed), Toast.LENGTH_SHORT).show();
                                    }else if (value.equals("-1")){
                                        ((PhotoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                        ((PhotoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                                        ProfileMediaObject o = list.get(position);
                                        o.setUser_vote(-1);
                                        o.setDownvote_count(o.getDownvote_count()+1);
                                        ((PhotoViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()-o.getDownvote_count())+"");
                                        list.set(position,o);
                                        notifyItemChanged(position);
                                        Toast.makeText(context, context.getResources().getString(R.string.vote_off), Toast.LENGTH_SHORT).show();
                                    }
                                }break;
                                //VideoViewHolder
                                case 2:{
                                    if (value.equals("1")){
                                        ((VideoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                                        ((VideoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                        ProfileMediaObject o = list.get(position);
                                        o.setUser_vote(1);
                                        o.setUpvoting_count(o.getUpvoting_count()+1);
                                        ((VideoViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()-o.getDownvote_count())+"");
                                        list.set(position,o);
                                        notifyItemChanged(position);
                                        Toast.makeText(context, context.getResources().getString(R.string.vote_on), Toast.LENGTH_SHORT).show();
                                    }else if (value.equals("0")){
                                        ((VideoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                        ((VideoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                        ProfileMediaObject o = list.get(position);
                                        if (o.getUser_vote() == 1){
                                            o.setUpvoting_count(o.getUpvoting_count()-1);
                                        }else if (o.getUser_vote() == -1){
                                            o.setDownvote_count(o.getDownvote_count()-1);
                                        }
                                        ((VideoViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()-o.getDownvote_count())+"");
                                        o.setUser_vote(0);
                                        list.set(position,o);
                                        notifyItemChanged(position);
                                        Toast.makeText(context, context.getResources().getString(R.string.vote_removed), Toast.LENGTH_SHORT).show();
                                    }else if (value.equals("-1")){
                                        ((VideoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                        ((VideoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                                        ProfileMediaObject o = list.get(position);
                                        o.setUser_vote(-1);
                                        o.setDownvote_count(o.getDownvote_count()+1);
                                        ((VideoViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()-o.getDownvote_count())+"");
                                        list.set(position,o);
                                        notifyItemChanged(position);
                                        Toast.makeText(context, context.getResources().getString(R.string.vote_off), Toast.LENGTH_SHORT).show();
                                    }
                                }break;
                                //CheckInViewHolder
                                case 3:{
                                    if (value.equals("1")){
                                        ((CheckInViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                                        ((CheckInViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                        ProfileMediaObject o = list.get(position);
                                        o.setUser_vote(1);
                                        o.setUpvoting_count(o.getUpvoting_count()+1);
                                        ((CheckInViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()-o.getDownvote_count())+"");
                                        list.set(position,o);
                                        notifyItemChanged(position);
                                        Toast.makeText(context, context.getResources().getString(R.string.vote_on), Toast.LENGTH_SHORT).show();
                                    }else if (value.equals("0")){
                                        ((CheckInViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                        ((CheckInViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                        ProfileMediaObject o = list.get(position);
                                        if (o.getUser_vote() == 1){
                                            o.setUpvoting_count(o.getUpvoting_count()-1);
                                        }else if (o.getUser_vote() == -1){
                                            o.setDownvote_count(o.getDownvote_count()-1);
                                        }
                                        ((CheckInViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()-o.getDownvote_count())+"");
                                        o.setUser_vote(0);
                                        list.set(position,o);
                                        notifyItemChanged(position);
                                        Toast.makeText(context, context.getResources().getString(R.string.vote_removed), Toast.LENGTH_SHORT).show();
                                    }else if (value.equals("-1")){
                                        ((CheckInViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                        ((CheckInViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                                        ProfileMediaObject o = list.get(position);
                                        o.setUser_vote(-1);
                                        o.setDownvote_count(o.getDownvote_count()+1);
                                        ((CheckInViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()-o.getDownvote_count())+"");
                                        list.set(position,o);
                                        notifyItemChanged(position);
                                        Toast.makeText(context, context.getResources().getString(R.string.vote_off), Toast.LENGTH_SHORT).show();
                                    }
                                }break;

                            }
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callFollowAPI(String id,
                               String type,
                               final String value,
                               final String follow_message,
                               final String unfollow_message,
                               final int position){
        FollowersAPIsClass.followNew(
                context,
                BaseFunctions.getDeviceId(context),
                type,
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                if (value.equals("yes")){
                                    Toast.makeText(context, follow_message, Toast.LENGTH_SHORT).show();
                                    ProfileMediaObject o = list.get(position);
                                    o.setIs_following(1);
                                    list.set(position,o);
                                    notifyItemChanged(position);
                                }else {
                                    Toast.makeText(context, unfollow_message, Toast.LENGTH_SHORT).show();
                                    ProfileMediaObject o = list.get(position);
                                    o.setIs_following(0);
                                    list.set(position,o);
                                    notifyItemChanged(position);
                                }
                            }else {
                                Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
