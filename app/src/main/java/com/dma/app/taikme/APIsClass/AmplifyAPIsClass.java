package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.AmplifyAPIs;
import com.dma.app.taikme.APIs.ReviewsAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Field;

public class AmplifyAPIsClass extends BaseRetrofit {

    private static NewProgressDialog dialog;

    public static void create(final Context context,
                              String device_id,
                              String en_name,
                              String ar_name,
                              int budget,
                              int location_id,
                              int custom_audience_id,
                              String gender,
                              int age_range_from,
                              int age_range_to,
                              String followers,
                              String amplifiable_id,
                              String amplifiable_type,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        AmplifyAPIs api = retrofit.create(AmplifyAPIs.class);
        Call<BaseResponse> call = api.create("application/json",
                device_id,
                en_name,
                ar_name,
                budget,
                location_id,
                custom_audience_id,
                gender,
                age_range_from,
                age_range_to,
                followers,
                amplifiable_id,
                amplifiable_type);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void get_all(final Context context,
                              String device_id,
                              int page,
                               final int type,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        if (type == 0) {
            dialog = new NewProgressDialog(context);
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(context);
        AmplifyAPIs api = retrofit.create(AmplifyAPIs.class);
        Call<BaseResponse> call = api.get_all("application/json",
                device_id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0) {
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0) {
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

}
