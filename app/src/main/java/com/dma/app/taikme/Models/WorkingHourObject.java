package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class WorkingHourObject {
    @SerializedName("day") private int day = 0;
    @SerializedName("from") private String from = "";
    @SerializedName("to") private String to = "";

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
