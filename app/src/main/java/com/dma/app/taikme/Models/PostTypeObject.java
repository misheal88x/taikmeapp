package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class PostTypeObject {
    @SerializedName("photo") private int photo = 0;
    @SerializedName("video") private int video = 0;
    @SerializedName("checkin") private int checkin = 0;

    public PostTypeObject() { }

    public PostTypeObject(int photo, int video, int checkin) {
        this.photo = photo;
        this.video = video;
        this.checkin = checkin;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public int getVideo() {
        return video;
    }

    public void setVideo(int video) {
        this.video = video;
    }

    public int getCheckin() {
        return checkin;
    }

    public void setCheckin(int checkin) {
        this.checkin = checkin;
    }
}
