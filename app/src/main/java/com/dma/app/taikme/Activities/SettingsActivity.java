package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Dialogs.ChangeMobileNumberDialog;
import com.dma.app.taikme.Dialogs.ChangePasswordDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.SettingsObject;
import com.dma.app.taikme.Models.UserObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.gson.Gson;

public class SettingsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private RelativeLayout btn_back;
    private TextView tv_title;
    private CheckBox chk_visible_to_search,chk_allow_people_to_follow,
    chk_is_profile_public,chk_stories_visible,chk_people_can_comment,chk_people_can_vote,
    chk_comments_notifications,chk_votes_notifications,chk_english,
    chk_arabic;
    private boolean is_restored = false;
    private SettingsObject settingsObject,backupSettingsObject;
    private LinearLayout lyt_change_phone_number,lyt_change_password;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_settings);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.settings_title));
        settingsObject = SharedPrefManager.getInstance(SettingsActivity.this).getSettings();
        backupSettingsObject = SharedPrefManager.getInstance(SettingsActivity.this).getSettings();
        restore_data(settingsObject);
    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = findViewById(R.id.settings_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        //Checkbox
        chk_visible_to_search = findViewById(R.id.settings_visible_to_search);
        chk_allow_people_to_follow = findViewById(R.id.settings_allow_people_to_follow);
        chk_is_profile_public = findViewById(R.id.settings_is_profile_public);
        chk_stories_visible = findViewById(R.id.settings_make_stories_visible);
        chk_people_can_comment = findViewById(R.id.settings_people_can_comment);
        chk_people_can_vote = findViewById(R.id.settings_people_can_vote);
        chk_comments_notifications = findViewById(R.id.settings_comments_notifications);
        chk_votes_notifications = findViewById(R.id.settings_votes_notifications);
        chk_english = findViewById(R.id.settings_english_language);
        chk_arabic = findViewById(R.id.settings_arabic_language);
        //LinearLayout
        lyt_change_phone_number = findViewById(R.id.settings_change_phone_number);
        lyt_change_password = findViewById(R.id.settings_change_password);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(SettingsActivity.this);
            }
        });
        chk_visible_to_search.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (is_restored){
                    if (isChecked){
                        settingsObject.setVisible_to_search(1);
                    }else {
                        settingsObject.setVisible_to_search(0);
                    }
                    SharedPrefManager.getInstance(SettingsActivity.this).setSettings(settingsObject);
                    callUpdateSettingsAPI();
                }
            }
        });
        chk_allow_people_to_follow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (is_restored){
                    if (isChecked){
                        settingsObject.setAllow_people_to_follow(1);
                    }else {
                        settingsObject.setAllow_people_to_follow(0);
                    }
                    SharedPrefManager.getInstance(SettingsActivity.this).setSettings(settingsObject);
                    callUpdateSettingsAPI();
                }
            }
        });
        chk_is_profile_public.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (is_restored){
                    if (isChecked){
                        settingsObject.setIs_profile_public(1);
                    }else {
                        if(SharedPrefManager.getInstance(SettingsActivity.this).getUser().getUser_type().equals(UserObject.BUSINESS_TYPE)){
                            Toast.makeText(SettingsActivity.this,getResources().getString(R.string.settings_cant_be_private) , Toast.LENGTH_SHORT).show();
                        }else{
                            settingsObject.setIs_profile_public(0);
                        }
                    }
                    SharedPrefManager.getInstance(SettingsActivity.this).setSettings(settingsObject);
                    callUpdateSettingsAPI();
                }
            }
        });
        chk_stories_visible.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (is_restored){
                    if (isChecked){
                        settingsObject.setIs_stories_visible(1);
                    }else {
                        settingsObject.setIs_stories_visible(0);
                    }
                    SharedPrefManager.getInstance(SettingsActivity.this).setSettings(settingsObject);
                    callUpdateSettingsAPI();
                }
            }
        });
        chk_people_can_comment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (is_restored){
                    if (isChecked){
                        settingsObject.setPeople_can_comment_on_posts(1);
                    }else {
                        settingsObject.setPeople_can_comment_on_posts(0);
                    }
                    SharedPrefManager.getInstance(SettingsActivity.this).setSettings(settingsObject);
                    callUpdateSettingsAPI();
                }
            }
        });
        chk_people_can_vote.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (is_restored){
                    if (isChecked){
                        settingsObject.setPeople_can_vote_posts(1);
                    }else {
                        settingsObject.setPeople_can_vote_posts(0);
                    }
                    SharedPrefManager.getInstance(SettingsActivity.this).setSettings(settingsObject);
                    callUpdateSettingsAPI();
                }
            }
        });
        chk_comments_notifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (is_restored){
                    if (isChecked){
                        settingsObject.setComments_notifications(1);
                    }else {
                        settingsObject.setComments_notifications(0);
                    }
                    SharedPrefManager.getInstance(SettingsActivity.this).setSettings(settingsObject);
                    callUpdateSettingsAPI();
                }
            }
        });
        chk_votes_notifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (is_restored){
                    if (isChecked){
                        settingsObject.setVotes_notifications(1);
                    }else {
                        settingsObject.setVotes_notifications(0);
                    }
                    SharedPrefManager.getInstance(SettingsActivity.this).setSettings(settingsObject);
                    callUpdateSettingsAPI();
                }
            }
        });
        chk_english.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (is_restored){
                    if (isChecked){
                        settingsObject.setView_language("en");
                        chk_arabic.setChecked(false);
                        SharedPrefManager.getInstance(SettingsActivity.this).setSettings(settingsObject);
                        callUpdateSettingsAPI();
                    }
                }
            }
        });
        chk_arabic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (is_restored){
                    if (isChecked){
                        settingsObject.setView_language("ar");
                        chk_english.setChecked(false);
                        SharedPrefManager.getInstance(SettingsActivity.this).setSettings(settingsObject);
                        callUpdateSettingsAPI();
                    }
                }
            }
        });
        lyt_change_phone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeMobileNumberDialog dialog = new ChangeMobileNumberDialog(SettingsActivity.this);
                dialog.show();
            }
        });
        lyt_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordDialog dialog = new ChangePasswordDialog(SettingsActivity.this);
                dialog.show();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void restore_data(SettingsObject settingsObject){
        settingsObject = SharedPrefManager.getInstance(SettingsActivity.this).getSettings();

        //Visible to search
        setupCheckBox(chk_visible_to_search,settingsObject.getVisible_to_search());
        //Allow People to follow
        setupCheckBox(chk_allow_people_to_follow,settingsObject.getAllow_people_to_follow());
        //Is profile public
        setupCheckBox(chk_is_profile_public,settingsObject.getIs_profile_public());
        //Stories Visible
        setupCheckBox(chk_stories_visible,settingsObject.getIs_stories_visible());
        //Comment on posts
        setupCheckBox(chk_people_can_comment,settingsObject.getPeople_can_comment_on_posts());
        //Vote on posts
        setupCheckBox(chk_people_can_vote,settingsObject.getPeople_can_vote_posts());
        //Comments notifications
        setupCheckBox(chk_comments_notifications,settingsObject.getComments_notifications());
        //Votes notifications
        setupCheckBox(chk_votes_notifications,settingsObject.getVotes_notifications());
        //Language
        if (settingsObject.getView_language().equals("en")){
            chk_english.setChecked(true);
            chk_arabic.setChecked(false);
        }else if (settingsObject.getView_language().equals("ar")){
            chk_arabic.setChecked(true);
            chk_english.setChecked(false);
        }
        is_restored = true;
    }

    private void setupCheckBox(CheckBox checkBox,int value){
        if (value == 0){
            checkBox.setChecked(false);
        }else {
            checkBox.setChecked(true);
        }
    }

    private void callUpdateSettingsAPI(){
        UserAPIsClass.updateSettings(
                SettingsActivity.this,
                BaseFunctions.getDeviceId(SettingsActivity.this),
                settingsObject.getVisible_to_search(),
                settingsObject.getAllow_people_to_follow(),
                settingsObject.getIs_profile_public(),
                settingsObject.getIs_stories_visible(),
                settingsObject.getPeople_can_comment_on_posts(),
                settingsObject.getPeople_can_vote_posts(),
                settingsObject.getComments_notifications(),
                settingsObject.getVotes_notifications(),
                settingsObject.getView_language(),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        is_restored = false;
                        restore_data(backupSettingsObject);
                        SharedPrefManager.getInstance(SettingsActivity.this).setSettings(backupSettingsObject);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            boolean success = new Gson().fromJson(json1,Boolean.class);
                            if (success == true){
                                Toast.makeText(SettingsActivity.this, getResources().getString(R.string.settings_update_successfully), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(SettingsActivity.this,SplashActivity.class));
                                finish();
                            }else {
                                is_restored = false;
                                //Toast.makeText(SettingsActivity.this, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                                restore_data(backupSettingsObject);
                                SharedPrefManager.getInstance(SettingsActivity.this).setSettings(backupSettingsObject);
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(SettingsActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        is_restored = false;
                        restore_data(backupSettingsObject);
                        SharedPrefManager.getInstance(SettingsActivity.this).setSettings(backupSettingsObject);
                    }
                });
    }
}
