package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.EventsAPIs;
import com.dma.app.taikme.APIs.NotificationsAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NotificationsAPIsClass extends BaseRetrofit {

    private static NewProgressDialog dialog;

    public static void getAllNotifications(final Context context,
                                    String device_id,
                                    int page,
                                           final int type,
                                    IResponse onResponse1,
                                    final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        if (type == 0){
            dialog = new NewProgressDialog(context);
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(context);
        NotificationsAPIs api = retrofit.create(NotificationsAPIs.class);
        Call<BaseResponse> call = api.get_all("application/json",
                device_id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if(type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }
}
