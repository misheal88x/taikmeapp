package com.dma.app.taikme.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.BubbleThumbSeekbar;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.dma.app.taikme.APIsClass.AuthAPIsClass;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Dialogs.ChooseSelectionDialog;
import com.dma.app.taikme.Dialogs.MultiSelectDialog;
import com.dma.app.taikme.Fragments.ProfileFragment;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMultiSelect;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Interfaces.ISelection;
import com.dma.app.taikme.Models.CuisineObject;
import com.dma.app.taikme.Models.LebanonCityObject;
import com.dma.app.taikme.Models.LocationObject;
import com.dma.app.taikme.Models.LoginResponse;
import com.dma.app.taikme.Models.MusicGenderObject;
import com.dma.app.taikme.Models.ProfileObject;
import com.dma.app.taikme.Models.SettingsObject;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Models.StartupObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.LocaleHelper;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.core.app.ActivityCompat;
import de.hdodenhof.circleimageview.CircleImageView;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends BaseActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate{

    private RelativeLayout root;
    private EditText edt_userName,edt_phone_number,edt_password,edt_confirm;
    private Spinner gender_spinner,country_spinner,area_spinner,cuisine_spinner,second_cuisine_spinner,
    third_cuisine_spinner,music_spinner,second_music_spinner,third_music_spinner,
    am_spinner;
    private CrystalSeekbar age_seek;
    private TextView age_value;

    private TextView prefer_spinner,usually_spinner;

    private List<String> gender_list_string;
    private List<StartupItemObject> gender_list;
    private List<String> country_list_string;
    private List<String> area_list_string;
    private List<String> cuisine_list_string;
    private List<String> second_cuisine_list_string;
    private List<String> third_cuisine_list_string;
    private List<String> music_list_string;
    private List<String> second_music_list_string;
    private List<String> third_music_list_string;
    //private List<String> prefer_list_string;
    private List<String> am_list_string;
    private List<StartupItemObject> am_list;
    //private List<String> usually_list_string;
    private List<Integer> selected_prefer,selected_prefer_indexes;
    private List<Integer> selected_usually,selected_usually_indexes;

    private List<LebanonCityObject> area_list;
    private List<StartupItemObject> cuisine_list,second_cuisine_list,third_cuisine_list;
    private List<StartupItemObject> music_list,second_music_list,third_music_list;

    private CustomSpinnerAdapter gender_adapter,country_adapter,area_adapter,cuisine_adapter,second_cuisine_adapter,
            third_cuisine_adapter,music_adapter,second_music_adapter,third_music_adapter,
            am_adapter;
    private TextView tv_agree;

    private CircleImageView profile_image;
    private TextView profile_image_hint;
    private RelativeLayout profile_image_layout;
    private File imageFile;
    private String imagePath = "";
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";

    private Button btn_register;

    private String selected_age = "";
    private String selected_gender = "";
    private String selected_country = "";
    private String selected_city = "";
    private String selected_first_cuisine = "";
    private String selected_second_cuisine = "";
    private String selected_third_cuisine = "";
    private String selected_first_music = "";
    private String selected_second_music = "";
    private String selected_third_music = "";
    private String selected_i_am = "";

    private CheckBox agree,understand;

    private String type = "";

    private TextView tv_arabic,tv_english;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_register);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        init_spinners();
        tv_agree.setText(Html.fromHtml(getResources().getString(R.string.register_agree)+" "+
                "<font color=#F7931D>"+getResources().getString(R.string.register_terms)+"</font>"+" "+
        getResources().getString(R.string.register_this_app)));
        type = getIntent().getStringExtra("type");
        if (type.equals("edit")){
            profile_image_hint.setVisibility(View.GONE);
            BaseFunctions.setGlideImage(RegisterActivity.this,profile_image,getIntent().getStringExtra("image"));
            edt_userName.setText(getIntent().getStringExtra("name"));
            edt_phone_number.setVisibility(View.GONE);
            edt_password.setVisibility(View.GONE);
            edt_confirm.setVisibility(View.GONE);
        }
    }

    @Override
    public void init_views() {
        //RelativeLayout
        root = findViewById(R.id.register_layout);
        profile_image_layout = findViewById(R.id.profile_image_layout);
        //EditText
        edt_userName = findViewById(R.id.register_username);
        edt_phone_number = findViewById(R.id.register_phone_number);
        edt_password = findViewById(R.id.register_password);
        edt_confirm = findViewById(R.id.register_confirm);
        //TextViews
        tv_agree = findViewById(R.id.register_agree_txt);
        profile_image_hint = findViewById(R.id.register_pick_image);
        tv_arabic = findViewById(R.id.login_arabic_language);
        tv_english = findViewById(R.id.login_english_language);
        age_value = findViewById(R.id.age_value);
        //Spinners
        gender_spinner = findViewById(R.id.register_gender_spinner);
        country_spinner = findViewById(R.id.register_country_spinner);
        area_spinner = findViewById(R.id.register_area_spinner);
        cuisine_spinner = findViewById(R.id.register_best_cuisine_spinner);
        second_cuisine_spinner = findViewById(R.id.register_second_best_cuisine_spinner);
        third_cuisine_spinner = findViewById(R.id.register_third_best_cuisine_spinner);
        music_spinner = findViewById(R.id.register_best_music_spinner);
        second_music_spinner = findViewById(R.id.register_second_music_spinner);
        third_music_spinner = findViewById(R.id.register_third_music_spinner);
        prefer_spinner = findViewById(R.id.register_prefer_spinner);
        am_spinner = findViewById(R.id.register_am_spinner);
        usually_spinner = findViewById(R.id.register_usually_spinner);
        //Button
        btn_register = findViewById(R.id.register_register);
        //Circle Image View
        profile_image = findViewById(R.id.register_image);
        //Checkbox
        agree = findViewById(R.id.register_agree);
        understand = findViewById(R.id.register_understand);
        selected_prefer_indexes = new ArrayList<>();
        selected_usually_indexes = new ArrayList<>();
        //Seekbar
        age_seek = findViewById(R.id.age_seek);

    }

    @Override
    public void init_events() {
        profile_image_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(RegisterActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA},
                        1);
            }
        });
        age_seek.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                selected_age = String.valueOf(value);
                age_value.setText(selected_age);
            }
        });

        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_gender = "";
                }else {
                    selected_gender = gender_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                StartupObject so = SharedPrefManager.getInstance(RegisterActivity.this).getStartUp();
                String language = SharedPrefManager.getInstance(RegisterActivity.this).getSettings().getView_language();
                if (position>0){
                    selected_country = country_list_string.get(position);
                    if (selected_country.equals("Lebanon")){
                        if (so.getLebanon_cities()!=null){
                            if (so.getLebanon_cities().size()>0){
                                for (LebanonCityObject lo : so.getLebanon_cities()){
                                    area_list.add(lo);
                                    if (language.equals("en")){
                                        area_list_string.add(lo.getEn_name());
                                    }else {
                                        area_list_string.add(lo.getAr_name());
                                    }
                                }
                            }
                        }
                        area_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,area_list_string);
                        area_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        area_spinner.setAdapter(area_adapter);
                    }else {
                        area_list.clear();
                        area_list_string.clear();
                        area_list_string.add(getResources().getString(R.string.register_area));
                        area_list.add(new LebanonCityObject());
                        area_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,area_list_string);
                        area_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        area_spinner.setAdapter(area_adapter);
                    }
                }else {
                    selected_country = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        area_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_city = "";
                }else {
                    selected_city = String.valueOf(area_list.get(position).getEn_name());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cuisine_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_first_cuisine = "";
                }else {
                    if (!selected_third_cuisine.equals(cuisine_list.get(position).getId())&&
                            !selected_second_cuisine.equals(cuisine_list.get(position).getId())) {
                        selected_first_cuisine = cuisine_list.get(position).getId();
                    }else {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_first_cuisine = "";
                        cuisine_spinner.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        second_cuisine_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_second_cuisine = "";
                }else {
                    if (!selected_first_cuisine.equals(second_cuisine_list.get(position).getId())&&
                            !selected_third_cuisine.equals(second_cuisine_list.get(position).getId())) {
                        selected_second_cuisine = second_cuisine_list.get(position).getId();
                    }else {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_second_cuisine = "";
                        second_cuisine_spinner.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        third_cuisine_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_third_cuisine = "";
                }else {
                    if (!selected_first_cuisine.equals(third_cuisine_list.get(position).getId())&&
                            !selected_second_cuisine.equals(third_cuisine_list.get(position).getId())) {
                        selected_third_cuisine = third_cuisine_list.get(position).getId();
                    }else {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_third_cuisine = "";
                        third_cuisine_spinner.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        music_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_first_music = "";
                }else {
                    if (!selected_third_music.equals(music_list.get(position).getId())&&
                            !selected_second_music.equals(music_list.get(position).getId())) {
                        selected_first_music = music_list.get(position).getId();
                    }else {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_first_music = "";
                        music_spinner.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        second_music_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_second_music = "";
                }else {
                    if (!selected_first_music.equals(second_music_list.get(position).getId())&&
                            !selected_third_music.equals(second_music_list.get(position).getId())) {
                        selected_second_music = second_music_list.get(position).getId();
                    }else {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_second_music = "";
                        second_music_spinner.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        third_music_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_third_music = "";
                }else {
                    if (!selected_first_music.equals(third_music_list.get(position).getId())&&
                    !selected_second_music.equals(third_music_list.get(position).getId())) {
                        selected_third_music = third_music_list.get(position).getId();
                    }else {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_third_music = "";
                        third_music_spinner.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        am_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_i_am = "";
                }else {
                    selected_i_am = am_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        prefer_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final StartupObject so = SharedPrefManager.getInstance(RegisterActivity.this).getStartUp();
                List<String> names = new ArrayList<>();
                for (StartupItemObject s : so.getBusiness_atmosphere()){
                    names.add(s.getName());
                }
                        MultiSelectDialog dialog = new MultiSelectDialog(RegisterActivity.this,
                        names,selected_prefer_indexes, new IMultiSelect() {
                    @Override
                    public void onSelected(List<Integer> list) {
                        selected_prefer.clear();
                        selected_prefer_indexes.clear();
                        if (list.size()>0){
                            String prefer_txt = "";
                            for (Integer i : list){
                                selected_prefer_indexes.add(i);
                                selected_prefer.add(Integer.valueOf(so.getBusiness_atmosphere().get(i).getId()));
                                prefer_txt+=so.getBusiness_atmosphere().get(i).getName()+" ";
                                prefer_spinner.setText(prefer_txt);
                            }
                        }else {
                            prefer_spinner.setText(getResources().getString(R.string.register_prefer));
                        }
                    }
                });
                dialog.show();
            }
        });
        usually_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> source_names = new ArrayList<>();
                final StartupObject so = SharedPrefManager.getInstance(RegisterActivity.this).getStartUp();
                if (so.getGoing_on()!=null && so.getGoing_on().size()>0){
                    for (StartupItemObject sio : so.getGoing_on()){
                        source_names.add(sio.getName());
                    }
                }
                MultiSelectDialog dialog = new MultiSelectDialog(RegisterActivity.this,
                        source_names,selected_usually_indexes, new IMultiSelect() {
                    @Override
                    public void onSelected(List<Integer> list) {
                        selected_usually.clear();
                        selected_usually_indexes.clear();
                        if (list.size()>0){
                            String usually_txt = "";
                            for (Integer i : list){
                                selected_usually_indexes.add(i);
                                selected_usually.add(Integer.valueOf(so.getGoing_on().get(i).getId()));
                                usually_txt+=so.getGoing_on().get(i).getName()+" ";
                                usually_spinner.setText(usually_txt);
                            }
                        }else {
                            usually_spinner.setText(getResources().getString(R.string.register_usually_go));
                        }
                    }
                });
                dialog.show();
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("register")) {
                    if (imagePath.equals("")) {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_profile), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(edt_phone_number.getText().toString())) {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_phone_number), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (edt_phone_number.getText().toString().length() < 10) {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_short_phone_number), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(edt_password.getText().toString())) {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_password), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (edt_password.getText().toString().length() < 8) {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_password_short), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (edt_confirm.getText().toString().equals("")) {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_confirm), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (!edt_confirm.getText().toString().equals(edt_password.getText().toString())) {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_match), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (TextUtils.isEmpty(edt_userName.getText().toString())){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_username), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_age.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_age), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_gender.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_gender), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_country.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_country), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_city.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_city), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_first_cuisine.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_first_cuisine), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_second_cuisine.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_second_cuisine), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_third_cuisine.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_third_cuisine), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_first_music.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_first_music), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_second_music.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_second_music), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_third_music.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_third_music), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_prefer.size() == 0){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_prefer), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_i_am.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_i_am), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_usually.size() == 0){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_go_out), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!agree.isChecked()){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_agree), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!understand.isChecked()){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_understand), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (type.equals("register")) {
                    callRegisterAPI();
                }else {
                    callUpdateSocial();
                }
            }
        });

        tv_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefManager.getInstance(RegisterActivity.this).getSettings()!=null){
                    if (!SharedPrefManager.getInstance(RegisterActivity.this).getSettings().getView_language().equals("en")){
                        SettingsObject o = SharedPrefManager.getInstance(RegisterActivity.this).getSettings();
                        o.setView_language("en");
                        SharedPrefManager.getInstance(RegisterActivity.this).setSettings(o);
                        LocaleHelper.setLocale(RegisterActivity.this, "en");
                        //attachBaseContext(CalligraphyContextWrapper.wrap(LocaleHelper.onAttach(RegisterActivity.this)));
                        Intent intent = new Intent(RegisterActivity.this,RegisterActivity.class);
                        intent.putExtra("type",getIntent().getStringExtra("type"));
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
        tv_arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefManager.getInstance(RegisterActivity.this).getSettings()!=null){
                    if (!SharedPrefManager.getInstance(RegisterActivity.this).getSettings().getView_language().equals("ar")){
                        SettingsObject o = SharedPrefManager.getInstance(RegisterActivity.this).getSettings();
                        o.setView_language("ar");
                        SharedPrefManager.getInstance(RegisterActivity.this).setSettings(o);
                        LocaleHelper.setLocale(RegisterActivity.this, "ar");
                        //attachBaseContext(CalligraphyContextWrapper.wrap(LocaleHelper.onAttach(RegisterActivity.this)));
                        Intent intent = new Intent(RegisterActivity.this,RegisterActivity.class);
                        intent.putExtra("type",getIntent().getStringExtra("type"));
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_spinners(){
        StartupObject so = SharedPrefManager.getInstance(RegisterActivity.this).getStartUp();
        String language = SharedPrefManager.getInstance(RegisterActivity.this).getSettings().getView_language();

        gender_list_string = new ArrayList<>();
        gender_list = new ArrayList<>();
        country_list_string = new ArrayList<>();
        area_list_string = new ArrayList<>();
        area_list = new ArrayList<>();
        cuisine_list_string = new ArrayList<>();
        cuisine_list = new ArrayList<>();
        second_cuisine_list = new ArrayList<>();
        third_cuisine_list = new ArrayList<>();
        second_cuisine_list_string = new ArrayList<>();
        third_cuisine_list_string = new ArrayList<>();
        music_list_string = new ArrayList<>();
        second_music_list_string = new ArrayList<>();
        third_music_list_string = new ArrayList<>();
        music_list = new ArrayList<>();
        second_music_list = new ArrayList<>();
        third_music_list = new ArrayList<>();
        //prefer_list_string = new ArrayList<>();
        am_list_string = new ArrayList<>();
        am_list = new ArrayList<>();
        //usually_list_string = new ArrayList<>();
        selected_prefer = new ArrayList<>();
        selected_usually = new ArrayList<>();

        gender_list_string.add(getResources().getString(R.string.register_gender));
        gender_list.add(new StartupItemObject());
        if (so.getGenders()!=null && so.getGenders().size()>0){
            for (StartupItemObject sio : so.getGenders()){
                gender_list.add(sio);
                gender_list_string.add(sio.getName());
            }
        }
        country_list_string.add(getResources().getString(R.string.register_country));
        if (so.getCountries()!=null){
            if (so.getCountries().size()>0){
                for (String s : so.getCountries()){
                    country_list_string.add(s);
                }
            }
        }

        area_list_string.add(getResources().getString(R.string.register_area));
        area_list.add(new LebanonCityObject());
        /*
        if (so.getLocations()!=null){
            if (so.getLocations().size()>0){
                for (LocationObject lo : so.getLocations()){
                    area_list.add(lo);
                    if (language.equals("en")){
                        area_list_string.add(lo.getEn_name());
                    }else {
                        area_list_string.add(lo.getAr_name());
                    }
                }
            }
        }
         */

        cuisine_list_string.add(getResources().getString(R.string.register_best_cuisine));
        cuisine_list.add(new StartupItemObject());
        if (so.getCuisine_definitions()!=null){
            if (so.getCuisine_definitions().size()>0){
                for (StartupItemObject co : so.getCuisine_definitions()){
                    cuisine_list.add(co);
                    cuisine_list_string.add(co.getName());
                }
            }
        }

        second_cuisine_list_string.add(getResources().getString(R.string.register_second_cuisine));
        second_cuisine_list.add(new StartupItemObject());
        if (so.getCuisine_definitions()!=null){
            if (so.getCuisine_definitions().size()>0){
                for (StartupItemObject co : so.getCuisine_definitions()){
                    second_cuisine_list.add(co);
                    second_cuisine_list_string.add(co.getName());
                }
            }
        }

        third_cuisine_list_string.add(getResources().getString(R.string.register_third_cuisine));
        third_cuisine_list.add(new StartupItemObject());
        if (so.getCuisine_definitions()!=null){
            if (so.getCuisine_definitions().size()>0){
                for (StartupItemObject co : so.getCuisine_definitions()){
                    third_cuisine_list.add(co);
                    third_cuisine_list_string.add(co.getName());
                }
            }
        }

        music_list_string.add(getResources().getString(R.string.register_music));
        music_list.add(new StartupItemObject());
        if (so.getMusic_gender_type()!=null){
            if (so.getMusic_gender_type().size()>0){
                for (StartupItemObject mo : so.getMusic_gender_type()){
                    music_list.add(mo);
                    music_list_string.add(mo.getName());
                }
            }
        }

        second_music_list_string.add(getResources().getString(R.string.register_second_music));
        second_music_list.add(new StartupItemObject());
        if (so.getMusic_gender_type()!=null){
            if (so.getMusic_gender_type().size()>0){
                for (StartupItemObject mo : so.getMusic_gender_type()){
                    second_music_list.add(mo);
                    second_music_list_string.add(mo.getName());
                }
            }
        }
        third_music_list_string.add(getResources().getString(R.string.register_third_music));
        third_music_list.add(new StartupItemObject());
        if (so.getMusic_gender_type()!=null){
            if (so.getMusic_gender_type().size()>0){
                for (StartupItemObject mo : so.getMusic_gender_type()){
                    third_music_list.add(mo);
                    third_music_list_string.add(mo.getName());
                }
            }
        }

        //if (so.getI_prefer()!=null){
          //  if (so.getI_prefer().size()>0){

          //  }
       // }

        am_list_string.add(getResources().getString(R.string.register_am));
        am_list.add(new StartupItemObject());
        if (so.getMy_place_better_for()!=null){
            if (so.getMy_place_better_for().size()>0){
                for (StartupItemObject s : so.getMy_place_better_for()){
                    am_list_string.add(s.getName());
                    am_list.add(s);
                }
            }
        }

        gender_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,gender_list_string);
        country_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,country_list_string);
        area_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,area_list_string);
        cuisine_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,cuisine_list_string);
        second_cuisine_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,second_cuisine_list_string);
        third_cuisine_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,third_cuisine_list_string);
        music_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,music_list_string);
        second_music_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,second_music_list_string);
        third_music_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,third_music_list_string);
        //prefer_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,prefer_list_string);
        am_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,am_list_string);
        //usually_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_item,usually_list_string);


        gender_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        country_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        area_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cuisine_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        second_cuisine_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        third_cuisine_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        music_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        second_music_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        third_music_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //prefer_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        am_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //usually_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        gender_spinner.setAdapter(gender_adapter);
        country_spinner.setAdapter(country_adapter);
        area_spinner.setAdapter(area_adapter);
        cuisine_spinner.setAdapter(cuisine_adapter);
        second_cuisine_spinner.setAdapter(second_cuisine_adapter);
        third_cuisine_spinner.setAdapter(third_cuisine_adapter);
        music_spinner.setAdapter(music_adapter);
        second_music_spinner.setAdapter(second_music_adapter);
        third_music_spinner.setAdapter(third_music_adapter);
        //prefer_spinner.setAdapter(prefer_adapter);
        am_spinner.setAdapter(am_adapter);
        //usually_spinner.setAdapter(usually_adapter);
    }

    private void callRegisterAPI(){
        AuthAPIsClass.NormalRegister(
                RegisterActivity.this,
                BaseFunctions.getDeviceId(RegisterActivity.this),
                edt_phone_number.getText().toString(),
                edt_password.getText().toString(),
                edt_userName.getText().toString(),
                "NORMAL_USER",
                selected_age,
                selected_gender,
                "1994-01-01",
                selected_country,
                selected_city,
                selected_first_cuisine,
                selected_second_cuisine,
                selected_third_cuisine,
                new Gson().toJson(selected_prefer),
                selected_i_am,
                "",
                "",
                selected_first_music,
                selected_second_music,
                selected_third_music,
                "",
                "0",
                "0",
                "0",
                "0",
                "0",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                new Gson().toJson(selected_usually),
                imagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        //Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_more), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){

                            String json1 = new Gson().toJson(json);
                            LoginResponse success = new Gson().fromJson(json1,LoginResponse.class);
                            if (success.getProfile()!=null){
                                /*
                                if (success.getProfile().getUser_basic_info()!=null){
                                    SharedPrefManager.getInstance(RegisterActivity.this).setUser(success.getProfile().getUser_basic_info());
                                }
                                if (success.getProfile().getDevice()!=null){
                                    SharedPrefManager.getInstance(RegisterActivity.this).setDevice(success.getProfile().getDevice());
                                }
                                if (success.getProfile().getDevice_setting()!=null){
                                    SharedPrefManager.getInstance(RegisterActivity.this).setDeviceSetting(success.getProfile().getDevice_setting());
                                }

                                 */
                                if (success.getProfile().getTokens()!=null){
                                    SharedPrefManager.getInstance(RegisterActivity.this).setAccessToken(success.getProfile().getTokens().getAccess_token());
                                }


                                Intent intent = new Intent(RegisterActivity.this,ConfirmAccountActivity.class);
                                intent.putExtra("phone",edt_phone_number.getText().toString());
                                startActivity(intent);
                                finish();
                            }

                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callRegisterAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callUpdateSocial(){
        AuthAPIsClass.update_social_data(
                RegisterActivity.this,
                BaseFunctions.getDeviceId(RegisterActivity.this),
                "",
                "",
                edt_userName.getText().toString(),
                "NORMAL_USER",
                selected_age,
                selected_gender,
                "1994-01-01",
                selected_country,
                selected_city,
                selected_first_cuisine,
                selected_second_cuisine,
                selected_third_cuisine,
                new Gson().toJson(selected_prefer),
                selected_i_am,
                "",
                "",
                selected_first_music,
                selected_second_music,
                selected_third_music,
                "",
                "0",
                "0",
                "0",
                "0",
                "0",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                new Gson().toJson(selected_usually),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        //Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_more), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){

                            String json1 = new Gson().toJson(json);
                            LoginResponse success = new Gson().fromJson(json1,LoginResponse.class);
                            if (success.getProfile()!=null){
                                if (success.getProfile().getUser_basic_info()!=null){
                                    SharedPrefManager.getInstance(RegisterActivity.this).setUser(success.getProfile().getUser_basic_info());
                                    SettingsObject so = new SettingsObject();
                                    so.setVisible_to_search(success.getProfile().getDevice_setting().getVisible_to_search());
                                    so.setAllow_people_to_follow(success.getProfile().getDevice_setting().getAllow_people_to_follow());
                                    so.setIs_profile_public(success.getProfile().getDevice_setting().getIs_profile_public());
                                    so.setPeople_can_comment_on_posts(success.getProfile().getDevice_setting().getPeople_can_comment_on_posts());
                                    so.setPeople_can_vote_posts(success.getProfile().getDevice_setting().getPeople_can_vote_posts());
                                    so.setComments_notifications(success.getProfile().getDevice_setting().getComments_notifications());
                                    so.setVotes_notifications(success.getProfile().getDevice_setting().getVotes_notifications());
                                    so.setView_language(success.getProfile().getDevice_setting().getView_language());
                                    SharedPrefManager.getInstance(RegisterActivity.this).setSettings(so);
                                }
                                if (success.getProfile().getDevice()!=null){
                                    SharedPrefManager.getInstance(RegisterActivity.this).setDevice(success.getProfile().getDevice());
                                }
                                if (success.getProfile().getDevice_setting()!=null){
                                    SharedPrefManager.getInstance(RegisterActivity.this).setDeviceSetting(success.getProfile().getDevice_setting());
                                }
                                if (success.getProfile().getTokens()!=null){
                                    SharedPrefManager.getInstance(RegisterActivity.this).setAccessToken(success.getProfile().getTokens().getAccess_token());
                                }
                                Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                                intent.putExtra("type", "home");
                                startActivity(intent);
                                finish();
                            }

                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callRegisterAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode){
            case 100:{
                if (resultCode == RESULT_OK) {
                    if (imageFile.exists()){
                        //Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getPath());
                        //profile_image.setImageBitmap(bitmap);
                        //profile_image_hint.setVisibility(View.GONE);
                        //imagePath = imageFile.getPath();
                        Uri source_uri = Uri.fromFile(imageFile);
                        Uri destination_uri = Uri.fromFile(getTempFile());
                        Crop.of(source_uri,destination_uri).asSquare().start( RegisterActivity.this);
                    }else {
                        Toast.makeText(RegisterActivity.this, "Image file does not exists", Toast.LENGTH_SHORT).show();
                    }
                }
            }break;

            case Crop.REQUEST_CROP:{
                if (resultCode == RESULT_OK) {
                    handle_crop(resultCode,data);
                }
            }break;
            default:{
                if (resultCode == Activity.RESULT_OK) {
                    Uri source_uri = Uri.fromFile(ImagePicker.Companion.getFile(data));
                    Uri destination_uri = Uri.fromFile(getTempFile());
                    Crop.of(source_uri,destination_uri).asSquare().start( RegisterActivity.this);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    ChooseSelectionDialog dialog = new ChooseSelectionDialog(RegisterActivity.this, "camera", new ISelection() {
                        @Override
                        public void onTakeCameraClicked() {
                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                            StrictMode.setVmPolicy(builder.build());
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            imageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                    "test.jpg");
                            Uri tempUri = Uri.fromFile(imageFile);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT,tempUri);
                            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,1);
                            startActivityForResult(intent,100);
                        }

                        @Override
                        public void onTakeVideoClicked() {

                        }

                        @Override
                        public void onCameraGalleryClicked() {
                            /*
                            BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.dma.app.taikme.provider")
                                    .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                                    .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                                    .build();
                            singleSelectionPicker.show(getSupportFragmentManager(),"picker");
                             */
                            ImagePicker.Companion.with(RegisterActivity.this)
                                    .galleryOnly()
                                    //.crop(16f,9f)	    			//Crop image(Optional), Check Customization for more option
                                    .compress(1024)			//Final image size will be less than 1 MB(Optional)
                                    .start();
                        }

                        @Override
                        public void onVideoGalleryClicked() {

                        }
                    });
                    dialog.show();
                } else {

                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.no_permissions), Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    @Override
    public void loadImage(File imageFile, ImageView ivImage) {
        Glide.with(RegisterActivity.this).load(imageFile).into(ivImage);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        //profile_image.setImageURI(uri);
        //profile_image_hint.setVisibility(View.GONE);
        //imagePath = uri.getPath();
        Uri source_uri = uri;
        Uri destination_uri = Uri.fromFile(getTempFile());
        Crop.of(source_uri,destination_uri).asSquare().start( RegisterActivity.this);

    }

    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    private void handle_crop(int code, final Intent data){
        if (code == RESULT_OK){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                        profile_image.setImageDrawable(null);
                        profile_image.setImageURI(Crop.getOutput(data));
                        profile_image_hint.setVisibility(View.GONE);
                        String filePath=  Environment.getExternalStorageDirectory()
                                + "/"+"temporary_holder.jpg";
                        imagePath = filePath;
                }
            });

        }else if (code == Crop.RESULT_ERROR){
            Toast.makeText(RegisterActivity.this, getResources().getString(R.string.error_cropping), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
        finish();
    }
}
