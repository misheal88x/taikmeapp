package com.dma.app.taikme.Fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dma.app.taikme.Adapters.ColorsAdapter;
import com.dma.app.taikme.Adapters.FontsAdapter;
import com.dma.app.taikme.Interfaces.AddtextFragmentListener;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class EditImageTextFragment extends BottomSheetDialogFragment implements IMove,FontsAdapter.FontAdapterClickListener{

    private EditText edt_text;
    private RecyclerView rv_colors,rv_font;
    private List<Integer> listOfColors;
    private ColorsAdapter colorsAdapter;
    private FontsAdapter fontsAdapter;
    private LinearLayoutManager layoutManager,layoutManager2;
    private Button btn_add;
    private int colorSelected = Color.parseColor("#000000");
    private Typeface typefaceSelected = Typeface.DEFAULT;
    private AddtextFragmentListener listener;
    public static EditImageTextFragment instance;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_image_text,container,false);
        init_views(view);
        init_events();
        init_fragment();
        return view;
    }


    public void init_views(View view) {
        edt_text = view.findViewById(R.id.edit_image_text_edt);
        rv_colors = view.findViewById(R.id.edit_image_text_recycler);
        rv_font = view.findViewById(R.id.edit_image_text_font_recycler);
        btn_add = view.findViewById(R.id.edit_image_text_btn);
    }


    public void init_events() {
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_text.getText().toString().equals("")){
                    Toast.makeText(getActivity(), getResources().getString(R.string.edit_image_text_no_text), Toast.LENGTH_SHORT).show();
                    return;
                }
                dismiss();
                listener.onAddTextButtonClick(typefaceSelected,edt_text.getText().toString(),colorSelected);
                edt_text.setText("");
            }
        });
    }


    public void init_fragment() {
        init_recycler();
    }

    private void init_recycler(){
        listOfColors = new ArrayList<>();
        listOfColors.add(Color.parseColor("#132722"));
        listOfColors.add(Color.parseColor("#ff545e"));
        listOfColors.add(Color.parseColor("#57bb82"));
        listOfColors.add(Color.parseColor("#dbeeff"));
        listOfColors.add(Color.parseColor("#ba5796"));
        listOfColors.add(Color.parseColor("#bb349b"));
        listOfColors.add(Color.parseColor("#6e557c"));
        listOfColors.add(Color.parseColor("#5e40b2"));
        listOfColors.add(Color.parseColor("#8051cf"));
        listOfColors.add(Color.parseColor("#895adc"));
        listOfColors.add(Color.parseColor("#935da0"));
        listOfColors.add(Color.parseColor("#7a5e93"));
        listOfColors.add(Color.parseColor("#6c4475"));
        listOfColors.add(Color.parseColor("#403890"));
        listOfColors.add(Color.parseColor("#1b36eb"));
        listOfColors.add(Color.parseColor("#10d6a2"));
        listOfColors.add(Color.parseColor("#45b9d3"));
        colorsAdapter = new ColorsAdapter(getActivity(), listOfColors,this );
        fontsAdapter = new FontsAdapter(getActivity(),this);
        rv_font.hasFixedSize();
        layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        layoutManager2 = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        rv_colors.setLayoutManager(layoutManager);
        rv_font.setLayoutManager(layoutManager2);
        rv_colors.setAdapter(colorsAdapter);
        rv_font.setAdapter(fontsAdapter);
    }

    @Override
    public void move() {
    }

    @Override
    public void move(int position) {
        colorSelected = listOfColors.get(position);
    }

    public void setListener(AddtextFragmentListener listener) {
        this.listener = listener;
    }

    public static EditImageTextFragment getInstance(){
        if (instance == null){
            instance = new EditImageTextFragment();
        }
        return instance;
    }

    @Override
    public void onFontSelected(String fontName) {
        typefaceSelected = Typeface.createFromAsset(getActivity().getAssets(),new StringBuilder("fonts/").
                append(fontName).toString());
    }
}
