package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.dma.app.taikme.APIsClass.CommentsAPIsClass;
import com.dma.app.taikme.APIsClass.PlacesAPIsClass;
import com.dma.app.taikme.Adapters.AllCommentsAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.CommentObject;
import com.dma.app.taikme.Models.CommentsResponse;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.PlacesResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AllCommentsDialog extends AlertDialog {
    private Context context;

    private LinearLayout no_data_layout;
    private RecyclerView rv_comments;
    private AllCommentsAdapter commentsAdapter;
    private List<CommentObject> list;
    private AVLoadingIndicatorView more;
    private NestedScrollView scrollView;
    private CardView cardView;

    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;

    private int id;
    private String type;
    public AllCommentsDialog(@NonNull Context context,String type,int id) {
        super(context);
        this.context = context;
        this.type = type;
        this.id = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_all_comments);
        /*
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);

         */
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        //LinearLayout
        no_data_layout = findViewById(R.id.no_data_layout);
        //RecyclerView
        rv_comments = findViewById(R.id.all_comments_recycler);
        //Progress bar
        more = findViewById(R.id.comments_more);
        //ScrollView
        scrollView = findViewById(R.id.scroll_view);
        //CardView
        cardView = findViewById(R.id.card_view);
    }

    private void init_events() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callCommentsAPI(currentPage,1);
                            }
                        }
                    }
                }
            }
        });
    }

    private void init_dialog() {
        no_data_layout.setVisibility(View.GONE);
        init_recycler();
        callCommentsAPI(currentPage,0);
    }

    private void init_recycler(){
        list = new ArrayList<>();
        commentsAdapter = new AllCommentsAdapter(context,list);
        rv_comments.setLayoutManager(new LinearLayoutManager(context,RecyclerView.VERTICAL,false));
        BaseFunctions.runAnimation(rv_comments,0,commentsAdapter);
        rv_comments.setAdapter(commentsAdapter);
    }

    private void callCommentsAPI(final int page,final int type){
        if (type == 1){
            more.smoothToShow();
        }
        CommentsAPIsClass.getComments(
                context,
                BaseFunctions.getDeviceId(context),
                this.type,
                String.valueOf(this.id),
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            CommentsResponse success = new Gson().fromJson(j,CommentsResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (CommentObject po : success.getData()){
                                        list.add(po);
                                        commentsAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(cardView, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callCommentsAPI(page,type);
                                    }
                                }).setActionTextColor(context.getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            more.smoothToHide();
        }else {
            no_data_layout.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }else {
            more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.VISIBLE);
        }else {
            more.smoothToHide();
            Snackbar.make(cardView,context.getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
