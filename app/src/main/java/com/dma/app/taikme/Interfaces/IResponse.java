package com.dma.app.taikme.Interfaces;

/**
 * Created by Misheal on 11/13/2019.
 */

public interface IResponse {
    void onResponse();
    void onResponse(Object json);
}
